-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 28, 2017 at 03:39 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `paycabs`
--

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `signup_date` varchar(255) NOT NULL,
  `driving_license` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `aadharcard_number` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`id`, `name`, `email`, `phone`, `signup_date`, `driving_license`, `password`, `city`, `aadharcard_number`, `image`) VALUES
(4, 'Ravindra verma', 'rajesh@gmail.com', '9456464678', '2017-10-11', 'UP45UY5463', '123456789', 'Noida', '323253564646', '_DSC0039.png'),
(5, 'Ritesh', 'rajesh@gmail.com', '9812323489', '2017-10-13', 'DL21TD3435', '123456789', 'up', '98766747654', 'camera.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `driver_auto`
--

CREATE TABLE `driver_auto` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `signup_date` varchar(255) NOT NULL,
  `driving_license` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `aadharcard_number` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver_auto`
--

INSERT INTO `driver_auto` (`id`, `name`, `email`, `phone`, `signup_date`, `driving_license`, `password`, `city`, `aadharcard_number`, `image`) VALUES
(2, 'Sumit', 'sshailendra1211@gmail.com', '6578978890', '2017-10-16', 'MH23TR5463', '0987654', 'mumbai', '654323131231', '_DSC0049.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `driver_bike`
--

CREATE TABLE `driver_bike` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `signup_date` varchar(255) NOT NULL,
  `driving_license` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `aadharcard_number` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver_bike`
--

INSERT INTO `driver_bike` (`id`, `name`, `email`, `phone`, `signup_date`, `driving_license`, `password`, `city`, `aadharcard_number`, `image`) VALUES
(1, 'Ritesh', 'ravi@gmail.com', '9450991405', '2017-10-08', 'RT65KH8745', '123456', 'ghaziabad', '645675756786', '_DSC0107.jpg'),
(2, 'Sumit', 'sshailendra1211@gmail.com', '8967564345', '2017-10-16', 'MH23TR5463', '9877665445', 'mumbai', '877643453456', '_DSC0124.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `header`
--

CREATE TABLE `header` (
  `id` int(250) NOT NULL,
  `logo` varchar(40) NOT NULL,
  `head_color` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `header`
--

INSERT INTO `header` (`id`, `logo`, `head_color`) VALUES
(1, 'logo1.png', '6c6c6c');

-- --------------------------------------------------------

--
-- Table structure for table `instant_booking`
--

CREATE TABLE `instant_booking` (
  `id` int(255) NOT NULL,
  `pickup_point` varchar(255) NOT NULL,
  `destination` varchar(255) NOT NULL,
  `ride_type` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `fare` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `payment_mode` varchar(255) NOT NULL,
  `signature` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `instant_booking`
--

INSERT INTO `instant_booking` (`id`, `pickup_point`, `destination`, `ride_type`, `date`, `fare`, `time`, `payment_mode`, `signature`, `driver_name`) VALUES
(1, 'Delhi', 'Noida', 'share', '2017-01-31', '3456', '20 minutes', 'cash', 'ravindra', 'Sumit'),
(2, 'noida', 'faridabad', 'booking', '2017-10-25', '765', '30minutes', 'case', 'ravindra', 'Rahul');

-- --------------------------------------------------------

--
-- Table structure for table `instant_booking_auto`
--

CREATE TABLE `instant_booking_auto` (
  `id` int(255) NOT NULL,
  `pickup_point` varchar(255) NOT NULL,
  `destination` varchar(255) NOT NULL,
  `ride_type` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `fare` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `payment_mode` varchar(255) NOT NULL,
  `signature` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `instant_booking_auto`
--

INSERT INTO `instant_booking_auto` (`id`, `pickup_point`, `destination`, `ride_type`, `date`, `fare`, `time`, `payment_mode`, `signature`, `driver_name`) VALUES
(1, 'noida', 'delhi', 'booking', '2017-10-07', '565', '50minutes', 'cash', 'sumit', 'Saumya');

-- --------------------------------------------------------

--
-- Table structure for table `instant_booking_bike`
--

CREATE TABLE `instant_booking_bike` (
  `id` int(255) NOT NULL,
  `pickup_point` varchar(255) NOT NULL,
  `destination` varchar(255) NOT NULL,
  `ride_type` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `fare` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `payment_mode` varchar(255) NOT NULL,
  `signature` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `instant_booking_bike`
--

INSERT INTO `instant_booking_bike` (`id`, `pickup_point`, `destination`, `ride_type`, `date`, `fare`, `time`, `payment_mode`, `signature`, `driver_name`) VALUES
(1, 'Delhi', 'Noida', 'share', '2017-10-12', '45', '45minutes', 'cash', 'sumit', 'Rahul');

-- --------------------------------------------------------

--
-- Table structure for table `late_booking_auto`
--

CREATE TABLE `late_booking_auto` (
  `id` int(255) NOT NULL,
  `passenger_name` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `destination` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `late_booking_auto`
--

INSERT INTO `late_booking_auto` (`id`, `passenger_name`, `date`, `pickup_location`, `destination`, `driver_name`, `ride_date`, `vehicle_number`) VALUES
(1, 'Sanjeev', '32/05/2017', 'noida', 'delhi', 'Sumit', '2017-10-14', 'DL34TD4355');

-- --------------------------------------------------------

--
-- Table structure for table `late_booking_bike`
--

CREATE TABLE `late_booking_bike` (
  `id` int(255) NOT NULL,
  `passenger_name` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `destination` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `late_booking_bike`
--

INSERT INTO `late_booking_bike` (`id`, `passenger_name`, `date`, `pickup_location`, `destination`, `driver_name`, `ride_date`, `vehicle_number`) VALUES
(1, 'Ritesh', '23/10/2017', 'noida', 'Ghaziabad', 'Saumya', '2017-10-10', 'UP16FT2345');

-- --------------------------------------------------------

--
-- Table structure for table `map`
--

CREATE TABLE `map` (
  `id` int(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL,
  `lat` float(15,11) NOT NULL,
  `lng` float(15,11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `map`
--

INSERT INTO `map` (`id`, `vehicle_number`, `lat`, `lng`) VALUES
(1, 'DL34TD4355', 28.57819938660, 77.31780242920);

-- --------------------------------------------------------

--
-- Table structure for table `map_auto`
--

CREATE TABLE `map_auto` (
  `id` int(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL,
  `lat` float(15,11) NOT NULL,
  `lng` float(15,11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `map_auto`
--

INSERT INTO `map_auto` (`id`, `vehicle_number`, `lat`, `lng`) VALUES
(1, 'UP16DY5436', 28.58530044556, 77.31150054932),
(2, 'DL34TH3456', 28.57080078125, 77.32610321045);

-- --------------------------------------------------------

--
-- Table structure for table `map_bike`
--

CREATE TABLE `map_bike` (
  `id` int(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL,
  `lat` float(15,11) NOT NULL,
  `lng` float(15,11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `map_bike`
--

INSERT INTO `map_bike` (`id`, `vehicle_number`, `lat`, `lng`) VALUES
(1, 'UP16DY5436', 28.58530044556, 77.31150054932),
(2, 'DL34TH3456', 28.57080078125, 77.32610321045);

-- --------------------------------------------------------

--
-- Table structure for table `payment_report`
--

CREATE TABLE `payment_report` (
  `id` int(255) NOT NULL,
  `ride_number` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `passenger_name` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `total_fare` varchar(255) NOT NULL,
  `ride_status` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_report`
--

INSERT INTO `payment_report` (`id`, `ride_number`, `driver_name`, `passenger_name`, `ride_date`, `total_fare`, `ride_status`, `payment_method`, `vehicle_number`) VALUES
(1, '250', 'Saumya', 'sumit', '2017-01-31', '50', 'Finished', 'Cash', 'DL34TD4355'),
(3, '234', 'Saumyag', 'Rahul', '2017-10-10', '45', 'Finished', 'online', 'JK45RE2314');

-- --------------------------------------------------------

--
-- Table structure for table `payment_report_auto`
--

CREATE TABLE `payment_report_auto` (
  `id` int(255) NOT NULL,
  `ride_number` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `passenger_name` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `total_fare` varchar(255) NOT NULL,
  `ride_status` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_report_auto`
--

INSERT INTO `payment_report_auto` (`id`, `ride_number`, `driver_name`, `passenger_name`, `ride_date`, `total_fare`, `ride_status`, `payment_method`, `vehicle_number`) VALUES
(1, '34', 'Rahul', 'sumit', '2017-10-06', '345', 'Finished', 'Cash', 'UP16FT2345'),
(2, '543', 'Rakesh Jha', 'Rahul', '2017-10-16', '677', 'Finished', 'online', 'JK45RE2314');

-- --------------------------------------------------------

--
-- Table structure for table `payment_report_bike`
--

CREATE TABLE `payment_report_bike` (
  `id` int(255) NOT NULL,
  `ride_number` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `passenger_name` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `total_fare` varchar(255) NOT NULL,
  `ride_status` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_report_bike`
--

INSERT INTO `payment_report_bike` (`id`, `ride_number`, `driver_name`, `passenger_name`, `ride_date`, `total_fare`, `ride_status`, `payment_method`, `vehicle_number`) VALUES
(1, '65', 'Ratan', 'Ritesh', '2017-10-10', '654', 'Finished', 'Cash', 'DL34TD4355');

-- --------------------------------------------------------

--
-- Table structure for table `rider`
--

CREATE TABLE `rider` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `signup_date` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rider`
--

INSERT INTO `rider` (`id`, `name`, `email`, `phone`, `signup_date`, `password`, `city`) VALUES
(1, 'Ravindra', 'ravindra4736@gmail.com', '7687978900', '2017-10-09', '123456789', 'Noida'),
(2, 'sachin sharma', 'r@gmail.com', '9812323489', '2017-10-10', '123456789', 'Delhi');

-- --------------------------------------------------------

--
-- Table structure for table `rider_later_booking`
--

CREATE TABLE `rider_later_booking` (
  `id` int(255) NOT NULL,
  `passenger_name` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `destination` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rider_later_booking`
--

INSERT INTO `rider_later_booking` (`id`, `passenger_name`, `date`, `pickup_location`, `destination`, `driver_name`, `ride_date`, `vehicle_number`) VALUES
(2, 'Sanjeev', '2017-10-11', 'noida', 'Ghaziabad', 'PUNIT', '2017-10-11', 'UP16FT2345');

-- --------------------------------------------------------

--
-- Table structure for table `taxi`
--

CREATE TABLE `taxi` (
  `id` int(255) NOT NULL,
  `taxi_name` varchar(255) NOT NULL,
  `model_name` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL,
  `vehicle_registration_number` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `taxi`
--

INSERT INTO `taxi` (`id`, `taxi_name`, `model_name`, `driver_name`, `vehicle_number`, `vehicle_registration_number`, `image`) VALUES
(1, 'TATA', 'safari', 'Ravindra', 'DL34TD4355', 'TF3421HG45', '_DSC0127.png');

-- --------------------------------------------------------

--
-- Table structure for table `taxi_auto`
--

CREATE TABLE `taxi_auto` (
  `id` int(255) NOT NULL,
  `taxi_name` varchar(255) NOT NULL,
  `model_name` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL,
  `vehicle_registration_number` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `taxi_auto`
--

INSERT INTO `taxi_auto` (`id`, `taxi_name`, `model_name`, `driver_name`, `vehicle_number`, `vehicle_registration_number`, `image`) VALUES
(1, 'Bajaj', 'vp7', 'Rahul', 'DL34TD4355', 'rgfd65465nu76', 'f_icon2.png');

-- --------------------------------------------------------

--
-- Table structure for table `taxi_bike`
--

CREATE TABLE `taxi_bike` (
  `id` int(255) NOT NULL,
  `taxi_name` varchar(255) NOT NULL,
  `model_name` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL,
  `vehicle_registration_number` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `taxi_bike`
--

INSERT INTO `taxi_bike` (`id`, `taxi_name`, `model_name`, `driver_name`, `vehicle_number`, `vehicle_registration_number`, `image`) VALUES
(1, 'TVS', 'Sport', 'Saumya', 'UP16FT2345', 'R2345S5678', '_DSC0204.png');

-- --------------------------------------------------------

--
-- Table structure for table `trips`
--

CREATE TABLE `trips` (
  `id` int(255) NOT NULL,
  `trip_type` varchar(255) NOT NULL,
  `trip_number` varchar(255) NOT NULL,
  `trip_date` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `passenger_name` varchar(255) NOT NULL,
  `fare` varchar(255) NOT NULL,
  `taxi` varchar(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trips`
--

INSERT INTO `trips` (`id`, `trip_type`, `trip_number`, `trip_date`, `driver_name`, `passenger_name`, `fare`, `taxi`, `vehicle_number`, `time`) VALUES
(1, 'Rental', '3456', '2017-10-17', 'Rahul', 'Rahul', '456', 'basic', 'UP16FT2345', '78 minutes'),
(2, 'Rental', '3456', '2017-10-13', 'Ronit', 'Sanjeev', '566', 'basic', 'DL34TD4355', '45minutes');

-- --------------------------------------------------------

--
-- Table structure for table `trips_auto`
--

CREATE TABLE `trips_auto` (
  `id` int(255) NOT NULL,
  `trip_type` varchar(255) NOT NULL,
  `trip_number` varchar(255) NOT NULL,
  `trip_date` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `passenger_name` varchar(255) NOT NULL,
  `fare` varchar(255) NOT NULL,
  `taxi` varchar(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trips_auto`
--

INSERT INTO `trips_auto` (`id`, `trip_type`, `trip_number`, `trip_date`, `driver_name`, `passenger_name`, `fare`, `taxi`, `vehicle_number`, `time`) VALUES
(1, 'Rental', '453', '2017-10-11', 'Rahul', 'sumit', '45', 'basic', 'UP16FT2345', '20minutes');

-- --------------------------------------------------------

--
-- Table structure for table `trips_bike`
--

CREATE TABLE `trips_bike` (
  `id` int(255) NOT NULL,
  `trip_type` varchar(255) NOT NULL,
  `trip_number` varchar(255) NOT NULL,
  `trip_date` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `passenger_name` varchar(255) NOT NULL,
  `fare` varchar(255) NOT NULL,
  `taxi` varchar(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trips_bike`
--

INSERT INTO `trips_bike` (`id`, `trip_type`, `trip_number`, `trip_date`, `driver_name`, `passenger_name`, `fare`, `taxi`, `vehicle_number`, `time`) VALUES
(1, 'booking', '453', '2017-10-27', 'Sumit', 'Ravindra', '345', 'basic', 'UP16FT2345', '56minutes');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(240) NOT NULL,
  `name` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `image` varchar(40) NOT NULL,
  `role` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `phone`, `password`, `image`, `role`) VALUES
(1, 'Ravi', 'ravindra4736@gmail.com', '1234567891', '123456789', 'logo1.png', 'Super Admin'),
(2, 'Ritesh', 'rajesh@gmail.com', '9812323456', '123456789', '_DSC0051.jpg', 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_type`
--

CREATE TABLE `vehicle_type` (
  `id` int(255) NOT NULL,
  `vehicle_type` varchar(255) NOT NULL,
  `price_per_km` varchar(255) NOT NULL,
  `price_per_min` varchar(255) NOT NULL,
  `base_fare` varchar(255) NOT NULL,
  `person_capacity` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_type`
--

INSERT INTO `vehicle_type` (`id`, `vehicle_type`, `price_per_km`, `price_per_min`, `base_fare`, `person_capacity`) VALUES
(1, 'luxary', '54', '56', '233', '4');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_type_auto`
--

CREATE TABLE `vehicle_type_auto` (
  `id` int(255) NOT NULL,
  `vehicle_type` varchar(255) NOT NULL,
  `price_per_km` varchar(255) NOT NULL,
  `price_per_min` varchar(255) NOT NULL,
  `base_fare` varchar(255) NOT NULL,
  `person_capacity` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_type_bike`
--

CREATE TABLE `vehicle_type_bike` (
  `id` int(255) NOT NULL,
  `vehicle_type` varchar(255) NOT NULL,
  `price_per_km` varchar(255) NOT NULL,
  `price_per_min` varchar(255) NOT NULL,
  `base_fare` varchar(255) NOT NULL,
  `person_capacity` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `driver_auto`
--
ALTER TABLE `driver_auto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `driver_bike`
--
ALTER TABLE `driver_bike`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `header`
--
ALTER TABLE `header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instant_booking`
--
ALTER TABLE `instant_booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instant_booking_auto`
--
ALTER TABLE `instant_booking_auto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instant_booking_bike`
--
ALTER TABLE `instant_booking_bike`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `late_booking_auto`
--
ALTER TABLE `late_booking_auto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `late_booking_bike`
--
ALTER TABLE `late_booking_bike`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `map`
--
ALTER TABLE `map`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `map_auto`
--
ALTER TABLE `map_auto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `map_bike`
--
ALTER TABLE `map_bike`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_report`
--
ALTER TABLE `payment_report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_report_auto`
--
ALTER TABLE `payment_report_auto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_report_bike`
--
ALTER TABLE `payment_report_bike`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rider`
--
ALTER TABLE `rider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rider_later_booking`
--
ALTER TABLE `rider_later_booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taxi`
--
ALTER TABLE `taxi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taxi_auto`
--
ALTER TABLE `taxi_auto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taxi_bike`
--
ALTER TABLE `taxi_bike`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trips`
--
ALTER TABLE `trips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trips_auto`
--
ALTER TABLE `trips_auto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trips_bike`
--
ALTER TABLE `trips_bike`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `vehicle_type`
--
ALTER TABLE `vehicle_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_type_auto`
--
ALTER TABLE `vehicle_type_auto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_type_bike`
--
ALTER TABLE `vehicle_type_bike`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `driver`
--
ALTER TABLE `driver`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `driver_auto`
--
ALTER TABLE `driver_auto`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `driver_bike`
--
ALTER TABLE `driver_bike`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `header`
--
ALTER TABLE `header`
  MODIFY `id` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `instant_booking`
--
ALTER TABLE `instant_booking`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `instant_booking_auto`
--
ALTER TABLE `instant_booking_auto`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `instant_booking_bike`
--
ALTER TABLE `instant_booking_bike`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `late_booking_auto`
--
ALTER TABLE `late_booking_auto`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `late_booking_bike`
--
ALTER TABLE `late_booking_bike`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `map`
--
ALTER TABLE `map`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `map_auto`
--
ALTER TABLE `map_auto`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `map_bike`
--
ALTER TABLE `map_bike`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `payment_report`
--
ALTER TABLE `payment_report`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `payment_report_auto`
--
ALTER TABLE `payment_report_auto`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `payment_report_bike`
--
ALTER TABLE `payment_report_bike`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `rider`
--
ALTER TABLE `rider`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `rider_later_booking`
--
ALTER TABLE `rider_later_booking`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `taxi`
--
ALTER TABLE `taxi`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `taxi_auto`
--
ALTER TABLE `taxi_auto`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `taxi_bike`
--
ALTER TABLE `taxi_bike`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `trips`
--
ALTER TABLE `trips`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `trips_auto`
--
ALTER TABLE `trips_auto`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `trips_bike`
--
ALTER TABLE `trips_bike`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(240) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `vehicle_type`
--
ALTER TABLE `vehicle_type`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vehicle_type_auto`
--
ALTER TABLE `vehicle_type_auto`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vehicle_type_bike`
--
ALTER TABLE `vehicle_type_bike`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
