<?php
if($_SERVER['REQUEST_METHOD']=='POST')
{
   
    include "database.php";
    $email = $_POST['email'];
    $sql = "SELECT name,password from user where email = '$email'";
    $result = $conn->query($sql);
    if ($result->num_rows>0)
    {
        $maildata = $result->fetch_assoc();

        $email_to = $email; 
        $email_from = "HYRcab@gmail.com"; // must be different than $email_from 
        $email_subject = "Password Recovery Mail";
        $name = $maildata['name'];
        $password = $maildata['password'];
        $message = "Your Account Details\r\nName : $name\r\nEmail : $email_to\r\nPassword : $password\r\n\r\n **Please change your password after login";
        // check for empty required fields
        if (!isset($name) || !isset($message))
        {
            echo "Please Fill Required Fields.";
        }
        // form validation
        $error_message = "";

        // name
        $name_exp = "/^[a-z0-9 .\-]+$/i";
        if (!preg_match($name_exp,$name))
        {
            $this_error = 'Please enter a valid name.';
            $error_message .= ($error_message == "") ? $this_error : "<br/>".$this_error;
        }        

        $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
        if (!preg_match($email_exp,$email))
        {
            $this_error = 'Please enter a valid email address.';
            $error_message .= ($error_message == "") ? $this_error : "<br/>".$this_error;
        } 

        // if there are validation errors
        if(strlen($error_message) > 0)
        {
            echo $error_message;
        }

        // prepare email message
        $email_message = "Form details below.\n\n";

        function clean_string($string)
        {
            $bad = array("content-type", "bcc:", "to:", "cc:", "href");
            return str_replace($bad, "", $string);
        }

        $email_message = $message;

        // create email headers
        $headers = 'From: '.$email_from."\r\n".
        'Reply-To: '.$email."\r\n" .
        'X-Mailer: PHP/' . phpversion();
        if (@mail($email_to, $email_subject, $email_message, $headers))
        {
            echo "Password is sent to Your Email"; 
        }

        else 
        {
            echo "An Error Occured. Please Try Again Later"; 
            die();        
        }

    }
    else
    {
        echo "Something Wrong! Please Try Again ";
    }
}
?>