<?php
$method = $_SERVER['REQUEST_METHOD'];
$input = json_decode(file_get_contents('php://input'),true);
if($method=='POST')
{
	function getaddress($lat,$lng)
	{
		$url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($lng).'&sensor=false';
		$json = @file_get_contents($url);
		$data=json_decode($json);
		$status = $data->status;
		if($status=="OK")
		{
			return $data->results[0]->formatted_address;
		}
			else
			{
				return false;
			}
	}

	function message($code,$message,$data)
     {
          $msg = array(
          "code" => $code,
          "message" => $message,
		  "driverInRoute" => $data
		  );
       echo json_encode($msg);
      }
	  	  
	  if(empty($input['booking_id']) && isset($input['booking_id']))
	  {
		message(400,"bookingId missing");
		die;
	  }
	  else
	  {
	  $booking_id = $input['booking_id'];	
	  }

	  if(empty($input['passenger_id']) && isset($input['passenger_id']))
	  {
		message(400,"PassengerId missing");
		die;
	  }
	  else
	  {
	  $passenger_id = $input['passenger_id'];	
	  }
	  	
	$con= new Mysqli('127.0.0.1','isiwal123','Mahaveer@123','hyrcabadmin');
	if($con->connect_error)
	{
		die("Connection Failed" .$con->connect_error);
	}		
		   				
	$sql="SELECT d.*,t.* FROM trips AS t INNER JOIN driver AS d ON d.user_id = t.driver_id  WHERE (t.booking_id = '$booking_id' AND t.passenger_id = '$passenger_id')";
	  $result= $con->query($sql);
   if($result-> num_rows > 0)
   {	
   		$row = $result->fetch_assoc();
   		if($row['trip_complete_status'] == 0)
   		{
			$data['driverId'] = $row['user_id'];
			$data['isTripComplete'] = "0";
			$data['lat'] = $row['lat'];
			$data['long'] = $row['lng'];
			$data['driverName'] = $row['name'];
			$data['driverPhone'] = $row['phone'];
			$data['vehicleName'] = $row['vehicle_name'];
			$data['vehicleNumber'] = $row['vehicle_number'];
			$data['driverAddress'] = getaddress($row['lat'],$row['lng']);		
   		}
   		if($row['trip_complete_status'] == 1)
   		{
			$data['driverId'] = $row['user_id'];
			$data['isTripComplete'] = "1";
			$data['lat'] = $row['lat'];
			$data['long'] = $row['lng'];
			$data['driverName'] = $row['name'];
			$data['driverPhone'] = $row['phone'];
			$data['vehicleName'] = $row['vehicle_name'];
			$data['vehicleNumber'] = $row['vehicle_number'];
			$data['driverAddress'] = getaddress($row['lat'],$row['lng']);		
   		}
    message(200,"Successful",$data);
   }    		 
   else
	{ $data = array();
		message(200,"Booking request is not accepted",$data);
	}	
	$con->close();	
}
?>