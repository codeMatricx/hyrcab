 <?php
 $image_dir = "assets/img/User/";
function password_mark($data)
{
    $star ="";
    $length = strlen($data);
  while($length)
  {
      $star .="*";
      $length--;
  }
  return $star;
}
function test_input($data)
{
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
if (isset($_POST['addUser']))
{
    
      
    $name = test_input($_POST['name']);
    $email = test_input($_POST['email']);
    $phone = test_input($_POST['phone']);
    $password = test_input($_POST['password']);
    $role = test_input($_POST['role']);
    $status = 1;
    $image = $_FILES['image']['name'];
    $image_file = $image_dir . basename($_FILES["image"]["name"]);
    $imageFileType = strtolower(pathinfo($image_file,PATHINFO_EXTENSION));

    $check = getimagesize($_FILES["image"]["tmp_name"]);
    if($check == false)
    {
        $status = 0;
    }
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif")
    {
        $status = 0;
    }
    if ($status == 0) 
    {
        $responseMessage = "Sorry, your file was not uploaded.";
    // if everything is ok, try to upload file
    }
    else
    {
        $imageupload = move_uploaded_file($_FILES["image"]["tmp_name"], $image_file);
        if ($imageupload)
        {
            
            $sql = "INSERT INTO user (name,email,phone,password,image,role) VALUES ('$name','$email','$phone','$password','$image','$role')";
            if ($conn->query($sql) === TRUE)
            {
               $responseMessage =  "User Add successfully";
            }
            else
            {
                $responseMessage =  "Connection failed: " . $conn->error;
            }
        }
        else
        {
            $responseMessage =  "Sorry, there was an error in uploading your file.";
        }
    }
}
if (isset($_POST['deleteUser']))
{
    $id = test_input($_POST['id']);
   /**$sql = "SELECT image from user where id = $id";
    $result = $conn->query($sql);
    if ($result->num_rows>0)
    {
        $data = $result->fetch_assoc();
        $deleteimage = $data['image'];
    }**/
    $sql = "DELETE FROM user WHERE id=$id";
    if ($conn->query($sql) === TRUE)
    {
        //unlink($image_dir.$deleteimage);
        $responseMessage =  "User Remove successfully";
    }
    else
    {
        $responseMessage =  "Connection failed: " . $conn->error;
    }
}

if (isset($_POST['updateUser']))
{
    
      
    $name = test_input($_POST['name']);
    $email = test_input($_POST['email']);
    $phone = test_input($_POST['phone']);
    $password = test_input($_POST['password']);
    $role = test_input($_POST['role']);
    $status = 1;
    $image = $_FILES['image']['name'];
    $image_file = $image_dir . basename($_FILES["image"]["name"]);
    $imageFileType = strtolower(pathinfo($image_file,PATHINFO_EXTENSION));

    $check = getimagesize($_FILES["image"]["tmp_name"]);
    if($check == false)
    {
        $status = 0;
    }
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif")
    {
        $status = 0;
    }
    if ($status == 0) 
    {
        $responseMessage = "Sorry, your file was not uploaded.";
    // if everything is ok, try to upload file
    }
    else
    {
        $imageupload = move_uploaded_file($_FILES["image"]["tmp_name"], $image_file);
        if ($imageupload)
        {
            $set .= " name = '$name',email='$email', phone = '$phone', password = '$password', image = '$image', role = '$role'";
            $sql = "UPDATE user SET $set WHERE email = '$email' ";
            if ($conn->query($sql) === TRUE)
            {
               $responseMessage =  "User Add successfully";
            }
            else
            {
                $responseMessage =  "Connection failed: " . $conn->error;
            }
        }
        else
        {
            $responseMessage =  "Sorry, there was an error in uploading your file.";
        }
    }
}





  ?>

 <div class="inner" style="min-height: 500px;">
    <div class="row">
        <div class="col-lg-12">
          <div class="col-md-4">
            <h2 style="margin-top: 25px; color:#333333;     font-size: 24px;"><b>ADMIN</b></h2>
			</div>
			<div class="col-md-6">
			</div>
			<div class="col-md-2" style="padding-left:100px;">
                <button id="popup" class="btn text-muted text-center btn-success" onclick="div_show('addUser')">Add Admin</button>
				</div>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-lg-12">
            <div class="">

                <div class="">
                    <div class="table-responsive" style=" width: 100%; overflow:scroll; max-height: 550px;">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="text-align: center;">Admin id</th>
                                    <th style="text-align: center;">Name</th>
                                    <th style="text-align: center;">Email</th>
                                    <th style="text-align: center;">Phone</th>
                                    <th style="text-align: center;">Password</th>
                                    <th style="text-align: center;">Image</th>
                                    <th style="text-align: center;">Role</th>
                                    <th style="text-align: center;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                    $sql = "SELECT * from user WHERE NOT role ='super admin'";
                                    $result = $conn->query($sql);
                                    if ($result->num_rows>0)
                                    {
                                        
                                        while($users = $result->fetch_assoc())
                                        {
                                    ?>
                                    <tr class="tosearch" id="<?php  echo $users['id'];?>">
                                        <?php foreach ($users as $key => $value) 
                                        {
                                            if ($key=='password') {
                                                $value = password_mark($value);
                                            }
                                            if($key == 'image')
                                            {
                                                $value = "<img height= '50px' width='auto'  src='assets/img/User/".$value."'>";
                                            }
                                        ?>
                                        <td style="text-align: center;" class="<?php echo $key ?>"><?php echo $value ?></td>
                                        <?php } ?>
                                        <td style="font-size: 13px; text-align: center">
                                            <a class="<?php  echo $users['id'];?>" onclick="div_show('updateUser',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;">UPDATE</a>/
                                            <a class="<?php  echo $users['id'];?>" onclick="div_show('deleteUser',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;">DELETE</a>
                                        </td>
                                    </tr>
                                    <?php
                                         } } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="addUser">
                    <!-- Popup Div Starts Here -->
                    <div id="popupAdd" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('addUser')">
                        <form id="form" method="post" name="form" enctype="multipart/form-data">
                            
                            <h2>Add Amin</h2>
                            <hr>
                            <input id="Name" name="name" placeholder=" User Full Name" type="text" required>
                            <input id="Email" name="email" placeholder="Email" type="email" required>
                            <input id="phone" name="phone" placeholder="Mobile Number" type="text" maxlength="10" >
                            <input type="password" id="password" name="password" placeholder="Password"><br>
                            <select name="role"  style="margin-top:20px;" placeholder="Role">
                                <option disabled selected>Select-Role</option>
                                <option>Admin</option>
                                
                            </select>
                            <label for="newimage" class="btn text-muted text-center btn-success" style="width:82%;margin-top: 10px;">Add Image</label>
                            <input id="newimage" type="file" style="display:none" name="image">
                            <input type="submit" id="submit" name="addUser" value="Add">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>
                <div id="updateUser">
                    <!-- Popup Div Starts Here -->
                    <div id="popupUpdate" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('updateUser')">
                        <form  id="form" method="post" name="form">
                            
                            <h2>Update Admin</h2>
                            <hr>
                            <input  name="name" placeholder=" User Full Name" type="text">
                            <input  name="email" placeholder="Registered Email" type="email" required>
                            <input  name="phone" placeholder="Mobile Number" type="text" maxlength="10" >
                            <input type="password"  name="password" placeholder="Password"><br>
                            <select name="role"  style="margin-top:20px;" placeholder="Role">
                                <option disabled selected>Select-Role</option>
                                <option>Admin</option>
                                
                            </select>
                            <label for="updateimage" class="btn text-muted text-center btn-success" style="width:82%;margin-top: 10px;">Update Image</label>
                           
                            <input  id="updateimage" type="file" style="display:none" name="image">
							<input type="submit" id="submit" name="updateUser" value="Update">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>
                 <!-- Display Popup Button -->
                <div id="deleteUser">
                    <!-- Popup Div Starts Here -->
                    <div id="popupDelete" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('deleteUser')">
                        <form method="post">
                            <hr>
                            <h2>Are You Sure??</h2>
                            <input type="submit" name="deleteUser" value="OK">
                            <input type="hidden" name="id" id="deleteId">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>
                <!--POP-->
            </div>
        </div>
    </div>

</div>