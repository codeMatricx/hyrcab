<?php
$profile_dir = "assets/img/profile/";
$logo_dir = "assets/img/logo/";
$sql = "SELECT * from user WHERE role = 'super admin'";
$result = $conn->query($sql);
if ($result->num_rows>0)
{
    
    $profile = $result->fetch_assoc();  
}    
function test_input($data)
{
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
if(isset($_POST['updateProfile']))
{
    $role = $_SESSION['role'];    
    $name = test_input($_POST['name']);
    $email = test_input($_POST['email']);
    $mobile = test_input($_POST['mobile']);
    $set = "";
    $status = 1;
        if (empty($name) || empty($email) || empty($mobile))
        {
            $status=0;
        }
        if (!empty($_FILES['image']['name']) && $status)
        {
            echo "Image is set\n";
            $imagename = $_FILES['image']['name'];

            $target_file = $profile_dir . basename($_FILES["image"]["name"]);
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
            // Check if image file is a actual image or fake image
            $check = getimagesize($_FILES["image"]["tmp_name"]);
            if($check == false)
            {
                echo "Valid size\n";
                $status = 0;
            }
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" )
            {
                echo "Invalid extension";
                $status = 0;
            }
            echo "$status\n";
            if($status)
            {
                if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file))
                {
                    $set .="image = '$imagename',"; 
                } 
            }     
        }
        if ($status)
        {
            echo $set."\n";
            $set .= "name = '$name',email = '$email',phone = '$mobile'";
            $sql = "UPDATE user SET $set WHERE role = '$role' ";
            if ($conn->query($sql) === TRUE)
            {
               $responseMessage =  "User Information Updated successfully";
               echo '<meta http-equiv="refresh" content="0">';
            }
            else
            {
                $responseMessage =  "Connection failed: " . $conn->error;
            }
        } 
    
}
if(isset($_POST['updatePassword']))
{
    $role = $_SESSION['role'];
    $oldpassword = test_input($_POST['currentPassword']);
    $newpassword = test_input($_POST['newPassword']);
    $confirmpassword = test_input($_POST['confirmPassword']);
    $status = 1;
    if ($newpassword == $confirmpassword )
    {
        if (empty($oldpassword) || empty($newpassword) || empty($confirmpassword) )
        {
            $status=0;
        }   
        if ($status)
        {
             $sql = "UPDATE user SET password = '$newpassword' WHERE role = '$role' ";
             if ($conn->query($sql) === TRUE)
            {
               $responseMessage =  "Password Updated successfully";
            }
            else
            {
                $responseMessage =  "Connection failed: " . $conn->error;
            }
        } 
    }
}

if (isset($_POST['updateColor'])) {

    $color = test_input($_POST['color']);
    $color = str_replace("#", "", $color);
    $status = 1;
    if (empty($color))
    {
        $status=0;
    }
    if ($status)
    {
        $sql = "UPDATE header SET head_color = '$color' WHERE id = '1' ";
        if ($conn->query($sql) === TRUE)
        {
            $responseMessage =  "Color Change successfully";
            echo '<meta http-equiv="refresh" content="0">';
        }
        else
        {
            $responseMessage =  "Connection failed: " . $conn->error;
        }
    }
}
if (isset($_POST['updatelogo'])) {
    $target_file = $logo_dir . basename($_FILES["logo"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    // Check if image file is a actual image or fake image
    $check = getimagesize($_FILES["logo"]["tmp_name"]);
    if($check == false)
    {
        $uploadOk = 0;
    }
    
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" )
    {
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) 
    {
        $responseMessage = "Sorry, your file was not uploaded.";
    // if everything is ok, try to upload file
    } 
    else
    {
        if (move_uploaded_file($_FILES["logo"]["tmp_name"], $target_file))
        {
            $name = $_FILES['logo']['name'];
            $sql = "UPDATE header SET logo = '$name'";
            if ($conn->query($sql) === TRUE)
            {
               $responseMessage =  "Logo Add successfully";
               echo '<meta http-equiv="refresh" content="0">';
            }
            else
            {
                $responseMessage =  "Connection failed: " . $conn->error;
            }
        }
        else
        {
            $responseMessage =  "Sorry, there was an error in uploading your file.";
        }
    }
}
?>
<div class="inner" style="min-height: 700px; overflow:scroll;">
    <div class="row">
        <div class="col-sm-12">
            <h1 class="text-center" style="text-align:left; color:#333333;font-size: 24px;"><b>PROFILE</b></h1>
        </div>
    </div>
    <hr>
    <div class="row" style="    margin-top: 48px;">
        <div class="col-sm-6">
        <div class="row">
            <img src="assets/img/profile/<?php echo $profile['image'] ;?>" 
            style="width:140px;height:auto; border:5px solid grey;     margin-left: 70px;">
            

            <div style="color: black; margin-left:10px; padding-left: 13px; margin-top: 36px; font-size: 18px;">

                <table class="space" style="border-collapse: separate; border-spacing: 15px;">
                    <tr>
                        <th>Name:-</th>
                        <td id="name"> <?php echo $profile['name']; ?> </td>
                    </tr>
                    <tr style="">
                        <th>Email:-</th>
                        <td id="email"> <?php echo $profile['email'] ?> </td>
                    </tr>
                    <tr>
                        <th>MOBILE:</th>
                        <td id="mobile"> <?php echo $profile['phone'] ?></td>
                    </tr>
                    <tr>
                        <th>About Me:-</th>
                        <td id="heading"> <?php echo $profile['role']; ?></td>
                    </tr>
                </table>
               
                <div id="updatePassword">
                    <!-- Popup Div Starts Here -->
                    <div id="popupUpdate" class="popup">
                        <!-- Contact Us Form -->
                        <form id="form" method="post" name="form">
                            <img id="close" src="assets/img/close.png" onclick="div_hide('updatePassword')">
                            <h2>Change Password</h2>
                             <hr>
                                <input name="currentPassword" placeholder="Currenet Password" type="password">
                                <hr>
                                <input name="newPassword" placeholder="New Password" type="password">
                                <input name="confirmPassword" placeholder="Confirm-Password" type="password">
                                 <!--<textarea id="msg" name="message" placeholder="Message"></textarea>-->
                                <input type="submit" id="submit" name="updatePassword" value="Update">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>                
                <div id="updateProfile" >
                    <div id="popupUpdate" class="popup">
                    <img id="close" src="assets/img/close.png" onclick="div_hide('updateProfile')">
                        <form id="form" method="post" name="form" enctype="multipart/form-data">
                            
                            <h2>Update Profile</h2>
                            <hr>
                            <input type="text" name="name" id="updatename" placeholder="Name">
                            <input id="updateemail" name="email" placeholder="Email" type="email">
                            <input id="updatemobile" name="mobile" placeholder="Mobile no." type="text" maxlength="10">
                            <label for="imageInput" class="btn text-muted text-center btn-success" style="width:82%;margin-top: 10px;">change Images</label>
                            <input id="imageInput" type="file" style="display:none" name="image">
                            <input type="submit" id="submit" name="updateProfile" value="Update">
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">   
            <div style="padding-left: 45px; margin-top: 20px; font-size: 12px;">                 
             <a  onclick="div_show('updateProfile')" style="cursor: pointer; color:#333333;" ><button type="submit" value="Assign"><b>Edit Profile</b></button></i></a>
             <a  onclick="div_show('updatePassword')" style="cursor: pointer; color:#333333;"><button type="submit" value="Assign"><b>Change Password</b></button></a><br>        
                </div>  
        </div>
       </div>
      <div class="col-sm-6">
        <div class="row">
            <form method="POST">
                <div class="form-group form-inline" style="width: 300px; margin-left: 70px;">
                    <input id="colorpicker" type="color" class="form-control" value="<?php echo $header['head_color']; ?>">
                    <div style="text-align: center;">--Choose Header color --</div>
                    <input type="text" id='headercolor' class="form-control" name="color" value="<?php echo $header['head_color']; ?>">
                    <input type="submit" id="submit" name="updateColor" value="Change" style="width: 25%;
    font-size: 14px;">
                </div>    
            </form>
            </div>
            
             <div class="row">
             <div style="margin-left: 110px;">
            <img src="<?php echo $logo_dir.$header['logo'] ;?>" style="width:100px;height:auto;">
            <form method="post" enctype="multipart/form-data" style="margin-left: 10px;"> 
              <input type="submit" name="updatelogo" style="display:none" id="updatelogo">
              <label for="logoinput" class="btn text-muted text-center btn-success" 
              style="  background-color: rgba(75, 80, 75, 0.75);
    border-color: rgba(75, 80, 75, 0.75);margin-left: -10px;">Update Logo</label>
              <input id="logoinput" type="file" style="display:none" 
              name="logo" onchange="document.getElementById('updatelogo').click()">
          </form>
          </div>
        </div>  
        </div>
        <div class="col-sm-1"></div>
   </div>
</div>
