 <?php
 //$image_dir = "assets/img/User/";
 
 function test_input($data)
    {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }
if (isset($_POST['addProduct'])) {
    $origin = test_input($_POST['source']);
    $destination = test_input($_POST['destination']);
    $ride_type = test_input($_POST['ride_type']);
    $ride_date = test_input($_POST['ride_date']);
    $fare = test_input($_POST['fare']);
    $time = test_input($_POST['time']);
    $payment_mode = test_input($_POST['payment_mode']);
    $passenger_name = test_input($_POST['passenger_name']);
    $passenger_phone = test_input($_POST['passenger_phone']);
    $number_passenger = test_input($_POST['number_passenger']);
    $driver_name = test_input($_POST['driver_name']);
    $driver_phone = test_input($_POST['driver_phone']);
    
            
            $sql = "INSERT INTO trips (source,destination,ride_type,ride_date,fare,time,payment_mode,,passenger_name,passenger_phone,driver_name,driver_phone) VALUES ('$origin','$destination','$ride_type','$ride_date','$fare','$time','$payment_mode','$passenger_name','$passenger_phone','$driver_name','$driver_phone')";
            if ($conn->query($sql) === TRUE)
            {
               $responseMessage =  "Booking Added Successfully";
            }
            else
            {
                $responseMessage =  "Connection failed: " . $conn->connect_error;
            }
       
    }

if (isset($_POST['updateProduct'])) {
   $origin = test_input($_POST['source']);
    $destination = test_input($_POST['destination']);
    $ride_type = test_input($_POST['ride_type']);
    $ride_date = test_input($_POST['ride_date']);
    $fare = test_input($_POST['fare']);
    $time = test_input($_POST['time']);
    $payment_mode = test_input($_POST['payment_mode']);
    $passenger_name = test_input($_POST['passenger_name']);
    $passenger_phone = test_input($_POST['passenger_phone']);
    $number_passenger = test_input($_POST['number_passenger']);
    $driver_name = test_input($_POST['driver_name']);
    $driver_phone = test_input($_POST['driver_phone']);
    $booking_id = test_input($_POST['booking_id']);
    $status = 1;
   
    if ($status)
    {
        $set .= "pickup_point='$pickup_point', destination ='$destination' , ride_type ='$ride_type', ride_date='$ride_date', fare='$fare', time='$time', payment_mode='$payment_mode' ,driver_name='$driver_name', passenger_name='$passenger_name', passenger_phone='$passenger_phone', driver_phone='$driver_phone' ";
        $sql = "UPDATE trips SET $set WHERE booking_id = $booking_id";
        if ($conn->query($sql) === TRUE)
        {
           $responseMessage =  "Booking Updated  Successfully";
        }
        else
        {
            $responseMessage =  "Connection failed: " . $conn->connect_error;
        }
    }
}
if (isset($_POST['deleteProduct']))
{
    $booking_id = test_input($_POST['booking_id']);
    
    $sql = "DELETE FROM trips WHERE booking_id=$booking_id";
    if ($conn->query($sql) === TRUE)
    {
        //unlink($image_dir.$deleteimage);
        
        $responseMessage =  "Detail Remove Successfully";
    }
    else
    {
        $responseMessage =  "Connection failed: " . $conn->connect_error;
    }

}
  ?>
<div class="inner" style="min-height: 800px;">
                <div class="row">
                    <div class="col-lg-12">
<div class="pull-left">
                        <h2 style="margin-top:25px;font-size: 20px;"><b>INSTANT BOOKING LIST</b> </h2>
                        </div>
                        <div class="pull-right">
                            <input type="text" id="myInput" onkeyup="changeSearch()" placeholder="Search here.." title="Type in a name" style="margin-right: 100px;width: 137px;margin-top: 22px;">
                        <!-- <input type="text" id="searchfor" placeholder="Search Here.." title="Type in a name" style="margin-right: 100px;width: 137px;margin-top: 22px;"> -->

                <button id="popup" class="btn text-muted text-center btn-success" onclick="div_show('addInstant')" style="width: 80px; margin-top:-30px;border-radius: 5px;font-size:12px; margin-left: 150px;">Add Booking</button>
                    </div>
                    </div>

                </div>

                <hr />
                <div class="row">
                    <div class="col-lg-12">
                        <div class="">

                            <div class="">
                                <div class="table-responsive"style=" width: 100%; overflow:scroll; max-height: 450px;">
                                    <table class="table table-striped table-bordered table-hover" style="margin-top: 10px; text-align: center;">
                                        <thead style="">
                                            <tr>
                                                <th>Booking Id</th>
                                                
                                   <th style="text-align: center;">Source</th>
                                    <th style="text-align: center;">Destination</th>
                                    <th style="text-align: center;">Ride Type</th>
                                    <th style="text-align: center;">Ride Date</th>
                                    <th style="text-align: center;">Fare</th>
                                    <th style="text-align: center;">Time</th>
                                    <th style="text-align: center;">Payment Mode</th>
                                    <th style="text-align: center;">Passenger Name</th>
                                    <th style="text-align: center;">Passenger Phone No.</th>
                                    <th style="text-align: center;">Driver Name</th>
                                    <th style="text-align: center;">Driver Phone No.</th>
                                    <th style="text-align: center;">Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                <?php 
                                    $sql = "SELECT * from trips";
                                    $result = $conn->query($sql);
                                    if ($result->num_rows>0)
                                    {
                                        $serial=1;
                                        
                                        while($products = $result->fetch_assoc())
                                        {
                                            
                                    ?>
                                    <tr class="tosearch" id="<?php  echo $products['booking_id'];?>">
                                        <td style="text-align: center;"><?php echo $serial; ?></td>
                                        
                                        <td style="text-align: left;" class="pickup"><?php  echo $products['source'];?></td>
                                        <td style="text-align: left;" class="destination"><?php  echo $products['destination'];?></td>
                                        <td style="text-align: center;" class="type"><?php  echo $products['ride_type'];?></td>
                                        <td style="text-align: center;" class="date"><?php  echo $products['ride_date'];?></td>
                                        <td style="text-align: center;" class="fare"><?php  echo $products['fare'];?></td>
                                        <td style="text-align: center;" class="time"><?php  echo $products['time'];?></td>
                                        <td style="text-align: center;" class="payment"><?php  echo $products['payment_mode'];?></td>
                                        <td style="text-align: center;" class="pname"><?php  echo $products['passenger_name'];?></td>
                                        <td style="text-align: center;" class="pphone"><?php  echo $products['passenger_phone'];?></td>
                                        <td style="text-align: center;" class="dname"><?php  echo $products['driver_name'];?></td>
                                        <td style="text-align: center;" class="dphone"><?php  echo $products['driver_phone'];?></td>
                                        <td style="font-size: 15px; text-align: center">
                                        <!--<a class="<?php  echo $products['booking_id'];?>" onclick="div_show('updateInstant',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;">UPDATE</a>/--><a class="<?php  echo $products['booking_id'];?>" onclick="div_show('deleteInstant',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;">DELETE</a></td>
                                    </tr>
                                    <?php
                                        $serial++;
                                         } } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="addInstant">
                    <!-- Popup Div Starts Here -->
                    <div id="popupAdd" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('addInstant')">
                        <form  id="form" method="post" name="form" enctype="multipart/form-data">
                            
                            <h2>Add Booking</h2>
                            <hr>
                            <input id="pickup_point" name="pickup_point" placeholder=" Pickup Point" type="text" required>
                            <input id="destination" name="destination" placeholder="Destination" type="text" required>
                            <input id="ride_type" name="ride_type" placeholder="Ride Type" type="text"required>
                           <input type="date" id="date" name="ride_date" placeholder="Booking Date" required>
                          
                            <input type="text" id="fare" name="fare" placeholder="Fare">
                            <input type="text" id="time" name="time" placeholder="Time">
                            <input type="text" id="payment_mode" name="payment_mode" placeholder="Payment Mode" required>
                            <input type="text" id="pname" name="passenger_name" placeholder="Passenger name" required>
                            <input type="text" id="pphone" name="passenger_phone" placeholder="Passenger Phone" required>
                            <input type="text" id="driver_name" name="driver_name" placeholder="Driver Name" required>
                            <input type="text" id="driver_phone" name="driver_phone" placeholder="Driver Phone" required>
                            <input type="submit" id="submit" name="addProduct" value="Add">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>
                <div id="updateInstant">
                    <!-- Popup Div Starts Here -->
                    <div id="popupUpdate" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('updateInstant')">
                        <form  id="form" method="post" name="form" enctype="multipart/form-data">
                            
                            <h2>Update Booking</h2>
                            <hr>
                            <input id="updatepickup" name="pickup_point" placeholder=" Pickup Point" type="text">
                            <input id="updatedestination" name="destination" placeholder="Destination" type="text">
                            <input id="updatetype" name="ride_type" placeholder="Ride Type" type="text">
                           <input id="updatedate" type="date"  name="ride_date" placeholder="Booking Date">
                          
                            <input id="updatefare" type="text"  name="fare" placeholder="Fare">
                            <input id="updatetime" type="text"  name="time" placeholder="Time">
                            <input id="updatepayment" type="text"  name="payment_mode" placeholder="Payment Mode">
                            <input id="updatepname" type="text"  name="passenger_name" placeholder="Passenger Name" >
                            <input id="updatepphone" type="text"  name="passenger_phone" placeholder="Passenger phone" >
                            <input id="updatedname" type="text"  name="driver_name" placeholder="Driver Name"> 
                            <input id="updatedphone" type="text"  name="driver_phone" placeholder="Driver Phone" required>                          
                            <input id="updateId" type="hidden" name="booking_id">
                            <input type="submit" id="submit" name="updateProduct" value="Update">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>
                 <!-- Display Popup Button -->
                <div id="deleteInstant">
                    <!-- Popup Div Starts Here -->
                    <div id="popupDelete" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('deleteInstant')">
                        <form method="post">
                            <hr>
                            <h2>Are You Sure??</h2>
                            <input type="submit" name="deleteProduct" value="OK">
                            <input type="hidden" name="booking_id" id="deleteId">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>
                <!--POP-->
            </div>
        </div>
    </div>           
</div>
<script>
function changeSearch() 
{
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("table");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) 
  {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) 
    {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) 
      {
        tr[i].style.display = "";
      } 
      else 
      {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>
