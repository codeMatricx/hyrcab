 <?php
 include "database.php";
 $tripId = $_SERVER['REQUEST_URI'];
 $tripIdfnd = explode("=", $tripId);
 $tripId = $tripIdfnd['1'];
 ?>
<div class="inner" style="min-height: 500px;">
                <div class="row">
                    <div class="col-lg-12">

                        <div class="pull-left">
                        <h2 style="margin-top: 25px;font-size: 20px;"><b>TRIPS</b></h2>
                        </div>
                        <div class="pull-right">
                        <input type="text" id="myInput"  onkeyup="myFunction()" placeholder="Driver Phone No.." title="Type in a phone number" style=" margin-right: 100px;width: 137px;margin-top: 22px;" >
                    </div>
                    </div>

                </div>

                <hr />
                <div class="row">
                    <div class="col-lg-12">
                        <div class="">

                            <div class="">
                                <div class="table-responsive"style=" width: 100%; overflow:scroll; min-height: 600px;">
                                    <table id="myTable" class="table table-striped table-bordered table-hover" style="margin-top: 10px; text-align: center;">
                                        <thead style="">
                                            <tr>
                                                <th>Booking ID</th>
                                    <th style="text-align: center;">Driver Phone No.</th>           
                                    <th style="text-align: center;">Ride Type</th>                                   
                                    <th style="text-align: center;">Ride Date</th>                                  
                                    <th style="text-align: center;">Driver Name</th>
                                    <th style="text-align: center;">Passenger Name</th>
                                    <th style="text-align: center;">Fare</th>
                                    <th style="text-align: center;">Vehicle Type</th>
                                    <th style="text-align: center;">Vehicle No.</th>
                                    <th style="text-align: center;">Taken Time</th>
                                    <th style="text-align: center;">Source</th>
                                    <th style="text-align: center;">Destination</th>
                                    <th style="text-align: center;">Number Of Passenger</th>
                                    <th style="text-align: center;">Vehicle Name</th>
                                    <th style="text-align: center;">Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                        //$id=$_GET['driver_id'];
                                        $sql = "SELECT t.*,d.name,r.name AS passengerName from trips AS t INNER JOIN driver AS d ON t.driver_id=d.user_id INNER JOIN rider AS r ON t.passenger_id=r.user_id WHERE d.user_id='$tripId'";
                                        $result = $conn->query($sql);
                                        if ($result->num_rows>0)
                                        {
                                           
                                        $serial=1;

                                        while($trip = $result->fetch_assoc())
                                        {


                                        ?>
                                    <tr class="tosearch" id="<?php  echo $trip['booking_id'];?>">
                                        <td style="text-align: center;"><?php echo $serial; ?></td>
                                        <td style="text-align: left;" class="dphone"><?php  echo $trip['driver_phone'];?></td>
                                        <td style="text-align: left;" class="type"><?php  echo $trip['ride_type'];?></td>                                       
                                        <td style="text-align: center;" class="date"><?php  echo $trip['ride_date'];?></td>                                     
                                        <td style="text-align: center;" class="dname"><?php  echo $trip['name'];?></td>
                                        <td style="text-align: center;" class="pname"><?php  echo $trip['passengerName'];?></td>
                                        <td style="text-align: center;" class="fare"><?php  echo $trip['fare'];?></td>
                                        <td style="text-align: center;" class="taxi"><?php  echo $trip['vehicle_type'];?></td>
                                        <td style="text-align: center;" class="vnumber"><?php  echo $trip['vehicle_number'];?></td>
                                        <td style="text-align: center;" class="time"><?php  echo $trip['time'];?></td>
                                        <td style="text-align: center;" class="source"><?php  echo $trip['source'];?></td>
                                        <td style="text-align: center;" class="destination"><?php  echo $trip['destination'];?></td>
                                        <td style="text-align: center;" class="npassenger"><?php  echo $trip['number_passenger'];?></td>
                                        <td style="text-align: center;" class="vname"><?php  echo $trip['vehicle_name'];?></td>
                                        
                                        <td style="font-size: 15px; text-align: center">
                                        <a class="<?php  echo $trip['booking_id'];?>" onclick="div_show('updateTrip',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;">UPDATE</a><br/><a class="<?php  echo $trip['booking_id'];?>" onclick="div_show('deleteTrip',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;">DELETE</a></td>
                                    </tr>
                                    <?php
                                        $serial++;
                                         } } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>           
</div>

<script>
function myFunction() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>