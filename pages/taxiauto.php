 <?php
 $image_dir = "assets/img/User/";
 
 function test_input($data)
    {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }
if (isset($_POST['addProduct'])) {
   $taxi_name = test_input($_POST['taxi_name']);
    $model_name = test_input($_POST['model_name']);
    $driver_name = test_input($_POST['driver_name']);
	
	$vehicle_number= test_input($_POST['vehicle_number']);
	$vehicle_registration_number = test_input($_POST['vehicle_registration_number']);
    $status = 1;

    $image_file = $image_dir . basename($_FILES["image"]["name"]);
    $imageFileType = strtolower(pathinfo($image_file,PATHINFO_EXTENSION));
    

    // Check if image file is a actual image or fake image
    $check = getimagesize($_FILES["image"]["tmp_name"]);

    if($check == false)
    {
        $status = 0;
    }
    // Check if file already exists
    if (file_exists($image_file)) {
        
		$status = 0;
		
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" )
    {
        $status = 0;
    }
    // Check if $status is set to 0 by an error
    if ($status == 0) 
    {
        $responseMessage = "Sorry, Your file already exist.";
    // if everything is ok, try to upload file
    } 
    else
    {
        $imageupload = move_uploaded_file($_FILES["image"]["tmp_name"], $image_file);
        
        if ($imageupload)
        {
            $image = $_FILES['image']['name'];
            
            $sql = "INSERT INTO taxi_auto (taxi_name, model_name, driver_name,vehicle_number,vehicle_registration_number,image) VALUES ('$taxi_name','$model_name','$driver_name','$vehicle_number','$vehicle_registration_number','$image')";
            if ($conn->query($sql) === TRUE)
            {
               $responseMessage =  "Vehicle Added successfully";
            }
            else
            {
                $responseMessage =  "Connection failed: " . $conn->connect_error;
            }
        }
        else
        {
            $responseMessage =  "Sorry, there was an error in uploading your file.";
        }
    }
}
if (isset($_POST['updateProduct'])) {
    $taxi_name = test_input($_POST['taxi_name']);
    $model_name = test_input($_POST['model_name']);
    $driver_name = test_input($_POST['driver_name']);
	//$imagename = test_input($_POST['image']);
	//$status1 = test_input($_POST['status']);
	$vehicle_number = test_input($_POST['vehicle_number']);
	$vehicle_registration_number = test_input($_POST['vehicle_registration_number']);
    $id = test_input($_POST['id']);
    $status = 1;
    $imagestatus = 1;
   
    $set = "";
    if (empty($taxi_name) || empty($model_name) || empty($driver_name) || empty($vehicle_number) || empty($vehicle_registration_number)) {
        $status=0;
    }
   
    
    
    if (!empty($_FILES['image']['name']) && $status)
        {
            $imagename = $_FILES['image']['name'];
            
            
            $image_file = $image_dir . basename($_FILES["image"]["name"]);
            $imageFileType = strtolower(pathinfo($image_file,PATHINFO_EXTENSION));
            
            
            // Check if image file is a actual image or fake image
            $check = getimagesize($_FILES["image"]["tmp_name"]);
            if($check == false)
            {
                $imagestatus = 0;
            }
            // Check if file already exists
            if (file_exists($image_file)) {
                $imagestatus = 0;
            }
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" )
            {
                $imagestatus = 0;
            }
            if($imagestatus)
            {
                $imageupload = move_uploaded_file($_FILES["image"]["tmp_name"], $image_file);
                if ($imageupload)
                {
                    $sql = "SELECT image from taxi where id = $id";
                    $result = $conn->query($sql);
                    if ($result->num_rows>0)
                    {
                        $data = $result->fetch_assoc();
                        $oldname = $data['image'];
                        unlink($image_dir.$oldname);
                    }
                    $set .="image = '$imagename',"; 
                }
            }     
        }
    if ($status)
    {
        $set .= "taxi_name = '$taxi_name', model_name = '$model_name', driver_name = '$driver_name' , vehicle_registration_number = '$vehicle_registration_number'";
        $sql = "UPDATE taxi_auto SET $set WHERE id = $id";
        if ($conn->query($sql) === TRUE)
        {
           $responseMessage =  "Detail Updated successfully";
        }
        else
        {
            $responseMessage =  "Connection failed: " . $conn->connect_error;
        }
    }
}
if (isset($_POST['deleteProduct']))
{
    $id = test_input($_POST['id']);
    $sql = "SELECT image from taxi_auto where id = $id";
    $result = $conn->query($sql);
    if ($result->num_rows>0)
    {
        $data = $result->fetch_assoc();
        $deleteimage = $data['image'];
        
    }
    $sql = "DELETE FROM taxi_auto WHERE id=$id";
    if ($conn->query($sql) === TRUE)
    {
        unlink($image_dir.$deleteimage);
        
        $responseMessage =  "Detail Remove successfully";
    }
    else
    {
        $responseMessage =  "Connection failed: " . $conn->connect_error;
    }

}
  ?>
<div class="inner" style="min-height: 500px;">
                <div class="row">
                    <div class="col-lg-12">

                      <div class="pull-left">
                        <h2 style="margin-top: 25px;font-size: 20px;"><b>AUTO</b></h2>
						</div>
						<div class="pull-right">
                        <input type="text" id="searchfor" placeholder="Search Here.." title="Type in a name" style=" margin-right: 100px;width: 137px;margin-top: 22px;">

                <button id="popup" class="btn text-muted text-center btn-success" onclick="div_show('addTaxi')" style="width: 80px; margin-top:-30px;border-radius: 5px;font-size:13px; margin-left: 148px;">Add Vehicle</button>
                    </div>
					</div>

                </div>

                <hr />
                <div class="row">
                    <div class="col-lg-12">
                        <div class="">

                            <div class="">
                                <div class="table-responsive"style=" width: 100%; overflow:scroll; min-height: 600px;">
                                    <table class="table table-striped table-bordered table-hover" style="margin-top: 10px; text-align: center;">
                                        <thead style="">
                                            <tr>
                                                <th>S.No.</th>
                                                
                                    <th style="text-align: center;">Vehicle Name</th>
                                    <th style="text-align: center;">Model Name</th>
                                    <th style="text-align: center;">Driver Name</th>
                                    
                                    <th style="text-align: center;"> Vehicle No.</th>
									<th style="text-align: center;"> Vehicle Registration No.</th>
									<th style="text-align: center;"> Vehicle Registration Copy</th>
                                    <th style="text-align: center;">Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                <?php 
                                    $sql = "SELECT * from taxi_auto";
                                    $result = $conn->query($sql);
                                    if ($result->num_rows>0)
                                    {
                                        $serial=1;
                                        
                                        while($products = $result->fetch_assoc())
                                        {
                                            
                                    ?>
                                    <tr class="tosearch" id="<?php  echo $products['id'];?>">
                                        <td style="text-align: center;"><?php echo $serial; ?></td>
                                        
                                        <td style="text-align: left;" class="name"><?php  echo $products['taxi_name'];?></td>
                                        <td style="text-align: left;" class="mname"><?php  echo $products['model_name'];?></td>
                                        <td style="text-align: center;" class="dname"><?php  echo $products['driver_name'];?></td>
                                        <td style="text-align: center;" class="vnumber"><?php  echo $products['vehicle_number'];?></td>
                                        <td style="text-align: center;" class="vreg"><?php  echo $products['vehicle_registration_number'];?></td>
                                        
										
										<td style="text-align: center;"><img height="50px" width="50px" src="assets/img/User/<?php echo $products['image']; ?>"></td>
                                        <td style="font-size: 15px; text-align: center">
										<a class="<?php  echo $products['id'];?>" onclick="div_show('updateTaxi',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;">UPDATE</a>/<a class="<?php  echo $products['id'];?>" onclick="div_show('deleteTaxi',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;">DELETE</a></td>
                                    </tr>
                                    <?php
                                        $serial++;
                                         } } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="addTaxi">
                    <!-- Popup Div Starts Here -->
                    <div id="popupAdd" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('addTaxi')">
                        <form  id="form" method="post" name="form" enctype="multipart/form-data">
                            
                            <h2>Add Cab</h2>
                            <hr>
                            <input id="Name" name="taxi_name" placeholder="Vehicle Name" type="text" required>
                            <input id="model_name" name="model_name" placeholder="Model Name" type="text" required>
                            <input id="driver_name" name="driver_name" placeholder="Driver Name" type="text" required>
                           
						   <input type="text" id="vehicle_number" name="vehicle_number" placeholder="Vehicle No."  maxlength="10" required>
						   <input type="text" id="vehicle_registration_number" name="vehicle_registration_number" placeholder="Vehicle Registration No." maxlength="20" required>
                            <label for="newimage" class="btn text-muted text-center btn-success" style="width:82%;margin-top: 10px;">Upload  Copy</label>                                                   
                            <input id="newimage" type="file" style="display:none" name="image">
                            <input type="submit" id="submit" name="addProduct" value="Add">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>
                <div id="updateTaxi">
                    <!-- Popup Div Starts Here -->
                    <div id="popupUpdate" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('updateTaxi')">
                        <form  id="form" method="post" name="form" enctype="multipart/form-data">
                            
                            <h2>Update Cab</h2>
                            <hr>
                            <input id="updatename" name="taxi_name" placeholder="Vehicle Name" type="text" >
                            <input id="updatemname" name="model_name" placeholder="Model Name" type="text">
                            <input id="updatedname" name="driver_name" placeholder="Driver Name" type="text">                         
                            <input id="updatevnumber" type="text"  name="vehicle_number" placeholder="Vehicle No." maxlength="10">
						   <input id="updatevreg" type="text"  name="vehicle_registration_number" placeholder="Vehicle Registration No." maxlength="20">
                            <label for="imageInput" class="btn text-muted text-center btn-success" style="width:82%;margin-top: 10px;">Upload DL Copy</label>                                                        
                            <input id="imageInput" type="file" style="display:none" name="image">
                            <input id="updateId" type="hidden" name="id">
                            <input type="submit" id="submit" name="updateProduct" value="Update">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>
                 <!-- Display Popup Button -->
                <div id="deleteTaxi">
                    <!-- Popup Div Starts Here -->
                    <div id="popupDelete" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('deleteTaxi')">
                        <form method="post">
                            <hr>
                            <h2>Are You Sure??</h2>
                            <input type="submit" name="deleteProduct" value="OK">
                            <input type="hidden" name="id" id="deleteId">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>
                <!--POP-->
            </div>
        </div>
    </div>           
</div>