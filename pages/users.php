 <?php
 $image_dir = "assets/img/User/";
 
 function test_input($data)
    {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }
if (isset($_POST['addProduct'])) {
    $name = test_input($_POST['name']);
    $email = test_input($_POST['email']);
    $phone = test_input($_POST['phone']);
    $password = test_input($_POST['password']);
    $role = test_input($_POST['role']);
    $status = 1;

    $image_file = $image_dir . basename($_FILES["image"]["name"]);
    $imageFileType = strtolower(pathinfo($image_file,PATHINFO_EXTENSION));
    

    // Check if image file is a actual image or fake image
    $check = getimagesize($_FILES["image"]["tmp_name"]);

    if($check == false)
    {
        $status = 0;
    }
    // Check if file already exists
    if (file_exists($image_file)) {
        
		$status = 0;
		
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" )
    {
        $status = 0;
    }
    // Check if $status is set to 0 by an error
    if ($status == 0) 
    {
        $responseMessage = "Sorry, Your file already exist.";
    // if everything is ok, try to upload file
    } 
    else
    {
        $imageupload = move_uploaded_file($_FILES["image"]["tmp_name"], $image_file);
        
        if ($imageupload)
        {
            $image = $_FILES['image']['name'];
            
            $sql = "INSERT INTO user (name,email,phone,password,image,role) VALUES ('$name','$email','$phone','$password','$image','$role')";
            if ($conn->query($sql) === TRUE)
            {
               $responseMessage =  "Details Added successfully";
            }
            else
            {
                $responseMessage =  "Connection failed: " . $conn->connect_error;
            }
        }
        else
        {
            $responseMessage =  "Sorry, there was an error in uploading your file.";
        }
    }
}
if (isset($_POST['updateProduct'])) {
    $name = test_input($_POST['name']);
    $email = test_input($_POST['email']);
    $phone = test_input($_POST['phone']);
    $password = test_input($_POST['password']);
    $role = test_input($_POST['role']);
    $id = test_input($_POST['id']);
    $status = 1;
    $imagestatus = 1;
   
    $set = "";
    if (empty($name) || empty($email) || empty($phone) || empty($password) || empty($role)) {
        $status=0;
    }
   
    
    
    if (!empty($_FILES['image']['name']) && $status)
        {
            $imagename = $_FILES['image']['name'];
            
            
            $image_file = $image_dir . basename($_FILES["image"]["name"]);
            $imageFileType = strtolower(pathinfo($image_file,PATHINFO_EXTENSION));
            
            
            // Check if image file is a actual image or fake image
            $check = getimagesize($_FILES["image"]["tmp_name"]);
            if($check == false)
            {
                $imagestatus = 0;
            }
            // Check if file already exists
            if (file_exists($image_file)) {
                $imagestatus = 0;
            }
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" )
            {
                $imagestatus = 0;
            }
            if($imagestatus)
            {
                $imageupload = move_uploaded_file($_FILES["image"]["tmp_name"], $image_file);
                if ($imageupload)
                {
                    $sql = "SELECT image from driver where id = $id";
                    $result = $conn->query($sql);
                    if ($result->num_rows>0)
                    {
                        $data = $result->fetch_assoc();
                        $oldname = $data['image'];
                        unlink($image_dir.$oldname);
                    }
                    $set .="image = '$imagename',"; 
                }
            }     
        }
    if ($status)
    {
        $set .= "name = '$name',email='$email', phone = '$phone', password = '$password', role = '$role'";
        $sql = "UPDATE user SET $set WHERE id = $id";
        if ($conn->query($sql) === TRUE)
        {
           $responseMessage =  "Details updated successfully";
        }
        else
        {
            $responseMessage =  "Connection failed: " . $conn->connect_error;
        }
    }
}
if (isset($_POST['deleteProduct']))
{
    $id = test_input($_POST['id']);
    $sql = "SELECT image from user where id = $id";
    $result = $conn->query($sql);
    if ($result->num_rows>0)
    {
        $data = $result->fetch_assoc();
        $deleteimage = $data['image'];
        
    }
    $sql = "DELETE FROM user WHERE id=$id";
    if ($conn->query($sql) === TRUE)
    {
        unlink($image_dir.$deleteimage);
        
        $responseMessage =  "Details Remove successfully";
    }
    else
    {
        $responseMessage =  "Connection failed: " . $conn->connect_error;
    }

}
  ?>
<div class="inner" style="min-height: 500px;">
                <div class="row">
                    <div class="col-lg-12">
<div class="pull-left">
                        <h2 style="margin-top: 25px;font-size:20px;"><b>ADMIN</b></h2>
						</div>
						<div class="pull-right">
                        <input type="text" id="searchfor" placeholder="Search Here.." title="Type in a name" style=" margin-right: 100px;width: 137px;margin-top: 22px;">

                <button id="popup" class="btn text-muted text-center btn-success" onclick="div_show('addAdmin')" style="width: 70px; margin-top:-30px;border-radius: 5px;font-size:13px; margin-left: 162px;">Add</button>
                    </div>
					</div>

                </div>

                <hr />
                <div class="row">
                    <div class="col-lg-12">
                        <div class="">

                            <div class="">
                                <div class="table-responsive"style=" width: 100%; overflow:scroll; min-height: 600px;">
                                    <table class="table table-striped table-bordered table-hover" style="margin-top: 10px; text-align: center;">
                                        <thead style="">
                                            <tr>
                                                <th>S.No.</th>
                                                
                                   <th style="text-align: center;">Name</th>
                                    <th style="text-align: center;">Email</th>
                                    <th style="text-align: center;">Phone</th>
                                    <th style="text-align: center;">Password</th>
                                    <th style="text-align: center;">Image</th>
                                    <th style="text-align: center;">Role</th>
                                    <th style="text-align: center;">Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                <?php 
                                    $sql = "SELECT * from user";
                                    $result = $conn->query($sql);
                                    if ($result->num_rows>0)
                                    {
                                        $serial=1;
                                        
                                        while($products = $result->fetch_assoc())
                                        {
                                            
                                    ?>
                                    <tr class="tosearch" id="<?php  echo $products['id'];?>">
                                        <td style="text-align: center;"><?php echo $serial; ?></td>
                                        
                                        <td style="text-align: left;" class="name"><?php  echo $products['name'];?></td>
                                        <td style="text-align: left;" class="email"><?php  echo $products['email'];?></td>
                                        <td style="text-align: center;" class="phone"><?php  echo $products['phone'];?></td>
                                        <td style="text-align: center;" class="pwd"><?php  echo $products['password'];?></td>
                                        <td style="text-align: center;" class="role"><?php  echo $products['role'];?></td>
                                        
										<td style="text-align: center;"><img height="50px" width="50px" src="assets/img/User/<?php echo $products['image']; ?>"></td>
                                        <td style="font-size: 15px; text-align: center">
										<a class="<?php  echo $products['id'];?>" onclick="div_show('updateAdmin',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;">UPDATE</a>/<a class="<?php  echo $products['id'];?>" onclick="div_show('deleteAdmin',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;">DELETE</a></td>
                                    </tr>
                                    <?php
                                        $serial++;
                                         } } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="addAdmin">
                    <!-- Popup Div Starts Here -->
                    <div id="popupAdd" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('addAdmin')">
                        <form  id="form" method="post" name="form" enctype="multipart/form-data">
                            
                            <h2>Add Admin</h2>
                            <hr>
                           <input id="Name" name="name" placeholder=" User Full Name" type="text" required>
                            <input id="Email" name="email" placeholder="Email" type="email" required>
                            <input id="phone" name="phone" placeholder="Mobile Number" type="text" maxlength="10" >
                            <input type="password" id="password" name="password" placeholder="Password"><br>
                            <select name="role"  style="margin-top:20px;" placeholder="Role">
                                <option disabled selected>Select-Role</option>
                                <option>Admin</option>
                                
                            </select>
                            <label for="newimage" class="btn text-muted text-center btn-success" style="width:82%;margin-top: 10px;">Upload Image</label>                                                   
                            <input id="newimage" type="file" style="display:none" name="image">
                            <input type="submit" id="submit" name="addProduct" value="Add">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>
                <div id="updateAdmin">
                    <!-- Popup Div Starts Here -->
                    <div id="popupUpdate" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('updateAdmin')">
                        <form  id="form" method="post" name="form" enctype="multipart/form-data">
                            
                            <h2>Update Driver</h2>
                            <hr>
                           <input id="updatename" name="name" placeholder=" User Full Name" type="text">
                            <input id="updateemail" name="email" placeholder="Registered Email" type="email" required>
                            <input id="updatephone" name="phone" placeholder="Mobile Number" type="text" maxlength="10" >
                            <input id="updatepwd" type="text"  name="password" placeholder="Password"><br>
                            <select name="role"  style="margin-top:20px;" placeholder="Role">
                                <option disabled selected>Select-Role</option>
                                <option>Admin</option>
                                
                            </select>
                            <label for="imageInput" class="btn text-muted text-center btn-success" style="width:82%;margin-top: 10px;">Upload DL Copy</label>                                                        
                            <input id="imageInput" type="file" style="display:none" name="image">
                            <input id="updateId" type="hidden" name="id">
                            <input type="submit" id="submit" name="updateProduct" value="Update">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>
                 <!-- Display Popup Button -->
                <div id="deleteAdmin">
                    <!-- Popup Div Starts Here -->
                    <div id="popupDelete" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('deleteAdmin')">
                        <form method="post">
                            <hr>
                            <h2>Are You Sure??</h2>
                            <input type="submit" name="deleteProduct" value="OK">
                            <input type="hidden" name="id" id="deleteId">
                        </form>
                    </div>
                    <!-- Popup Div Ends Here -->
                </div>
                <!--POP-->
            </div>
        </div>
    </div>           
</div>