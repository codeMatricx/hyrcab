-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jan 27, 2018 at 05:58 AM
-- Server version: 5.6.36-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hyrcabadmin`
--

-- --------------------------------------------------------

--
-- Table structure for table `assign_driver`
--

CREATE TABLE IF NOT EXISTS `assign_driver` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `driver_id` int(11) NOT NULL,
  `booking_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `passenger_id` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `is_trip_complete` tinyint(4) NOT NULL DEFAULT '0',
  `is_trip_start` varchar(10) NOT NULL DEFAULT '0',
  `is_trip_accept` varchar(10) NOT NULL DEFAULT '0',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=378 ;

--
-- Dumping data for table `assign_driver`
--

INSERT INTO `assign_driver` (`id`, `driver_id`, `booking_date`, `passenger_id`, `booking_id`, `is_trip_complete`, `is_trip_start`, `is_trip_accept`, `created_date_time`) VALUES
(376, 104, '2018-01-24 13:33:55', 69, 1269, 0, '0', '1', '2018-01-24 13:33:55'),
(377, 104, '2018-01-25 10:27:51', 69, 1272, 0, '0', '0', '2018-01-25 10:27:51');

-- --------------------------------------------------------

--
-- Table structure for table `cancel_booking`
--

CREATE TABLE IF NOT EXISTS `cancel_booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `driver_id` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `create_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `cancel_booking`
--

INSERT INTO `cancel_booking` (`id`, `driver_id`, `booking_id`, `create_date_time`) VALUES
(2, 104, 1134, '2018-01-17 10:03:38'),
(3, 104, 1133, '2018-01-17 10:05:04'),
(4, 104, 1172, '2018-01-22 07:16:24'),
(5, 104, 1172, '2018-01-22 07:16:25'),
(6, 104, 1211, '2018-01-22 11:11:37'),
(7, 104, 1211, '2018-01-22 11:11:37'),
(8, 104, 1211, '2018-01-22 11:13:46'),
(9, 104, 1211, '2018-01-22 11:13:46'),
(10, 104, 1214, '2018-01-22 11:26:42'),
(11, 104, 1214, '2018-01-22 11:26:43'),
(12, 104, 1222, '2018-01-22 12:18:54'),
(13, 104, 1222, '2018-01-22 12:18:55'),
(14, 104, 1227, '2018-01-22 12:47:52'),
(15, 104, 1227, '2018-01-22 12:47:52'),
(16, 104, 1230, '2018-01-22 14:15:41'),
(17, 104, 1230, '2018-01-22 14:15:41'),
(18, 104, 1230, '2018-01-22 14:15:53'),
(19, 104, 1230, '2018-01-22 14:15:54'),
(20, 104, 1230, '2018-01-22 14:16:02'),
(21, 104, 1230, '2018-01-22 14:16:02'),
(22, 104, 1230, '2018-01-22 14:18:50'),
(23, 104, 1230, '2018-01-22 14:18:50'),
(24, 104, 1230, '2018-01-22 14:19:15'),
(25, 104, 1230, '2018-01-22 14:19:16'),
(26, 104, 1230, '2018-01-22 14:19:36'),
(27, 104, 1230, '2018-01-22 14:19:36'),
(28, 104, 1230, '2018-01-22 14:20:40'),
(29, 104, 1230, '2018-01-22 14:20:41'),
(30, 104, 1230, '2018-01-22 14:21:28'),
(31, 104, 1230, '2018-01-22 14:21:29'),
(32, 104, 1239, '2018-01-24 08:24:38'),
(33, 104, 1239, '2018-01-24 08:24:39'),
(34, 104, 1254, '2018-01-24 09:53:57'),
(35, 104, 1254, '2018-01-24 09:53:58'),
(36, 104, 1254, '2018-01-24 09:55:00'),
(37, 104, 1254, '2018-01-24 09:55:00');

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE IF NOT EXISTS `driver` (
  `user_id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `driving_license` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `aadharcard_number` varchar(255) NOT NULL,
  `pan_number` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `aadhar_image` varchar(255) NOT NULL,
  `dl_image` varchar(255) NOT NULL,
  `vehicle_name` varchar(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL,
  `vehicle_doc` varchar(255) NOT NULL,
  `lat` float(15,11) NOT NULL,
  `lng` float(15,11) NOT NULL,
  `date` varchar(200) NOT NULL,
  `time` varchar(200) NOT NULL,
  `registration_date` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isapproved` tinyint(1) NOT NULL DEFAULT '0',
  `is_duty_on` tinyint(1) NOT NULL DEFAULT '1',
  `rcNumber` varchar(100) DEFAULT NULL,
  `drivingLicence` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=131 ;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`user_id`, `name`, `email`, `phone`, `driving_license`, `password`, `city`, `aadharcard_number`, `pan_number`, `image`, `aadhar_image`, `dl_image`, `vehicle_name`, `vehicle_number`, `vehicle_doc`, `lat`, `lng`, `date`, `time`, `registration_date`, `status`, `created_date_time`, `isapproved`, `is_duty_on`, `rcNumber`, `drivingLicence`) VALUES
(104, 'Abidkhan1', 'mohdabid75@gmail.com', '9205629660', 'DL56YT45367', 'e10adc3949ba59abbe56e057f20f883e', 'delhi', '456665678686', '', '', '', '', 'Suzuki', 'Mh04-Bh2020', '', 28.58837318420, 77.33406066895, '', '', '', 0, '2018-01-15 10:46:16', 1, 1, NULL, NULL),
(130, 'ashish singh', 'aasu147@gmail.com', '9896857485', 'UP3535HG9', 'e10adc3949ba59abbe56e057f20f883e', 'Noida', '676677t78897', '', '', '', '', '', '', '', 28.59020042419, 77.33441925049, '', '', '', 1, '2018-01-20 07:02:12', 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `driver_auto`
--

CREATE TABLE IF NOT EXISTS `driver_auto` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `signup_date` varchar(255) NOT NULL,
  `driving_license` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `aadharcard_number` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `driver_auto`
--

INSERT INTO `driver_auto` (`id`, `name`, `email`, `phone`, `signup_date`, `driving_license`, `password`, `city`, `aadharcard_number`, `image`) VALUES
(2, 'Sumit', 'sshailendra1211@gmail.com', '6578978890', '2017-10-16', 'MH23TR5463', '0987654', 'mumbai', '654323131231', '_DSC0049.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `driver_bike`
--

CREATE TABLE IF NOT EXISTS `driver_bike` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `signup_date` varchar(255) NOT NULL,
  `driving_license` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `aadharcard_number` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `driver_bike`
--

INSERT INTO `driver_bike` (`id`, `name`, `email`, `phone`, `signup_date`, `driving_license`, `password`, `city`, `aadharcard_number`, `image`) VALUES
(1, 'Ritesh', 'ravi@gmail.com', '9450991405', '2017-10-08', 'RT65KH8745', '123456', 'ghaziabad', '645675756786', '_DSC0107.jpg'),
(2, 'Sumit', 'sshailendra1211@gmail.com', '8967564345', '2017-10-16', 'MH23TR5463', '9877665445', 'mumbai', '877643453456', '_DSC0124.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `driver_verify`
--

CREATE TABLE IF NOT EXISTS `driver_verify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) NOT NULL,
  `otp` int(11) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=99 ;

--
-- Dumping data for table `driver_verify`
--

INSERT INTO `driver_verify` (`id`, `phone`, `otp`, `created_date_time`) VALUES
(97, '8077024382', 6167, '2018-01-20 07:59:08'),
(98, '8077024382', 1120, '2018-01-20 07:59:10');

-- --------------------------------------------------------

--
-- Table structure for table `emergency_contact`
--

CREATE TABLE IF NOT EXISTS `emergency_contact` (
  `contact_id` int(255) NOT NULL AUTO_INCREMENT,
  `contact_name` varchar(255) NOT NULL,
  `contact_phone` varchar(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Dumping data for table `emergency_contact`
--

INSERT INTO `emergency_contact` (`contact_id`, `contact_name`, `contact_phone`, `user_id`) VALUES
(28, 'rakeshjha', '9632587410', 63),
(29, 'rakeshjha', '9632587410', 63),
(30, 'ttggg', '8523698528', 64),
(31, 'ttggg', '8523698528', 64),
(32, 'ashu', '8432856958', 64),
(33, 'ashu', '8432856958', 64),
(34, 'saumya', '8439666778', 69);

-- --------------------------------------------------------

--
-- Table structure for table `header`
--

CREATE TABLE IF NOT EXISTS `header` (
  `id` int(250) NOT NULL AUTO_INCREMENT,
  `logo` varchar(40) NOT NULL,
  `head_color` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `header`
--

INSERT INTO `header` (`id`, `logo`, `head_color`) VALUES
(1, 'hyrcablogo.png', 'f9f004');

-- --------------------------------------------------------

--
-- Table structure for table `instant_booking`
--

CREATE TABLE IF NOT EXISTS `instant_booking` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `pickup_point` varchar(255) NOT NULL,
  `destination` varchar(255) NOT NULL,
  `ride_type` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `fare` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `payment_mode` varchar(255) NOT NULL,
  `signature` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `instant_booking_auto`
--

CREATE TABLE IF NOT EXISTS `instant_booking_auto` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `pickup_point` varchar(255) NOT NULL,
  `destination` varchar(255) NOT NULL,
  `ride_type` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `fare` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `payment_mode` varchar(255) NOT NULL,
  `signature` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `instant_booking_auto`
--

INSERT INTO `instant_booking_auto` (`id`, `pickup_point`, `destination`, `ride_type`, `date`, `fare`, `time`, `payment_mode`, `signature`, `driver_name`) VALUES
(1, 'noida', 'delhi', 'booking', '2017-10-07', '565', '50minutes', 'cash', 'sumit', 'Saumya');

-- --------------------------------------------------------

--
-- Table structure for table `instant_booking_bike`
--

CREATE TABLE IF NOT EXISTS `instant_booking_bike` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `pickup_point` varchar(255) NOT NULL,
  `destination` varchar(255) NOT NULL,
  `ride_type` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `fare` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `payment_mode` varchar(255) NOT NULL,
  `signature` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `instant_booking_bike`
--

INSERT INTO `instant_booking_bike` (`id`, `pickup_point`, `destination`, `ride_type`, `date`, `fare`, `time`, `payment_mode`, `signature`, `driver_name`) VALUES
(1, 'Delhi', 'Noida', 'share', '2017-10-12', '45', '45minutes', 'cash', 'sumit', 'Rahul');

-- --------------------------------------------------------

--
-- Table structure for table `late_booking_auto`
--

CREATE TABLE IF NOT EXISTS `late_booking_auto` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `passenger_name` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `destination` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `late_booking_auto`
--

INSERT INTO `late_booking_auto` (`id`, `passenger_name`, `date`, `pickup_location`, `destination`, `driver_name`, `ride_date`, `vehicle_number`) VALUES
(1, 'Sanjeev', '32/05/2017', 'noida', 'delhi', 'Sumit', '2017-10-14', 'DL34TD4355');

-- --------------------------------------------------------

--
-- Table structure for table `late_booking_bike`
--

CREATE TABLE IF NOT EXISTS `late_booking_bike` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `passenger_name` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `destination` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `late_booking_bike`
--

INSERT INTO `late_booking_bike` (`id`, `passenger_name`, `date`, `pickup_location`, `destination`, `driver_name`, `ride_date`, `vehicle_number`) VALUES
(1, 'Ritesh', '23/10/2017', 'noida', 'Ghaziabad', 'Saumya', '2017-10-10', 'UP16FT2345');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `session_id` varchar(255) NOT NULL,
  `device_token` varchar(255) NOT NULL,
  `device_type` varchar(255) NOT NULL,
  `user_type` int(11) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=456 ;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `user_id`, `session_id`, `device_token`, `device_type`, `user_type`, `created_date_time`) VALUES
(190, 130, '790', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 07:02:44'),
(191, 130, '590', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 07:02:45'),
(192, 69, '589', 'cZ9D0XS_iiQ:APA91bFkKLEN8xv-AV3ZkjVr1x9MaIAaIoOiu2zQMShkn4r6Af0iH_3tSpx03soIEPn9RUxirOw037gH3_bNMJkiclGyWMCr20OTwonqSzjk5FO87_TlM-qJYX3-xPU6l1odmmObzN9I', 'andriod', 2, '2018-01-20 07:04:50'),
(193, 69, '935', 'cZ9D0XS_iiQ:APA91bFkKLEN8xv-AV3ZkjVr1x9MaIAaIoOiu2zQMShkn4r6Af0iH_3tSpx03soIEPn9RUxirOw037gH3_bNMJkiclGyWMCr20OTwonqSzjk5FO87_TlM-qJYX3-xPU6l1odmmObzN9I', 'andriod', 2, '2018-01-20 07:04:51'),
(194, 130, '981', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 07:12:12'),
(195, 130, '670', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 07:12:13'),
(196, 130, '614', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 07:15:15'),
(197, 130, '496', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 07:15:15'),
(198, 130, '929', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 07:20:22'),
(199, 130, '664', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 07:20:35'),
(200, 130, '989', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 07:20:35'),
(201, 130, '834', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 07:31:38'),
(202, 130, '982', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 07:31:38'),
(203, 130, '670', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 07:33:25'),
(204, 130, '896', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 07:33:25'),
(205, 70, '824', 'cZ9D0XS_iiQ:APA91bFkKLEN8xv-AV3ZkjVr1x9MaIAaIoOiu2zQMShkn4r6Af0iH_3tSpx03soIEPn9RUxirOw037gH3_bNMJkiclGyWMCr20OTwonqSzjk5FO87_TlM-qJYX3-xPU6l1odmmObzN9I', 'andriod', 2, '2018-01-20 07:47:25'),
(206, 70, '628', 'cZ9D0XS_iiQ:APA91bFkKLEN8xv-AV3ZkjVr1x9MaIAaIoOiu2zQMShkn4r6Af0iH_3tSpx03soIEPn9RUxirOw037gH3_bNMJkiclGyWMCr20OTwonqSzjk5FO87_TlM-qJYX3-xPU6l1odmmObzN9I', 'andriod', 2, '2018-01-20 07:47:26'),
(207, 130, '903', 'dZJUMyJ7S4o:APA91bEcmdzYAcBCczNbGaYf-rQe7jEHP3I0qfXK4NfWOmnbFK6B6hSFFDbQXDEosIQdYMHpeq5bw7j6ZhLveV_XDxl736PUU3DhtnnMtnt2sEhoeITj6ES8RXBp0UqOerlU8eVv5I_p', 'andriod', 1, '2018-01-20 08:23:07'),
(208, 130, '920', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 08:23:23'),
(209, 130, '636', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 08:23:24'),
(210, 130, '613', 'dZJUMyJ7S4o:APA91bEcmdzYAcBCczNbGaYf-rQe7jEHP3I0qfXK4NfWOmnbFK6B6hSFFDbQXDEosIQdYMHpeq5bw7j6ZhLveV_XDxl736PUU3DhtnnMtnt2sEhoeITj6ES8RXBp0UqOerlU8eVv5I_p', 'andriod', 1, '2018-01-20 08:23:25'),
(211, 130, '609', 'dZJUMyJ7S4o:APA91bEcmdzYAcBCczNbGaYf-rQe7jEHP3I0qfXK4NfWOmnbFK6B6hSFFDbQXDEosIQdYMHpeq5bw7j6ZhLveV_XDxl736PUU3DhtnnMtnt2sEhoeITj6ES8RXBp0UqOerlU8eVv5I_p', 'andriod', 1, '2018-01-20 08:23:26'),
(212, 130, '985', 'dZJUMyJ7S4o:APA91bEcmdzYAcBCczNbGaYf-rQe7jEHP3I0qfXK4NfWOmnbFK6B6hSFFDbQXDEosIQdYMHpeq5bw7j6ZhLveV_XDxl736PUU3DhtnnMtnt2sEhoeITj6ES8RXBp0UqOerlU8eVv5I_p', 'andriod', 1, '2018-01-20 08:24:19'),
(213, 130, '667', 'dZJUMyJ7S4o:APA91bEcmdzYAcBCczNbGaYf-rQe7jEHP3I0qfXK4NfWOmnbFK6B6hSFFDbQXDEosIQdYMHpeq5bw7j6ZhLveV_XDxl736PUU3DhtnnMtnt2sEhoeITj6ES8RXBp0UqOerlU8eVv5I_p', 'andriod', 1, '2018-01-20 08:24:19'),
(214, 130, '450', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 08:24:23'),
(215, 130, '724', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 08:24:23'),
(216, 130, '657', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 08:30:29'),
(217, 130, '626', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 08:30:30'),
(218, 104, '996', 'dZJUMyJ7S4o:APA91bEcmdzYAcBCczNbGaYf-rQe7jEHP3I0qfXK4NfWOmnbFK6B6hSFFDbQXDEosIQdYMHpeq5bw7j6ZhLveV_XDxl736PUU3DhtnnMtnt2sEhoeITj6ES8RXBp0UqOerlU8eVv5I_p', 'andriod', 1, '2018-01-20 09:06:14'),
(219, 104, '944', 'dZJUMyJ7S4o:APA91bEcmdzYAcBCczNbGaYf-rQe7jEHP3I0qfXK4NfWOmnbFK6B6hSFFDbQXDEosIQdYMHpeq5bw7j6ZhLveV_XDxl736PUU3DhtnnMtnt2sEhoeITj6ES8RXBp0UqOerlU8eVv5I_p', 'andriod', 1, '2018-01-20 09:06:15'),
(220, 130, '977', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 11:23:19'),
(221, 130, '941', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 11:23:19'),
(222, 71, '548', 'cZ9D0XS_iiQ:APA91bFkKLEN8xv-AV3ZkjVr1x9MaIAaIoOiu2zQMShkn4r6Af0iH_3tSpx03soIEPn9RUxirOw037gH3_bNMJkiclGyWMCr20OTwonqSzjk5FO87_TlM-qJYX3-xPU6l1odmmObzN9I', 'andriod', 2, '2018-01-20 11:25:14'),
(223, 71, '909', 'cZ9D0XS_iiQ:APA91bFkKLEN8xv-AV3ZkjVr1x9MaIAaIoOiu2zQMShkn4r6Af0iH_3tSpx03soIEPn9RUxirOw037gH3_bNMJkiclGyWMCr20OTwonqSzjk5FO87_TlM-qJYX3-xPU6l1odmmObzN9I', 'andriod', 2, '2018-01-20 11:25:14'),
(224, 104, '886', 'dZJUMyJ7S4o:APA91bEcmdzYAcBCczNbGaYf-rQe7jEHP3I0qfXK4NfWOmnbFK6B6hSFFDbQXDEosIQdYMHpeq5bw7j6ZhLveV_XDxl736PUU3DhtnnMtnt2sEhoeITj6ES8RXBp0UqOerlU8eVv5I_p', 'andriod', 1, '2018-01-20 11:29:08'),
(225, 104, '523', 'dZJUMyJ7S4o:APA91bEcmdzYAcBCczNbGaYf-rQe7jEHP3I0qfXK4NfWOmnbFK6B6hSFFDbQXDEosIQdYMHpeq5bw7j6ZhLveV_XDxl736PUU3DhtnnMtnt2sEhoeITj6ES8RXBp0UqOerlU8eVv5I_p', 'andriod', 1, '2018-01-20 11:29:08'),
(226, 69, '750', 'fTVws3qe9dQ:APA91bG7tSWMfJBkDv4pWBypGzE1MZmXjqHJHGlLZ842MlD7xiuW3Iuv3BUWI4U0eLHE0_ExCSNPoWH1SvSn8Ys4lJ-u2IIRhq8Nb7I2e61UG6fCQIuFISZiQ0rZRBThht7tkSRVYgPH', 'andriod', 2, '2018-01-20 12:02:53'),
(227, 69, '523', 'fTVws3qe9dQ:APA91bG7tSWMfJBkDv4pWBypGzE1MZmXjqHJHGlLZ842MlD7xiuW3Iuv3BUWI4U0eLHE0_ExCSNPoWH1SvSn8Ys4lJ-u2IIRhq8Nb7I2e61UG6fCQIuFISZiQ0rZRBThht7tkSRVYgPH', 'andriod', 2, '2018-01-20 12:02:53'),
(228, 130, '938', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 12:06:56'),
(229, 130, '953', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 12:06:57'),
(230, 130, '507', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 12:12:01'),
(231, 130, '715', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 12:12:01'),
(232, 130, '924', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 12:31:11'),
(233, 130, '832', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-20 12:31:12'),
(234, 69, '639', 'fTVws3qe9dQ:APA91bG7tSWMfJBkDv4pWBypGzE1MZmXjqHJHGlLZ842MlD7xiuW3Iuv3BUWI4U0eLHE0_ExCSNPoWH1SvSn8Ys4lJ-u2IIRhq8Nb7I2e61UG6fCQIuFISZiQ0rZRBThht7tkSRVYgPH', 'andriod', 2, '2018-01-20 12:32:13'),
(235, 69, '719', 'fTVws3qe9dQ:APA91bG7tSWMfJBkDv4pWBypGzE1MZmXjqHJHGlLZ842MlD7xiuW3Iuv3BUWI4U0eLHE0_ExCSNPoWH1SvSn8Ys4lJ-u2IIRhq8Nb7I2e61UG6fCQIuFISZiQ0rZRBThht7tkSRVYgPH', 'andriod', 2, '2018-01-20 12:32:13'),
(236, 130, '461', 'dZJUMyJ7S4o:APA91bEcmdzYAcBCczNbGaYf-rQe7jEHP3I0qfXK4NfWOmnbFK6B6hSFFDbQXDEosIQdYMHpeq5bw7j6ZhLveV_XDxl736PUU3DhtnnMtnt2sEhoeITj6ES8RXBp0UqOerlU8eVv5I_p', 'andriod', 1, '2018-01-20 13:44:06'),
(237, 130, '562', 'dZJUMyJ7S4o:APA91bEcmdzYAcBCczNbGaYf-rQe7jEHP3I0qfXK4NfWOmnbFK6B6hSFFDbQXDEosIQdYMHpeq5bw7j6ZhLveV_XDxl736PUU3DhtnnMtnt2sEhoeITj6ES8RXBp0UqOerlU8eVv5I_p', 'andriod', 1, '2018-01-20 13:44:07'),
(238, 130, '996', 'dZJUMyJ7S4o:APA91bEcmdzYAcBCczNbGaYf-rQe7jEHP3I0qfXK4NfWOmnbFK6B6hSFFDbQXDEosIQdYMHpeq5bw7j6ZhLveV_XDxl736PUU3DhtnnMtnt2sEhoeITj6ES8RXBp0UqOerlU8eVv5I_p', 'andriod', 1, '2018-01-20 13:45:14'),
(239, 130, '645', 'dZJUMyJ7S4o:APA91bEcmdzYAcBCczNbGaYf-rQe7jEHP3I0qfXK4NfWOmnbFK6B6hSFFDbQXDEosIQdYMHpeq5bw7j6ZhLveV_XDxl736PUU3DhtnnMtnt2sEhoeITj6ES8RXBp0UqOerlU8eVv5I_p', 'andriod', 1, '2018-01-20 13:45:14'),
(240, 130, '497', 'dZJUMyJ7S4o:APA91bEcmdzYAcBCczNbGaYf-rQe7jEHP3I0qfXK4NfWOmnbFK6B6hSFFDbQXDEosIQdYMHpeq5bw7j6ZhLveV_XDxl736PUU3DhtnnMtnt2sEhoeITj6ES8RXBp0UqOerlU8eVv5I_p', 'andriod', 1, '2018-01-20 13:48:43'),
(241, 130, '486', 'dZJUMyJ7S4o:APA91bEcmdzYAcBCczNbGaYf-rQe7jEHP3I0qfXK4NfWOmnbFK6B6hSFFDbQXDEosIQdYMHpeq5bw7j6ZhLveV_XDxl736PUU3DhtnnMtnt2sEhoeITj6ES8RXBp0UqOerlU8eVv5I_p', 'andriod', 1, '2018-01-20 13:48:43'),
(242, 130, '712', 'eWJqPITS-4Q:APA91bEPkFjwv2FBBvBdTHlN-SlSBMD0Yub2-bR-YIcTcW-egux4kUIxy88bIhqfIMWlxrCKIWHyM1miyrKnAg01Qz7ug_C9bpxGMbwhfAfmiGxWc0W5xSyh9tIQqEeFJO8dHl7imVA5', 'andriod', 1, '2018-01-20 13:51:59'),
(243, 130, '929', 'eWJqPITS-4Q:APA91bEPkFjwv2FBBvBdTHlN-SlSBMD0Yub2-bR-YIcTcW-egux4kUIxy88bIhqfIMWlxrCKIWHyM1miyrKnAg01Qz7ug_C9bpxGMbwhfAfmiGxWc0W5xSyh9tIQqEeFJO8dHl7imVA5', 'andriod', 1, '2018-01-20 13:52:00'),
(244, 130, '539', 'eWJqPITS-4Q:APA91bEPkFjwv2FBBvBdTHlN-SlSBMD0Yub2-bR-YIcTcW-egux4kUIxy88bIhqfIMWlxrCKIWHyM1miyrKnAg01Qz7ug_C9bpxGMbwhfAfmiGxWc0W5xSyh9tIQqEeFJO8dHl7imVA5', 'andriod', 1, '2018-01-20 13:55:08'),
(245, 130, '555', 'eWJqPITS-4Q:APA91bEPkFjwv2FBBvBdTHlN-SlSBMD0Yub2-bR-YIcTcW-egux4kUIxy88bIhqfIMWlxrCKIWHyM1miyrKnAg01Qz7ug_C9bpxGMbwhfAfmiGxWc0W5xSyh9tIQqEeFJO8dHl7imVA5', 'andriod', 1, '2018-01-20 13:55:08'),
(246, 130, '499', 'eWJqPITS-4Q:APA91bEPkFjwv2FBBvBdTHlN-SlSBMD0Yub2-bR-YIcTcW-egux4kUIxy88bIhqfIMWlxrCKIWHyM1miyrKnAg01Qz7ug_C9bpxGMbwhfAfmiGxWc0W5xSyh9tIQqEeFJO8dHl7imVA5', 'andriod', 1, '2018-01-20 13:59:17'),
(247, 130, '781', 'eWJqPITS-4Q:APA91bEPkFjwv2FBBvBdTHlN-SlSBMD0Yub2-bR-YIcTcW-egux4kUIxy88bIhqfIMWlxrCKIWHyM1miyrKnAg01Qz7ug_C9bpxGMbwhfAfmiGxWc0W5xSyh9tIQqEeFJO8dHl7imVA5', 'andriod', 1, '2018-01-20 13:59:17'),
(248, 130, '900', 'eWJqPITS-4Q:APA91bEPkFjwv2FBBvBdTHlN-SlSBMD0Yub2-bR-YIcTcW-egux4kUIxy88bIhqfIMWlxrCKIWHyM1miyrKnAg01Qz7ug_C9bpxGMbwhfAfmiGxWc0W5xSyh9tIQqEeFJO8dHl7imVA5', 'andriod', 1, '2018-01-20 14:11:14'),
(249, 130, '567', 'eWJqPITS-4Q:APA91bEPkFjwv2FBBvBdTHlN-SlSBMD0Yub2-bR-YIcTcW-egux4kUIxy88bIhqfIMWlxrCKIWHyM1miyrKnAg01Qz7ug_C9bpxGMbwhfAfmiGxWc0W5xSyh9tIQqEeFJO8dHl7imVA5', 'andriod', 1, '2018-01-20 14:11:15'),
(250, 69, '705', 'fXUt4ipiOfo:APA91bHH6DXmHf2g54LPV99U6uVrje7kEuQ7YSq65RqYH5kA3u21InGyb1_C5BzEsvFInEhy5QMAsYEclyh0tJlv8Uc5SsSuHJfV82Rg44SYLjBC3xoPB4Qa2TQPb2ZsCjdSZi8PJ8c3', 'andriod', 2, '2018-01-20 14:45:03'),
(251, 69, '570', 'fXUt4ipiOfo:APA91bHH6DXmHf2g54LPV99U6uVrje7kEuQ7YSq65RqYH5kA3u21InGyb1_C5BzEsvFInEhy5QMAsYEclyh0tJlv8Uc5SsSuHJfV82Rg44SYLjBC3xoPB4Qa2TQPb2ZsCjdSZi8PJ8c3', 'andriod', 2, '2018-01-20 14:45:04'),
(252, 69, '449', 'f7m6R7KQWbg:APA91bFuBOVIfHwCsA7y1cSOvGHmmnFS_dRKzUbNSV-mc0aI4bzueIOQb0SGAgv4cKoCdWUv53Vx-5YIqx41uByu8BTrFCONdIBqMKjlxIkoK-bowyMLWyUlaIkNLzCnQ87JIijVFpY0', 'andriod', 2, '2018-01-20 15:16:13'),
(253, 69, '543', 'f7m6R7KQWbg:APA91bFuBOVIfHwCsA7y1cSOvGHmmnFS_dRKzUbNSV-mc0aI4bzueIOQb0SGAgv4cKoCdWUv53Vx-5YIqx41uByu8BTrFCONdIBqMKjlxIkoK-bowyMLWyUlaIkNLzCnQ87JIijVFpY0', 'andriod', 2, '2018-01-20 15:16:14'),
(254, 130, '891', 'eWJqPITS-4Q:APA91bEPkFjwv2FBBvBdTHlN-SlSBMD0Yub2-bR-YIcTcW-egux4kUIxy88bIhqfIMWlxrCKIWHyM1miyrKnAg01Qz7ug_C9bpxGMbwhfAfmiGxWc0W5xSyh9tIQqEeFJO8dHl7imVA5', 'andriod', 1, '2018-01-20 15:24:03'),
(255, 130, '459', 'eWJqPITS-4Q:APA91bEPkFjwv2FBBvBdTHlN-SlSBMD0Yub2-bR-YIcTcW-egux4kUIxy88bIhqfIMWlxrCKIWHyM1miyrKnAg01Qz7ug_C9bpxGMbwhfAfmiGxWc0W5xSyh9tIQqEeFJO8dHl7imVA5', 'andriod', 1, '2018-01-20 15:24:05'),
(256, 130, '974', 'eWJqPITS-4Q:APA91bEPkFjwv2FBBvBdTHlN-SlSBMD0Yub2-bR-YIcTcW-egux4kUIxy88bIhqfIMWlxrCKIWHyM1miyrKnAg01Qz7ug_C9bpxGMbwhfAfmiGxWc0W5xSyh9tIQqEeFJO8dHl7imVA5', 'andriod', 1, '2018-01-20 15:31:51'),
(257, 130, '758', 'eWJqPITS-4Q:APA91bEPkFjwv2FBBvBdTHlN-SlSBMD0Yub2-bR-YIcTcW-egux4kUIxy88bIhqfIMWlxrCKIWHyM1miyrKnAg01Qz7ug_C9bpxGMbwhfAfmiGxWc0W5xSyh9tIQqEeFJO8dHl7imVA5', 'andriod', 1, '2018-01-20 15:31:51'),
(258, 104, '481', 'eWJqPITS-4Q:APA91bEPkFjwv2FBBvBdTHlN-SlSBMD0Yub2-bR-YIcTcW-egux4kUIxy88bIhqfIMWlxrCKIWHyM1miyrKnAg01Qz7ug_C9bpxGMbwhfAfmiGxWc0W5xSyh9tIQqEeFJO8dHl7imVA5', 'andriod', 1, '2018-01-20 15:38:06'),
(259, 104, '642', 'eWJqPITS-4Q:APA91bEPkFjwv2FBBvBdTHlN-SlSBMD0Yub2-bR-YIcTcW-egux4kUIxy88bIhqfIMWlxrCKIWHyM1miyrKnAg01Qz7ug_C9bpxGMbwhfAfmiGxWc0W5xSyh9tIQqEeFJO8dHl7imVA5', 'andriod', 1, '2018-01-20 15:38:07'),
(260, 104, '991', 'eWJqPITS-4Q:APA91bEPkFjwv2FBBvBdTHlN-SlSBMD0Yub2-bR-YIcTcW-egux4kUIxy88bIhqfIMWlxrCKIWHyM1miyrKnAg01Qz7ug_C9bpxGMbwhfAfmiGxWc0W5xSyh9tIQqEeFJO8dHl7imVA5', 'andriod', 1, '2018-01-20 16:30:56'),
(261, 104, '938', 'eWJqPITS-4Q:APA91bEPkFjwv2FBBvBdTHlN-SlSBMD0Yub2-bR-YIcTcW-egux4kUIxy88bIhqfIMWlxrCKIWHyM1miyrKnAg01Qz7ug_C9bpxGMbwhfAfmiGxWc0W5xSyh9tIQqEeFJO8dHl7imVA5', 'andriod', 1, '2018-01-20 16:30:57'),
(262, 104, '553', 'eWJqPITS-4Q:APA91bEPkFjwv2FBBvBdTHlN-SlSBMD0Yub2-bR-YIcTcW-egux4kUIxy88bIhqfIMWlxrCKIWHyM1miyrKnAg01Qz7ug_C9bpxGMbwhfAfmiGxWc0W5xSyh9tIQqEeFJO8dHl7imVA5', 'andriod', 1, '2018-01-20 16:35:43'),
(263, 104, '596', 'eWJqPITS-4Q:APA91bEPkFjwv2FBBvBdTHlN-SlSBMD0Yub2-bR-YIcTcW-egux4kUIxy88bIhqfIMWlxrCKIWHyM1miyrKnAg01Qz7ug_C9bpxGMbwhfAfmiGxWc0W5xSyh9tIQqEeFJO8dHl7imVA5', 'andriod', 1, '2018-01-20 16:35:44'),
(264, 104, '786', 'dzJkAyGF4uE:APA91bHN0Dbx2qetzUiBDwOrXFR2_YLG-pse4Lf6x1iF-DCMlox083Opv7uUeof8HLS9inJGfydlMCcIGwiulVWjUNeTlfjvp5Bb2ZnERLkbE-z8GxnjVqP_fKXVkueODMDL59CeDxS6', 'andriod', 1, '2018-01-20 16:38:18'),
(265, 104, '552', 'dzJkAyGF4uE:APA91bHN0Dbx2qetzUiBDwOrXFR2_YLG-pse4Lf6x1iF-DCMlox083Opv7uUeof8HLS9inJGfydlMCcIGwiulVWjUNeTlfjvp5Bb2ZnERLkbE-z8GxnjVqP_fKXVkueODMDL59CeDxS6', 'andriod', 1, '2018-01-20 16:38:19'),
(266, 104, '891', 'dzJkAyGF4uE:APA91bHN0Dbx2qetzUiBDwOrXFR2_YLG-pse4Lf6x1iF-DCMlox083Opv7uUeof8HLS9inJGfydlMCcIGwiulVWjUNeTlfjvp5Bb2ZnERLkbE-z8GxnjVqP_fKXVkueODMDL59CeDxS6', 'andriod', 1, '2018-01-20 16:46:10'),
(267, 104, '977', 'dzJkAyGF4uE:APA91bHN0Dbx2qetzUiBDwOrXFR2_YLG-pse4Lf6x1iF-DCMlox083Opv7uUeof8HLS9inJGfydlMCcIGwiulVWjUNeTlfjvp5Bb2ZnERLkbE-z8GxnjVqP_fKXVkueODMDL59CeDxS6', 'andriod', 1, '2018-01-20 16:46:11'),
(268, 104, '633', 'dzJkAyGF4uE:APA91bHN0Dbx2qetzUiBDwOrXFR2_YLG-pse4Lf6x1iF-DCMlox083Opv7uUeof8HLS9inJGfydlMCcIGwiulVWjUNeTlfjvp5Bb2ZnERLkbE-z8GxnjVqP_fKXVkueODMDL59CeDxS6', 'andriod', 1, '2018-01-20 16:59:36'),
(269, 104, '495', 'dzJkAyGF4uE:APA91bHN0Dbx2qetzUiBDwOrXFR2_YLG-pse4Lf6x1iF-DCMlox083Opv7uUeof8HLS9inJGfydlMCcIGwiulVWjUNeTlfjvp5Bb2ZnERLkbE-z8GxnjVqP_fKXVkueODMDL59CeDxS6', 'andriod', 1, '2018-01-20 16:59:37'),
(270, 130, '612', 'dzJkAyGF4uE:APA91bHN0Dbx2qetzUiBDwOrXFR2_YLG-pse4Lf6x1iF-DCMlox083Opv7uUeof8HLS9inJGfydlMCcIGwiulVWjUNeTlfjvp5Bb2ZnERLkbE-z8GxnjVqP_fKXVkueODMDL59CeDxS6', 'andriod', 1, '2018-01-20 17:08:12'),
(271, 130, '625', 'dzJkAyGF4uE:APA91bHN0Dbx2qetzUiBDwOrXFR2_YLG-pse4Lf6x1iF-DCMlox083Opv7uUeof8HLS9inJGfydlMCcIGwiulVWjUNeTlfjvp5Bb2ZnERLkbE-z8GxnjVqP_fKXVkueODMDL59CeDxS6', 'andriod', 1, '2018-01-20 17:08:13'),
(272, 69, '568', 'fq1m6eBGtxE:APA91bHsDGT9AW_yMzmjPNdW3Qj9K58NfItSj2qRDvtfureHc0xFDHJSp4MOtuYP6nG8U1F1hiNlzZt9WRGVlLqEI-Lr3W61jioEtWpOlTvpComgQMyU1-bQG1GP1y-Zz15v8EV9llko', 'andriod', 2, '2018-01-22 06:50:25'),
(273, 69, '506', 'fq1m6eBGtxE:APA91bHsDGT9AW_yMzmjPNdW3Qj9K58NfItSj2qRDvtfureHc0xFDHJSp4MOtuYP6nG8U1F1hiNlzZt9WRGVlLqEI-Lr3W61jioEtWpOlTvpComgQMyU1-bQG1GP1y-Zz15v8EV9llko', 'andriod', 2, '2018-01-22 06:50:26'),
(274, 104, '720', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-22 07:00:22'),
(275, 104, '891', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-22 07:00:23'),
(276, 69, '599', 'cZBudojTDWA:APA91bHcNSQ97SW4coePDRvNJ9stI7mRRnPySGWg8Bs-ySuiQ6UpeJ6-TB4W96IoeWDpysOzCM24W8o76n9inFrlWOLU-wcGu9KOHjbvZErNfY6DTddquqwtU4BIJxRxOy17HNTqQCYt', 'andriod', 2, '2018-01-22 07:19:04'),
(277, 69, '813', 'cZBudojTDWA:APA91bHcNSQ97SW4coePDRvNJ9stI7mRRnPySGWg8Bs-ySuiQ6UpeJ6-TB4W96IoeWDpysOzCM24W8o76n9inFrlWOLU-wcGu9KOHjbvZErNfY6DTddquqwtU4BIJxRxOy17HNTqQCYt', 'andriod', 2, '2018-01-22 07:19:04'),
(278, 104, '680', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-22 07:42:14'),
(279, 104, '790', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-22 07:42:14'),
(280, 104, '823', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-22 07:47:47'),
(281, 104, '792', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-22 07:47:47'),
(282, 104, '504', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-22 07:51:23'),
(283, 104, '790', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-22 07:51:23'),
(284, 104, '928', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-22 07:56:06'),
(285, 104, '846', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-22 07:56:07'),
(286, 104, '595', 'dzJkAyGF4uE:APA91bHN0Dbx2qetzUiBDwOrXFR2_YLG-pse4Lf6x1iF-DCMlox083Opv7uUeof8HLS9inJGfydlMCcIGwiulVWjUNeTlfjvp5Bb2ZnERLkbE-z8GxnjVqP_fKXVkueODMDL59CeDxS6', 'andriod', 1, '2018-01-22 08:05:44'),
(287, 104, '561', 'dzJkAyGF4uE:APA91bHN0Dbx2qetzUiBDwOrXFR2_YLG-pse4Lf6x1iF-DCMlox083Opv7uUeof8HLS9inJGfydlMCcIGwiulVWjUNeTlfjvp5Bb2ZnERLkbE-z8GxnjVqP_fKXVkueODMDL59CeDxS6', 'andriod', 1, '2018-01-22 08:05:44'),
(288, 104, '876', 'dzJkAyGF4uE:APA91bHN0Dbx2qetzUiBDwOrXFR2_YLG-pse4Lf6x1iF-DCMlox083Opv7uUeof8HLS9inJGfydlMCcIGwiulVWjUNeTlfjvp5Bb2ZnERLkbE-z8GxnjVqP_fKXVkueODMDL59CeDxS6', 'andriod', 1, '2018-01-22 08:13:43'),
(289, 104, '774', 'dzJkAyGF4uE:APA91bHN0Dbx2qetzUiBDwOrXFR2_YLG-pse4Lf6x1iF-DCMlox083Opv7uUeof8HLS9inJGfydlMCcIGwiulVWjUNeTlfjvp5Bb2ZnERLkbE-z8GxnjVqP_fKXVkueODMDL59CeDxS6', 'andriod', 1, '2018-01-22 08:13:44'),
(290, 104, '485', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-22 08:34:04'),
(291, 104, '684', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-22 08:34:04'),
(292, 104, '805', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-22 08:37:53'),
(293, 104, '603', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-22 08:37:53'),
(294, 104, '453', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-22 08:39:59'),
(295, 104, '829', 'f5scMmMs6XU:APA91bESCQYx84VgUFuRUltdZHcWbpWoRpzx4_8hbQc7Tx1pDywgaSFnNR8Yqzg4fsjM7CtkgokMzfrtmxo73_AujyN7WYjbuAjuWji3rlrSQJewHhO5hoChPfylBntqnnLKbm1Ru0CC', 'andriod', 1, '2018-01-22 08:40:00'),
(296, 104, '598', 'dzJkAyGF4uE:APA91bHN0Dbx2qetzUiBDwOrXFR2_YLG-pse4Lf6x1iF-DCMlox083Opv7uUeof8HLS9inJGfydlMCcIGwiulVWjUNeTlfjvp5Bb2ZnERLkbE-z8GxnjVqP_fKXVkueODMDL59CeDxS6', 'andriod', 1, '2018-01-22 08:46:10'),
(297, 104, '714', 'dzJkAyGF4uE:APA91bHN0Dbx2qetzUiBDwOrXFR2_YLG-pse4Lf6x1iF-DCMlox083Opv7uUeof8HLS9inJGfydlMCcIGwiulVWjUNeTlfjvp5Bb2ZnERLkbE-z8GxnjVqP_fKXVkueODMDL59CeDxS6', 'andriod', 1, '2018-01-22 08:46:11'),
(298, 104, '642', 'dzJkAyGF4uE:APA91bHN0Dbx2qetzUiBDwOrXFR2_YLG-pse4Lf6x1iF-DCMlox083Opv7uUeof8HLS9inJGfydlMCcIGwiulVWjUNeTlfjvp5Bb2ZnERLkbE-z8GxnjVqP_fKXVkueODMDL59CeDxS6', 'andriod', 1, '2018-01-22 08:51:40'),
(299, 104, '765', 'dzJkAyGF4uE:APA91bHN0Dbx2qetzUiBDwOrXFR2_YLG-pse4Lf6x1iF-DCMlox083Opv7uUeof8HLS9inJGfydlMCcIGwiulVWjUNeTlfjvp5Bb2ZnERLkbE-z8GxnjVqP_fKXVkueODMDL59CeDxS6', 'andriod', 1, '2018-01-22 08:51:40'),
(300, 104, '823', 'dzJkAyGF4uE:APA91bHN0Dbx2qetzUiBDwOrXFR2_YLG-pse4Lf6x1iF-DCMlox083Opv7uUeof8HLS9inJGfydlMCcIGwiulVWjUNeTlfjvp5Bb2ZnERLkbE-z8GxnjVqP_fKXVkueODMDL59CeDxS6', 'andriod', 1, '2018-01-22 09:01:11'),
(301, 104, '788', 'dzJkAyGF4uE:APA91bHN0Dbx2qetzUiBDwOrXFR2_YLG-pse4Lf6x1iF-DCMlox083Opv7uUeof8HLS9inJGfydlMCcIGwiulVWjUNeTlfjvp5Bb2ZnERLkbE-z8GxnjVqP_fKXVkueODMDL59CeDxS6', 'andriod', 1, '2018-01-22 09:01:12'),
(302, 104, '999', 'dzJkAyGF4uE:APA91bHN0Dbx2qetzUiBDwOrXFR2_YLG-pse4Lf6x1iF-DCMlox083Opv7uUeof8HLS9inJGfydlMCcIGwiulVWjUNeTlfjvp5Bb2ZnERLkbE-z8GxnjVqP_fKXVkueODMDL59CeDxS6', 'andriod', 1, '2018-01-22 09:37:25'),
(303, 104, '597', 'dzJkAyGF4uE:APA91bHN0Dbx2qetzUiBDwOrXFR2_YLG-pse4Lf6x1iF-DCMlox083Opv7uUeof8HLS9inJGfydlMCcIGwiulVWjUNeTlfjvp5Bb2ZnERLkbE-z8GxnjVqP_fKXVkueODMDL59CeDxS6', 'andriod', 1, '2018-01-22 09:37:26'),
(304, 104, '725', 'f_0yR6dr43g:APA91bGz6LoiRcb4yMvb13vzNAsU327tpmpesfhKxMVyBkD8r0ie4wZFh5fk0cu2nSfIcwHTb6d1ROcr_Qc-8SsFEmc6qI29xEC6p9zDtGd7oQSlIAOm-2mNlP6gFkTqld9enZmjxZwt', 'andriod', 1, '2018-01-22 09:50:57'),
(305, 104, '473', 'f_0yR6dr43g:APA91bGz6LoiRcb4yMvb13vzNAsU327tpmpesfhKxMVyBkD8r0ie4wZFh5fk0cu2nSfIcwHTb6d1ROcr_Qc-8SsFEmc6qI29xEC6p9zDtGd7oQSlIAOm-2mNlP6gFkTqld9enZmjxZwt', 'andriod', 1, '2018-01-22 09:50:57'),
(306, 104, '626', 'drGZ0ZQbbFk:APA91bEOy0MDayQJfNSMU3NEIZGLimAyCwhxPnfOHo9OSgRFkMPhqP-JtxXqICC25srdPnh41qMVIgPEnMuWx13BcuIi9HZEB_J3vZr1CgMXU_WW2iNH7aFDnTh2n5feXunNexQd7vg5', 'andriod', 1, '2018-01-22 10:08:47'),
(307, 104, '871', 'drGZ0ZQbbFk:APA91bEOy0MDayQJfNSMU3NEIZGLimAyCwhxPnfOHo9OSgRFkMPhqP-JtxXqICC25srdPnh41qMVIgPEnMuWx13BcuIi9HZEB_J3vZr1CgMXU_WW2iNH7aFDnTh2n5feXunNexQd7vg5', 'andriod', 1, '2018-01-22 10:08:48'),
(308, 104, '566', 'drGZ0ZQbbFk:APA91bEOy0MDayQJfNSMU3NEIZGLimAyCwhxPnfOHo9OSgRFkMPhqP-JtxXqICC25srdPnh41qMVIgPEnMuWx13BcuIi9HZEB_J3vZr1CgMXU_WW2iNH7aFDnTh2n5feXunNexQd7vg5', 'andriod', 1, '2018-01-22 10:09:55'),
(309, 104, '509', 'drGZ0ZQbbFk:APA91bEOy0MDayQJfNSMU3NEIZGLimAyCwhxPnfOHo9OSgRFkMPhqP-JtxXqICC25srdPnh41qMVIgPEnMuWx13BcuIi9HZEB_J3vZr1CgMXU_WW2iNH7aFDnTh2n5feXunNexQd7vg5', 'andriod', 1, '2018-01-22 10:09:55'),
(310, 104, '827', 'eTDCexJF14A:APA91bH91-cbrs5hjrWp28ZcuNJ8dsXm62B0jcXDotj57jPJIKswNspN-b9CLlQXgeRrIh1Kd9gX0KpxbrHTLXO082_sWXV5hSdNyUNMVsfW8-QQGTmpFZhrE7XKHuYWvk06CqFq3XbM', 'andriod', 1, '2018-01-22 10:28:05'),
(311, 104, '974', 'eTDCexJF14A:APA91bH91-cbrs5hjrWp28ZcuNJ8dsXm62B0jcXDotj57jPJIKswNspN-b9CLlQXgeRrIh1Kd9gX0KpxbrHTLXO082_sWXV5hSdNyUNMVsfW8-QQGTmpFZhrE7XKHuYWvk06CqFq3XbM', 'andriod', 1, '2018-01-22 10:28:06'),
(312, 104, '993', 'fuUgF_3R2f8:APA91bE3xKz4GBUlq4P-yhwb2Psr80ZDP3cHJkEcNgmVfw1ltayJ6esdJ17eB_CB06qiwB8hQxl82fGqY-xaTbNS4AaTQuTF7UKhcKxB-QuHdQj61GM7cWOEWRzHX1TU1_nhaiaH6VnM', 'andriod', 1, '2018-01-22 10:51:36'),
(313, 104, '741', 'fuUgF_3R2f8:APA91bE3xKz4GBUlq4P-yhwb2Psr80ZDP3cHJkEcNgmVfw1ltayJ6esdJ17eB_CB06qiwB8hQxl82fGqY-xaTbNS4AaTQuTF7UKhcKxB-QuHdQj61GM7cWOEWRzHX1TU1_nhaiaH6VnM', 'andriod', 1, '2018-01-22 10:51:37'),
(314, 104, '761', 'fuUgF_3R2f8:APA91bE3xKz4GBUlq4P-yhwb2Psr80ZDP3cHJkEcNgmVfw1ltayJ6esdJ17eB_CB06qiwB8hQxl82fGqY-xaTbNS4AaTQuTF7UKhcKxB-QuHdQj61GM7cWOEWRzHX1TU1_nhaiaH6VnM', 'andriod', 1, '2018-01-22 10:56:09'),
(315, 104, '458', 'fuUgF_3R2f8:APA91bE3xKz4GBUlq4P-yhwb2Psr80ZDP3cHJkEcNgmVfw1ltayJ6esdJ17eB_CB06qiwB8hQxl82fGqY-xaTbNS4AaTQuTF7UKhcKxB-QuHdQj61GM7cWOEWRzHX1TU1_nhaiaH6VnM', 'andriod', 1, '2018-01-22 10:56:10'),
(316, 104, '682', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-22 11:09:05'),
(317, 104, '550', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-22 11:09:06'),
(318, 104, '518', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-22 11:22:16'),
(319, 104, '865', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-22 11:22:17'),
(320, 104, '776', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-22 11:37:06'),
(321, 104, '964', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-22 11:37:06'),
(322, 104, '985', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-22 11:56:00'),
(323, 104, '632', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-22 11:56:01'),
(324, 69, '445', 'dm8pAttuRB8:APA91bH2_J7eveOy4Y-Djvqdc52yVRiHP7zajEaVYE1NRr-LDX3EKeO0qEj8CBX89s9l10SdtUThMIZvhFtAEvH4MfEjoJtIhdCbY8b_8K8Hf4YCmt-oGFQ-50b527jZpmxWD5cwxgU8', 'andriod', 2, '2018-01-22 12:10:41'),
(325, 69, '698', 'dm8pAttuRB8:APA91bH2_J7eveOy4Y-Djvqdc52yVRiHP7zajEaVYE1NRr-LDX3EKeO0qEj8CBX89s9l10SdtUThMIZvhFtAEvH4MfEjoJtIhdCbY8b_8K8Hf4YCmt-oGFQ-50b527jZpmxWD5cwxgU8', 'andriod', 2, '2018-01-22 12:10:42'),
(326, 104, '496', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-22 12:12:49'),
(327, 104, '628', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-22 12:12:50'),
(328, 104, '651', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-22 12:17:22'),
(329, 104, '647', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-22 12:17:23'),
(330, 104, '850', 'dqJ_cFa2s7s:APA91bGNMbLsOAtrjz6AbDnUKh8K917iE4SdZFHed9OCB0UQaUSxB9VzwdhlQjvzsc1AC6bo_D15XuTViehfWJps_kcB3oPHsIxL5IJxEe4GxP_zYi_-yu3wDHjRhVJxr8dAMLqPgrwv', 'andriod', 1, '2018-01-22 13:23:35'),
(331, 104, '576', 'dqJ_cFa2s7s:APA91bGNMbLsOAtrjz6AbDnUKh8K917iE4SdZFHed9OCB0UQaUSxB9VzwdhlQjvzsc1AC6bo_D15XuTViehfWJps_kcB3oPHsIxL5IJxEe4GxP_zYi_-yu3wDHjRhVJxr8dAMLqPgrwv', 'andriod', 1, '2018-01-22 13:23:37'),
(332, 104, '760', 'dqJ_cFa2s7s:APA91bGNMbLsOAtrjz6AbDnUKh8K917iE4SdZFHed9OCB0UQaUSxB9VzwdhlQjvzsc1AC6bo_D15XuTViehfWJps_kcB3oPHsIxL5IJxEe4GxP_zYi_-yu3wDHjRhVJxr8dAMLqPgrwv', 'andriod', 1, '2018-01-22 14:02:08'),
(333, 104, '587', 'dqJ_cFa2s7s:APA91bGNMbLsOAtrjz6AbDnUKh8K917iE4SdZFHed9OCB0UQaUSxB9VzwdhlQjvzsc1AC6bo_D15XuTViehfWJps_kcB3oPHsIxL5IJxEe4GxP_zYi_-yu3wDHjRhVJxr8dAMLqPgrwv', 'andriod', 1, '2018-01-22 14:02:09'),
(334, 69, '859', 'cZBudojTDWA:APA91bHcNSQ97SW4coePDRvNJ9stI7mRRnPySGWg8Bs-ySuiQ6UpeJ6-TB4W96IoeWDpysOzCM24W8o76n9inFrlWOLU-wcGu9KOHjbvZErNfY6DTddquqwtU4BIJxRxOy17HNTqQCYt', 'andriod', 2, '2018-01-22 14:04:14'),
(335, 69, '513', 'cZBudojTDWA:APA91bHcNSQ97SW4coePDRvNJ9stI7mRRnPySGWg8Bs-ySuiQ6UpeJ6-TB4W96IoeWDpysOzCM24W8o76n9inFrlWOLU-wcGu9KOHjbvZErNfY6DTddquqwtU4BIJxRxOy17HNTqQCYt', 'andriod', 2, '2018-01-22 14:04:14'),
(336, 104, '895', 'dqJ_cFa2s7s:APA91bGNMbLsOAtrjz6AbDnUKh8K917iE4SdZFHed9OCB0UQaUSxB9VzwdhlQjvzsc1AC6bo_D15XuTViehfWJps_kcB3oPHsIxL5IJxEe4GxP_zYi_-yu3wDHjRhVJxr8dAMLqPgrwv', 'andriod', 1, '2018-01-22 14:12:04'),
(337, 104, '824', 'dqJ_cFa2s7s:APA91bGNMbLsOAtrjz6AbDnUKh8K917iE4SdZFHed9OCB0UQaUSxB9VzwdhlQjvzsc1AC6bo_D15XuTViehfWJps_kcB3oPHsIxL5IJxEe4GxP_zYi_-yu3wDHjRhVJxr8dAMLqPgrwv', 'andriod', 1, '2018-01-22 14:12:05'),
(338, 104, '453', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-22 14:14:38'),
(339, 104, '542', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-22 14:14:38'),
(340, 104, '787', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-22 14:21:21'),
(341, 104, '681', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-22 14:21:21'),
(342, 104, '457', 'c1bjmBdQV4U:APA91bGV8-wLWGPvphEny2nHBoKeAh9S1X1NlnGEoPmH_Kw0lblH-IWZwpOHoRg5vNj6qZt-RdcdfCVn8nKRltysgnR2vImo4ZG75kTFNF0c6UtHijaksjcgUvh0UvgD37ScbLlPra5B', 'andriod', 1, '2018-01-22 14:27:21'),
(343, 104, '794', 'c1bjmBdQV4U:APA91bGV8-wLWGPvphEny2nHBoKeAh9S1X1NlnGEoPmH_Kw0lblH-IWZwpOHoRg5vNj6qZt-RdcdfCVn8nKRltysgnR2vImo4ZG75kTFNF0c6UtHijaksjcgUvh0UvgD37ScbLlPra5B', 'andriod', 1, '2018-01-22 14:27:21'),
(344, 104, '525', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-22 14:43:34'),
(345, 104, '549', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-22 14:43:34'),
(346, 104, '748', 'dvcHYpc-azs:APA91bE1Oknxvau0k9xGDVe5UQWYJy_8c6WD9-VpDCAzUIHg0QXbr6M8555yC3hY2ntx4rb9WnEfuDKLjJFoTbet3xpHzqg1eHI1cEKhy7iUmJU_C15JDJ-i4wnYUI4kyj3W7Dq5ka_h', 'andriod', 1, '2018-01-22 15:01:19'),
(347, 104, '620', 'dvcHYpc-azs:APA91bE1Oknxvau0k9xGDVe5UQWYJy_8c6WD9-VpDCAzUIHg0QXbr6M8555yC3hY2ntx4rb9WnEfuDKLjJFoTbet3xpHzqg1eHI1cEKhy7iUmJU_C15JDJ-i4wnYUI4kyj3W7Dq5ka_h', 'andriod', 1, '2018-01-22 15:01:19'),
(348, 104, '930', 'dvcHYpc-azs:APA91bE1Oknxvau0k9xGDVe5UQWYJy_8c6WD9-VpDCAzUIHg0QXbr6M8555yC3hY2ntx4rb9WnEfuDKLjJFoTbet3xpHzqg1eHI1cEKhy7iUmJU_C15JDJ-i4wnYUI4kyj3W7Dq5ka_h', 'andriod', 1, '2018-01-22 15:08:37'),
(349, 104, '444', 'dvcHYpc-azs:APA91bE1Oknxvau0k9xGDVe5UQWYJy_8c6WD9-VpDCAzUIHg0QXbr6M8555yC3hY2ntx4rb9WnEfuDKLjJFoTbet3xpHzqg1eHI1cEKhy7iUmJU_C15JDJ-i4wnYUI4kyj3W7Dq5ka_h', 'andriod', 1, '2018-01-22 15:08:37'),
(350, 104, '608', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 06:25:49'),
(351, 104, '508', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 06:25:50'),
(352, 69, '691', '', 'andriod', 2, '2018-01-24 07:30:57'),
(353, 69, '695', '', 'andriod', 2, '2018-01-24 07:30:57'),
(354, 104, '985', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-24 07:35:50'),
(355, 104, '748', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-24 07:35:51'),
(356, 69, '646', 'dmdO9JAJfU8:APA91bGrbeN4wy0rTmLc6N-EvoQCxZTvQPbdVJ7jeQPUN5_4FYqcIEDmr2wutdl5F9lHlofAfE6EiP9cRA8v0Uf1hWFsbe35SxjtfKml4qPcdEMUnc_z1G6hDJJf5qGj9pZZd7JCAJ1E', 'andriod', 2, '2018-01-24 07:57:40'),
(357, 69, '479', 'dmdO9JAJfU8:APA91bGrbeN4wy0rTmLc6N-EvoQCxZTvQPbdVJ7jeQPUN5_4FYqcIEDmr2wutdl5F9lHlofAfE6EiP9cRA8v0Uf1hWFsbe35SxjtfKml4qPcdEMUnc_z1G6hDJJf5qGj9pZZd7JCAJ1E', 'andriod', 2, '2018-01-24 07:57:41'),
(358, 104, '882', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 08:05:03'),
(359, 104, '605', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 08:05:03'),
(360, 104, '562', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 08:16:34'),
(361, 104, '975', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 08:16:35'),
(362, 104, '672', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-24 08:23:51'),
(363, 104, '814', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-24 08:23:52'),
(364, 104, '680', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 08:53:22'),
(365, 104, '616', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 08:53:22'),
(366, 104, '480', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 09:22:06'),
(367, 104, '989', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 09:22:07'),
(368, 104, '464', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 09:42:39'),
(369, 104, '760', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 09:42:40'),
(370, 104, '784', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 09:57:13'),
(371, 104, '548', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 09:57:14'),
(372, 104, '713', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 10:17:12'),
(373, 104, '554', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 10:17:12'),
(374, 104, '711', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 10:23:35'),
(375, 104, '993', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 10:23:35'),
(376, 104, '904', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 11:04:53'),
(377, 104, '771', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 11:04:54'),
(378, 104, '991', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 11:19:56'),
(379, 104, '631', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 11:19:57'),
(380, 69, '995', 'eh3nu-TFs-c:APA91bHjNgZc5NA5LGl8BPq1ORgJpMbB_zioLl0omdX2kyuf12D34Cv7XELsrUdpKF18q_mDuvnh7jtfVYQVXb9VKttdblrYD-4LyHFF3nuDVegA03Yd2pyEDhKiJdvGX72XF_JQdu7G', 'andriod', 2, '2018-01-24 11:27:30'),
(381, 69, '462', 'eh3nu-TFs-c:APA91bHjNgZc5NA5LGl8BPq1ORgJpMbB_zioLl0omdX2kyuf12D34Cv7XELsrUdpKF18q_mDuvnh7jtfVYQVXb9VKttdblrYD-4LyHFF3nuDVegA03Yd2pyEDhKiJdvGX72XF_JQdu7G', 'andriod', 2, '2018-01-24 11:27:30'),
(382, 104, '532', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 11:31:18'),
(383, 104, '887', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 11:31:18'),
(384, 104, '883', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 11:47:48'),
(385, 104, '884', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 11:47:49'),
(386, 104, '502', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 11:58:57'),
(387, 104, '945', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 11:58:57'),
(388, 104, '670', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 13:14:58'),
(389, 104, '930', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-24 13:14:59'),
(390, 104, '861', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-25 12:37:25'),
(391, 104, '516', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-25 12:37:26'),
(392, 104, '741', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-25 13:21:45'),
(393, 104, '455', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-25 13:21:45'),
(394, 104, '640', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-25 13:28:30'),
(395, 104, '609', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-25 13:28:31'),
(396, 104, '729', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-25 13:52:43'),
(397, 104, '985', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-25 13:52:44'),
(398, 104, '539', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-25 14:00:16'),
(399, 104, '638', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-25 14:00:17'),
(400, 104, '756', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-25 14:39:57'),
(401, 104, '855', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-25 14:39:58'),
(402, 104, '687', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-25 15:04:31'),
(403, 104, '786', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-25 15:04:32'),
(404, 72, '631', 'edNkVRZ-dPQ:APA91bEuVygOsCvUSzB5oOeiQNZTy16M94exNV7KRqvv6ak8l90idAuQyJ5u8M-wsygpz5NKWS3ex9BniZWaqbQjctX0bo_95y9H_cVwHFFHNhUXbdos5mPoOtRkM96BKeYKDl6470M7', 'andriod', 2, '2018-01-25 15:12:32'),
(405, 72, '617', 'edNkVRZ-dPQ:APA91bEuVygOsCvUSzB5oOeiQNZTy16M94exNV7KRqvv6ak8l90idAuQyJ5u8M-wsygpz5NKWS3ex9BniZWaqbQjctX0bo_95y9H_cVwHFFHNhUXbdos5mPoOtRkM96BKeYKDl6470M7', 'andriod', 2, '2018-01-25 15:12:32'),
(406, 72, '835', 'dM5l3MZyNvc:APA91bHVXW2Vzpn4IFKtIcMPVlhRKECOmtG30Q9OgSgaNP0CHQZPRqtUJ5T_ncG4eA89ntftMpFSvQ_96guKTpKMTxT8t6wjumfpz3MJqeKUxvi9e2DiqMtbeFd46cLzadSBNJguLF4p', 'andriod', 2, '2018-01-25 16:01:52'),
(407, 72, '468', 'dM5l3MZyNvc:APA91bHVXW2Vzpn4IFKtIcMPVlhRKECOmtG30Q9OgSgaNP0CHQZPRqtUJ5T_ncG4eA89ntftMpFSvQ_96guKTpKMTxT8t6wjumfpz3MJqeKUxvi9e2DiqMtbeFd46cLzadSBNJguLF4p', 'andriod', 2, '2018-01-25 16:01:52'),
(408, 72, '500', 'dM5l3MZyNvc:APA91bHVXW2Vzpn4IFKtIcMPVlhRKECOmtG30Q9OgSgaNP0CHQZPRqtUJ5T_ncG4eA89ntftMpFSvQ_96guKTpKMTxT8t6wjumfpz3MJqeKUxvi9e2DiqMtbeFd46cLzadSBNJguLF4p', 'andriod', 2, '2018-01-25 16:09:42'),
(409, 72, '703', 'dM5l3MZyNvc:APA91bHVXW2Vzpn4IFKtIcMPVlhRKECOmtG30Q9OgSgaNP0CHQZPRqtUJ5T_ncG4eA89ntftMpFSvQ_96guKTpKMTxT8t6wjumfpz3MJqeKUxvi9e2DiqMtbeFd46cLzadSBNJguLF4p', 'andriod', 2, '2018-01-25 16:09:42'),
(410, 104, '619', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-25 16:13:55'),
(411, 104, '502', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-25 16:13:55'),
(412, 104, '688', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-25 16:16:36'),
(413, 104, '803', 'fJ5p83jT-Q8:APA91bFF_tOed1dGii2cHy8SuO2k1ikxAyy10D1fyiMEeHIYNj1Rl7wpUYQaidKfkBoFXRu4YZtM1c9dXghaWjr0AkyGXVq1T_5A7difxqzJpN9J53d2SzaEg8NEQo4nzxzYsMPJ_q9A', 'andriod', 1, '2018-01-25 16:16:37'),
(414, 104, '643', 'f_mgnyPx6ZU:APA91bGxXiMgKaQB1nb0AZWMqenfFflBLf-Rka5NKbDR0tk07pSxYDKq7k_tSYXjS_1S0-ATZE9bWv_gfkEEQNBfkhMMQnFjogWvSuKFgQb3IDQ-E8362ePIGD3HlL_db6vMZRt6OKmq', 'andriod', 1, '2018-01-25 16:22:43'),
(415, 104, '556', 'f_mgnyPx6ZU:APA91bGxXiMgKaQB1nb0AZWMqenfFflBLf-Rka5NKbDR0tk07pSxYDKq7k_tSYXjS_1S0-ATZE9bWv_gfkEEQNBfkhMMQnFjogWvSuKFgQb3IDQ-E8362ePIGD3HlL_db6vMZRt6OKmq', 'andriod', 1, '2018-01-25 16:22:43'),
(416, 104, '903', 'f_mgnyPx6ZU:APA91bGxXiMgKaQB1nb0AZWMqenfFflBLf-Rka5NKbDR0tk07pSxYDKq7k_tSYXjS_1S0-ATZE9bWv_gfkEEQNBfkhMMQnFjogWvSuKFgQb3IDQ-E8362ePIGD3HlL_db6vMZRt6OKmq', 'andriod', 1, '2018-01-25 16:23:51'),
(417, 104, '748', 'f_mgnyPx6ZU:APA91bGxXiMgKaQB1nb0AZWMqenfFflBLf-Rka5NKbDR0tk07pSxYDKq7k_tSYXjS_1S0-ATZE9bWv_gfkEEQNBfkhMMQnFjogWvSuKFgQb3IDQ-E8362ePIGD3HlL_db6vMZRt6OKmq', 'andriod', 1, '2018-01-25 16:23:51'),
(418, 104, '850', 'f_mgnyPx6ZU:APA91bGxXiMgKaQB1nb0AZWMqenfFflBLf-Rka5NKbDR0tk07pSxYDKq7k_tSYXjS_1S0-ATZE9bWv_gfkEEQNBfkhMMQnFjogWvSuKFgQb3IDQ-E8362ePIGD3HlL_db6vMZRt6OKmq', 'andriod', 1, '2018-01-27 06:09:04'),
(419, 104, '512', 'f_mgnyPx6ZU:APA91bGxXiMgKaQB1nb0AZWMqenfFflBLf-Rka5NKbDR0tk07pSxYDKq7k_tSYXjS_1S0-ATZE9bWv_gfkEEQNBfkhMMQnFjogWvSuKFgQb3IDQ-E8362ePIGD3HlL_db6vMZRt6OKmq', 'andriod', 1, '2018-01-27 06:09:08'),
(420, 104, '729', 'f_mgnyPx6ZU:APA91bGxXiMgKaQB1nb0AZWMqenfFflBLf-Rka5NKbDR0tk07pSxYDKq7k_tSYXjS_1S0-ATZE9bWv_gfkEEQNBfkhMMQnFjogWvSuKFgQb3IDQ-E8362ePIGD3HlL_db6vMZRt6OKmq', 'andriod', 1, '2018-01-27 06:24:27'),
(421, 104, '895', 'f_mgnyPx6ZU:APA91bGxXiMgKaQB1nb0AZWMqenfFflBLf-Rka5NKbDR0tk07pSxYDKq7k_tSYXjS_1S0-ATZE9bWv_gfkEEQNBfkhMMQnFjogWvSuKFgQb3IDQ-E8362ePIGD3HlL_db6vMZRt6OKmq', 'andriod', 1, '2018-01-27 06:24:28'),
(422, 104, '825', 'f_mgnyPx6ZU:APA91bGxXiMgKaQB1nb0AZWMqenfFflBLf-Rka5NKbDR0tk07pSxYDKq7k_tSYXjS_1S0-ATZE9bWv_gfkEEQNBfkhMMQnFjogWvSuKFgQb3IDQ-E8362ePIGD3HlL_db6vMZRt6OKmq', 'andriod', 1, '2018-01-27 06:54:48'),
(423, 104, '515', 'f_mgnyPx6ZU:APA91bGxXiMgKaQB1nb0AZWMqenfFflBLf-Rka5NKbDR0tk07pSxYDKq7k_tSYXjS_1S0-ATZE9bWv_gfkEEQNBfkhMMQnFjogWvSuKFgQb3IDQ-E8362ePIGD3HlL_db6vMZRt6OKmq', 'andriod', 1, '2018-01-27 06:54:49'),
(424, 104, '494', 'f_mgnyPx6ZU:APA91bGxXiMgKaQB1nb0AZWMqenfFflBLf-Rka5NKbDR0tk07pSxYDKq7k_tSYXjS_1S0-ATZE9bWv_gfkEEQNBfkhMMQnFjogWvSuKFgQb3IDQ-E8362ePIGD3HlL_db6vMZRt6OKmq', 'andriod', 1, '2018-01-27 07:17:32'),
(425, 104, '901', 'f_mgnyPx6ZU:APA91bGxXiMgKaQB1nb0AZWMqenfFflBLf-Rka5NKbDR0tk07pSxYDKq7k_tSYXjS_1S0-ATZE9bWv_gfkEEQNBfkhMMQnFjogWvSuKFgQb3IDQ-E8362ePIGD3HlL_db6vMZRt6OKmq', 'andriod', 1, '2018-01-27 07:17:33'),
(426, 104, '734', 'f_mgnyPx6ZU:APA91bGxXiMgKaQB1nb0AZWMqenfFflBLf-Rka5NKbDR0tk07pSxYDKq7k_tSYXjS_1S0-ATZE9bWv_gfkEEQNBfkhMMQnFjogWvSuKFgQb3IDQ-E8362ePIGD3HlL_db6vMZRt6OKmq', 'andriod', 1, '2018-01-27 07:36:37'),
(427, 104, '680', 'f_mgnyPx6ZU:APA91bGxXiMgKaQB1nb0AZWMqenfFflBLf-Rka5NKbDR0tk07pSxYDKq7k_tSYXjS_1S0-ATZE9bWv_gfkEEQNBfkhMMQnFjogWvSuKFgQb3IDQ-E8362ePIGD3HlL_db6vMZRt6OKmq', 'andriod', 1, '2018-01-27 07:36:37'),
(428, 104, '904', 'f_mgnyPx6ZU:APA91bGxXiMgKaQB1nb0AZWMqenfFflBLf-Rka5NKbDR0tk07pSxYDKq7k_tSYXjS_1S0-ATZE9bWv_gfkEEQNBfkhMMQnFjogWvSuKFgQb3IDQ-E8362ePIGD3HlL_db6vMZRt6OKmq', 'andriod', 1, '2018-01-27 07:40:59');
INSERT INTO `login` (`id`, `user_id`, `session_id`, `device_token`, `device_type`, `user_type`, `created_date_time`) VALUES
(429, 104, '649', 'f_mgnyPx6ZU:APA91bGxXiMgKaQB1nb0AZWMqenfFflBLf-Rka5NKbDR0tk07pSxYDKq7k_tSYXjS_1S0-ATZE9bWv_gfkEEQNBfkhMMQnFjogWvSuKFgQb3IDQ-E8362ePIGD3HlL_db6vMZRt6OKmq', 'andriod', 1, '2018-01-27 07:41:00'),
(430, 104, '603', 'f_mgnyPx6ZU:APA91bGxXiMgKaQB1nb0AZWMqenfFflBLf-Rka5NKbDR0tk07pSxYDKq7k_tSYXjS_1S0-ATZE9bWv_gfkEEQNBfkhMMQnFjogWvSuKFgQb3IDQ-E8362ePIGD3HlL_db6vMZRt6OKmq', 'andriod', 1, '2018-01-27 07:54:01'),
(431, 104, '718', 'f_mgnyPx6ZU:APA91bGxXiMgKaQB1nb0AZWMqenfFflBLf-Rka5NKbDR0tk07pSxYDKq7k_tSYXjS_1S0-ATZE9bWv_gfkEEQNBfkhMMQnFjogWvSuKFgQb3IDQ-E8362ePIGD3HlL_db6vMZRt6OKmq', 'andriod', 1, '2018-01-27 07:54:02'),
(432, 104, '801', 'f_mgnyPx6ZU:APA91bGxXiMgKaQB1nb0AZWMqenfFflBLf-Rka5NKbDR0tk07pSxYDKq7k_tSYXjS_1S0-ATZE9bWv_gfkEEQNBfkhMMQnFjogWvSuKFgQb3IDQ-E8362ePIGD3HlL_db6vMZRt6OKmq', 'andriod', 1, '2018-01-27 08:16:26'),
(433, 104, '942', 'f_mgnyPx6ZU:APA91bGxXiMgKaQB1nb0AZWMqenfFflBLf-Rka5NKbDR0tk07pSxYDKq7k_tSYXjS_1S0-ATZE9bWv_gfkEEQNBfkhMMQnFjogWvSuKFgQb3IDQ-E8362ePIGD3HlL_db6vMZRt6OKmq', 'andriod', 1, '2018-01-27 08:16:26'),
(434, 104, '517', 'f_mgnyPx6ZU:APA91bGxXiMgKaQB1nb0AZWMqenfFflBLf-Rka5NKbDR0tk07pSxYDKq7k_tSYXjS_1S0-ATZE9bWv_gfkEEQNBfkhMMQnFjogWvSuKFgQb3IDQ-E8362ePIGD3HlL_db6vMZRt6OKmq', 'andriod', 1, '2018-01-27 08:58:58'),
(435, 104, '871', 'f_mgnyPx6ZU:APA91bGxXiMgKaQB1nb0AZWMqenfFflBLf-Rka5NKbDR0tk07pSxYDKq7k_tSYXjS_1S0-ATZE9bWv_gfkEEQNBfkhMMQnFjogWvSuKFgQb3IDQ-E8362ePIGD3HlL_db6vMZRt6OKmq', 'andriod', 1, '2018-01-27 08:58:59'),
(436, 104, '644', 'f_mgnyPx6ZU:APA91bGxXiMgKaQB1nb0AZWMqenfFflBLf-Rka5NKbDR0tk07pSxYDKq7k_tSYXjS_1S0-ATZE9bWv_gfkEEQNBfkhMMQnFjogWvSuKFgQb3IDQ-E8362ePIGD3HlL_db6vMZRt6OKmq', 'andriod', 1, '2018-01-27 09:04:51'),
(437, 104, '597', 'f_mgnyPx6ZU:APA91bGxXiMgKaQB1nb0AZWMqenfFflBLf-Rka5NKbDR0tk07pSxYDKq7k_tSYXjS_1S0-ATZE9bWv_gfkEEQNBfkhMMQnFjogWvSuKFgQb3IDQ-E8362ePIGD3HlL_db6vMZRt6OKmq', 'andriod', 1, '2018-01-27 09:04:52'),
(438, 104, '656', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-27 09:25:05'),
(439, 104, '852', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-27 09:25:06'),
(440, 104, '588', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-27 09:32:31'),
(441, 104, '882', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-27 09:32:31'),
(442, 104, '983', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-27 10:06:37'),
(443, 104, '797', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-27 10:06:37'),
(444, 104, '866', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-27 10:18:38'),
(445, 104, '483', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-27 10:18:38'),
(446, 104, '472', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-27 11:33:36'),
(447, 104, '844', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-27 11:33:37'),
(448, 104, '720', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-27 11:36:56'),
(449, 104, '932', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-27 11:36:56'),
(450, 104, '949', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-27 11:43:03'),
(451, 104, '904', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-27 11:43:04'),
(452, 104, '959', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-27 11:58:32'),
(453, 104, '989', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-27 11:58:33'),
(454, 104, '777', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-27 12:21:22'),
(455, 104, '923', 'fEEVt2HR7b8:APA91bGkC0cvZu2AACFaSmLySzYm8aHLMUueWzw-DKSYOjayAmY24u1OMpcm9YqKwDAw3k_XGMEY1LALatnsst9Zftq9pJgqhZtC4jnTKACYER2R2AgELS5S-JzgNBi5U2FSl9j9EbZ7', 'andriod', 1, '2018-01-27 12:21:24');

-- --------------------------------------------------------

--
-- Table structure for table `map`
--

CREATE TABLE IF NOT EXISTS `map` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `vehicle_number` varchar(255) NOT NULL,
  `lat` float(15,11) NOT NULL,
  `lng` float(15,11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `map`
--

INSERT INTO `map` (`id`, `vehicle_number`, `lat`, `lng`) VALUES
(1, 'DL34TD4355', 28.57819938660, 77.31780242920);

-- --------------------------------------------------------

--
-- Table structure for table `map_auto`
--

CREATE TABLE IF NOT EXISTS `map_auto` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `vehicle_number` varchar(255) NOT NULL,
  `lat` float(15,11) NOT NULL,
  `lng` float(15,11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `map_auto`
--

INSERT INTO `map_auto` (`id`, `vehicle_number`, `lat`, `lng`) VALUES
(1, 'UP16DY5436', 28.58530044556, 77.31150054932),
(2, 'DL34TH3456', 28.57080078125, 77.32610321045);

-- --------------------------------------------------------

--
-- Table structure for table `map_bike`
--

CREATE TABLE IF NOT EXISTS `map_bike` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `vehicle_number` varchar(255) NOT NULL,
  `lat` float(15,11) NOT NULL,
  `lng` float(15,11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `map_bike`
--

INSERT INTO `map_bike` (`id`, `vehicle_number`, `lat`, `lng`) VALUES
(1, 'UP16DY5436', 28.58530044556, 77.31150054932),
(2, 'DL34TH3456', 28.57080078125, 77.32610321045);

-- --------------------------------------------------------

--
-- Table structure for table `passenger_verify`
--

CREATE TABLE IF NOT EXISTS `passenger_verify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) NOT NULL,
  `otp` int(11) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=89 ;

--
-- Dumping data for table `passenger_verify`
--

INSERT INTO `passenger_verify` (`id`, `phone`, `otp`, `created_date_time`) VALUES
(78, '8439666778', 6256, '2018-01-20 07:43:40'),
(77, '8439666778', 9303, '2018-01-20 07:43:38'),
(84, '5998888888', 4205, '2018-01-20 10:44:13'),
(83, '5998888888', 1279, '2018-01-20 10:44:11');

-- --------------------------------------------------------

--
-- Table structure for table `payment_report`
--

CREATE TABLE IF NOT EXISTS `payment_report` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `ride_number` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `passenger_name` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `total_fare` varchar(255) NOT NULL,
  `ride_status` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `payment_report`
--

INSERT INTO `payment_report` (`id`, `ride_number`, `driver_name`, `passenger_name`, `ride_date`, `total_fare`, `ride_status`, `payment_method`, `vehicle_number`) VALUES
(1, '250', 'Saumya', 'sumit', '2017-01-31', '50', 'Finished', 'Cash', 'DL34TD4355'),
(3, '234', 'Saumyag', 'Rahul', '2017-10-10', '45', 'Finished', 'online', 'JK45RE2314');

-- --------------------------------------------------------

--
-- Table structure for table `payment_report_auto`
--

CREATE TABLE IF NOT EXISTS `payment_report_auto` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `ride_number` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `passenger_name` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `total_fare` varchar(255) NOT NULL,
  `ride_status` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `payment_report_auto`
--

INSERT INTO `payment_report_auto` (`id`, `ride_number`, `driver_name`, `passenger_name`, `ride_date`, `total_fare`, `ride_status`, `payment_method`, `vehicle_number`) VALUES
(1, '34', 'Rahul', 'sumit', '2017-10-06', '345', 'Finished', 'Cash', 'UP16FT2345'),
(2, '543', 'Rakesh Jha', 'Rahul', '2017-10-16', '677', 'Finished', 'online', 'JK45RE2314');

-- --------------------------------------------------------

--
-- Table structure for table `payment_report_bike`
--

CREATE TABLE IF NOT EXISTS `payment_report_bike` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `ride_number` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `passenger_name` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `total_fare` varchar(255) NOT NULL,
  `ride_status` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `payment_report_bike`
--

INSERT INTO `payment_report_bike` (`id`, `ride_number`, `driver_name`, `passenger_name`, `ride_date`, `total_fare`, `ride_status`, `payment_method`, `vehicle_number`) VALUES
(1, '65', 'Ratan', 'Ritesh', '2017-10-10', '654', 'Finished', 'Cash', 'DL34TD4355');

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE IF NOT EXISTS `rating` (
  `rating_id` int(30) NOT NULL AUTO_INCREMENT,
  `driver_id` int(30) NOT NULL,
  `passenger_id` int(30) NOT NULL,
  `rating_number` int(30) NOT NULL,
  `rating_1` int(30) NOT NULL,
  `rating_2` int(30) NOT NULL,
  `rating_3` int(30) NOT NULL,
  `rating_4` int(30) NOT NULL,
  `rating_5` int(30) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`rating_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=74 ;

--
-- Dumping data for table `rating`
--

INSERT INTO `rating` (`rating_id`, `driver_id`, `passenger_id`, `rating_number`, `rating_1`, `rating_2`, `rating_3`, `rating_4`, `rating_5`, `created`, `modified`) VALUES
(1, 3, 2, 3, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 3, 3, 1, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 3, 5, 4, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 10, 16, 0, 0, 0, 0, 0, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 10, 16, 0, 0, 0, 0, 0, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 0, 0, 0, 0, 0, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 0, 0, 0, 0, 0, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 0, 0, 0, 0, 0, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 0, 0, 0, 0, 0, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 0, 0, 0, 0, 0, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 0, 0, 0, 0, 0, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 0, 0, 0, 0, 0, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 0, 0, 0, 0, 0, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 0, 0, 0, 0, 0, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 0, 0, 0, 0, 0, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 0, 0, 0, 0, 0, 0, 4, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 0, 0, 0, 0, 0, 0, 4, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 0, 0, 0, 0, 0, 0, 4, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 0, 0, 0, 0, 0, 0, 4, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 0, 0, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 0, 0, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 0, 0, 0, 0, 0, 0, 4, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 0, 0, 0, 0, 0, 0, 4, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 0, 0, 0, 0, 0, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 0, 0, 0, 0, 0, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 0, 0, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 0, 0, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 0, 0, 0, 0, 0, 0, 0, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 0, 0, 0, 0, 0, 0, 0, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 0, 0, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 0, 0, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 0, 0, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 0, 0, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 0, 0, 0, 0, 0, 0, 0, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 0, 0, 0, 0, 0, 0, 0, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 0, 0, 0, 0, 0, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 0, 0, 0, 0, 0, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 0, 0, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 0, 0, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 10, 16, 0, 0, 0, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 10, 16, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 10, 16, 0, 0, 0, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 10, 16, 0, 0, 0, 0, 0, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 10, 16, 0, 0, 0, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 10, 16, 0, 0, 0, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 10, 16, 0, 0, 0, 0, 4, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 10, 16, 0, 0, 0, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 10, 16, 0, 0, 0, 0, 4, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 10, 16, 0, 0, 0, 0, 4, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 10, 16, 0, 0, 0, 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 10, 16, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 10, 16, 0, 0, 0, 0, 0, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 10, 16, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 10, 16, 0, 0, 0, 0, 0, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 10, 16, 0, 0, 0, 0, 0, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 104, 69, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 104, 69, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 104, 69, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 104, 69, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 104, 69, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 104, 69, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 104, 69, 0, 0, 0, 0, 0, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 104, 69, 0, 0, 0, 0, 0, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 104, 69, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 104, 69, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 104, 69, 0, 0, 0, 0, 0, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 104, 69, 0, 0, 0, 0, 0, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 104, 69, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 104, 69, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 104, 69, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 104, 69, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 104, 69, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 104, 69, 0, 0, 2, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `rider`
--

CREATE TABLE IF NOT EXISTS `rider` (
  `user_id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `registration_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `password` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `contact_name1` varchar(255) NOT NULL,
  `contact_phone1` varchar(255) NOT NULL,
  `contact_name2` varchar(255) NOT NULL,
  `contact_phone2` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

--
-- Dumping data for table `rider`
--

INSERT INTO `rider` (`user_id`, `name`, `email`, `phone`, `registration_date`, `password`, `city`, `contact_name1`, `contact_phone1`, `contact_name2`, `contact_phone2`) VALUES
(69, 'saumya54', 'saumya@gmail.com', '8077024382', '2018-01-20 07:04:11', '35403e36de4311aade75c685fceeb379', '', '', '', '', ''),
(70, 'rakesh', 'rakesh.jha@isiwal.com', '9717844140', '2018-01-20 07:47:01', '25f9e794323b453885f5181f1b624d0b', '', '', '', '', ''),
(71, 'Rakesh jha', 'rak@gmail.cpm', '9039765775', '2018-01-20 11:24:52', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', '', ''),
(72, 'Rakeshjha', 'rkkkk@gmail.com', '9205629660', '2018-01-25 15:00:53', '827ccb0eea8a706c4c34a16891f84e7b', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ride_later_booking`
--

CREATE TABLE IF NOT EXISTS `ride_later_booking` (
  `booking_id` int(11) NOT NULL AUTO_INCREMENT,
  `ride_type` varchar(255) NOT NULL,
  `vehicle_type` varchar(255) NOT NULL,
  `vehicle_name` varchar(255) NOT NULL,
  `source` varchar(255) NOT NULL,
  `destination` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `driver_phone` varchar(255) NOT NULL,
  `passenger_id` int(11) NOT NULL,
  `passenger_phone` varchar(255) NOT NULL,
  `passenger_name` varchar(255) NOT NULL,
  `number_passenger` int(11) NOT NULL,
  `payment_mode` varchar(100) NOT NULL,
  `booking_date` date NOT NULL,
  `booking_time` time NOT NULL,
  `fare` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `trip_complete_status` tinyint(4) NOT NULL DEFAULT '0',
  `is_trip_start` tinyint(4) NOT NULL DEFAULT '0',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trip_id` int(11) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL,
  PRIMARY KEY (`booking_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `ride_later_booking`
--

INSERT INTO `ride_later_booking` (`booking_id`, `ride_type`, `vehicle_type`, `vehicle_name`, `source`, `destination`, `driver_id`, `driver_name`, `driver_phone`, `passenger_id`, `passenger_phone`, `passenger_name`, `number_passenger`, `payment_mode`, `booking_date`, `booking_time`, `fare`, `status`, `trip_complete_status`, `is_trip_start`, `created_date_time`, `trip_id`, `vehicle_number`) VALUES
(42, '', 'Basic', '', 'C-, 7, Amaltash Marg, C Block, Sector 10, Noida, Uttar Pradesh 201301, India', 'Sector 20, Noida, Uttar Pradesh 201301, India', 104, '', '', 69, '', '', 0, '', '2018-01-24', '21:03:00', '0', 0, 0, 0, '2018-01-24 14:34:04', 0, ''),
(43, '', 'basic', '', 'noida', 'ghaziabadnnn', 104, '', '', 69, '', '', 0, '', '2018-02-24', '02:23:23', '216.428', 0, 0, 0, '2018-01-24 14:49:57', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `taxi`
--

CREATE TABLE IF NOT EXISTS `taxi` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `taxi_name` varchar(255) NOT NULL,
  `model_name` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL,
  `vehicle_registration_number` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `taxi_auto`
--

CREATE TABLE IF NOT EXISTS `taxi_auto` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `taxi_name` varchar(255) NOT NULL,
  `model_name` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL,
  `vehicle_registration_number` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `taxi_auto`
--

INSERT INTO `taxi_auto` (`id`, `taxi_name`, `model_name`, `driver_name`, `vehicle_number`, `vehicle_registration_number`, `image`) VALUES
(1, 'Bajaj', 'vp7', 'Rahul', 'DL34TD4355', 'rgfd65465nu76', 'f_icon2.png');

-- --------------------------------------------------------

--
-- Table structure for table `taxi_bike`
--

CREATE TABLE IF NOT EXISTS `taxi_bike` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `taxi_name` varchar(255) NOT NULL,
  `model_name` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL,
  `vehicle_registration_number` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `taxi_bike`
--

INSERT INTO `taxi_bike` (`id`, `taxi_name`, `model_name`, `driver_name`, `vehicle_number`, `vehicle_registration_number`, `image`) VALUES
(1, 'TVS', 'Sport', 'Saumya', 'UP16FT2345', 'R2345S5678', '_DSC0204.png');

-- --------------------------------------------------------

--
-- Table structure for table `temporary_driver`
--

CREATE TABLE IF NOT EXISTS `temporary_driver` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `driving_license` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `city` varchar(255) DEFAULT NULL,
  `aadharcard_number` varchar(255) DEFAULT NULL,
  `pan_number` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `aadhar_image` varchar(255) DEFAULT NULL,
  `dl_image` varchar(255) NOT NULL,
  `vehicle_name` varchar(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL,
  `vehicle_doc` varchar(255) DEFAULT NULL,
  `lat` float(15,11) DEFAULT NULL,
  `lng` float(15,11) NOT NULL,
  `date` date NOT NULL,
  `time` date NOT NULL,
  `registration_date` date NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isapproved` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=85 ;

--
-- Dumping data for table `temporary_driver`
--

INSERT INTO `temporary_driver` (`user_id`, `name`, `email`, `phone`, `driving_license`, `password`, `city`, `aadharcard_number`, `pan_number`, `image`, `aadhar_image`, `dl_image`, `vehicle_name`, `vehicle_number`, `vehicle_doc`, `lat`, `lng`, `date`, `time`, `registration_date`, `status`, `created_date_time`, `isapproved`) VALUES
(72, 'rakeshjha', 'rakesh@gmail.com', '9717844140', 'hffdd55443322gg', '827ccb0eea8a706c4c34a16891f84e7b', 'hariom', NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, 0.00000000000, '0000-00-00', '0000-00-00', '0000-00-00', 0, '2018-01-15 06:23:42', 0),
(74, 'ttkk', 'bbhh@gg.bom', '9973746001', 'bnkhgggfgggy666', '81dc9bdb52d04dc20036dbd8313ed055', 'gjjhgf', NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, 0.00000000000, '0000-00-00', '0000-00-00', '0000-00-00', 0, '2018-01-15 09:27:44', 0),
(76, 'Abid', 'mohdabid75@gmail.com', '9205629660', '123412341234', '75ac2c91d93efba8651671f18ec013d0', 'Noida', NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, 0.00000000000, '0000-00-00', '0000-00-00', '0000-00-00', 0, '2018-01-15 10:45:02', 0),
(80, 'Ritesh kumar', 'riteshkr@gmail.com', '9039765775', 'kloo7h66hgggggg', '6b54185ccddbef6cc10b6586179db501', 'noida', NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, 0.00000000000, '0000-00-00', '0000-00-00', '0000-00-00', 0, '2018-01-17 11:40:02', 0),
(81, 'Ranjeev', 'ranjeevkumar456@gmail.com', '7678263182', '123456', 'ac82129596b6b73fdcf68658540eb76c', 'noida', NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, 0.00000000000, '0000-00-00', '0000-00-00', '0000-00-00', 0, '2018-01-20 07:01:36', 0),
(82, 'Ranjeev', 'ranjeevkumar456@gmail.com', '7678263182', '123456', 'ac82129596b6b73fdcf68658540eb76c', 'noida', NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, 0.00000000000, '0000-00-00', '0000-00-00', '0000-00-00', 0, '2018-01-20 07:01:38', 0),
(83, 'saumya', 'saumya@gmail.com', '8077024382', 'sjalagakagakavj', '25f9e794323b453885f5181f1b624d0b', 'noida', NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, 0.00000000000, '0000-00-00', '0000-00-00', '0000-00-00', 0, '2018-01-20 07:59:08', 0),
(84, 'saumya', 'saumya@gmail.com', '8077024382', 'sjalagakagakavj', '25f9e794323b453885f5181f1b624d0b', 'noida', NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, 0.00000000000, '0000-00-00', '0000-00-00', '0000-00-00', 0, '2018-01-20 07:59:10', 0);

-- --------------------------------------------------------

--
-- Table structure for table `temporary_rider`
--

CREATE TABLE IF NOT EXISTS `temporary_rider` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `registration_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `password` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `contact_name1` varchar(255) NOT NULL,
  `contact_phone1` varchar(255) NOT NULL,
  `contact_name2` varchar(255) NOT NULL,
  `contact_phone2` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=89 ;

--
-- Dumping data for table `temporary_rider`
--

INSERT INTO `temporary_rider` (`user_id`, `name`, `email`, `phone`, `registration_date`, `password`, `city`, `contact_name1`, `contact_phone1`, `contact_name2`, `contact_phone2`) VALUES
(58, 'driver', 'drivmm2er435@gmail.com', '9990112928', '2018-01-15 10:12:39', '25f9e794323b453885f5181f1b624d0b', '', '', '', '', ''),
(77, 'saumya', 'saumyaranjan@gmail.com', '8439666778', '2018-01-20 07:43:38', 'e0eb969fb7f476ee3f6721ec5947d735', '', '', '', '', ''),
(60, 'raks', 'jha@gg.vom', '7042071224', '2018-01-15 10:15:27', '827ccb0eea8a706c4c34a16891f84e7b', '', '', '', '', ''),
(70, 'saumya', 'saumya@gmail.com', '8077024382', '2018-01-20 07:00:47', 'e0eb969fb7f476ee3f6721ec5947d735', '', '', '', '', ''),
(64, 'saumya', 'saumya@gmail.com', '8077024382', '2018-01-20 06:55:37', 'e0eb969fb7f476ee3f6721ec5947d735', '', '', '', '', ''),
(65, 'saumya', 'saumya@gmail.com', '8077024382', '2018-01-20 06:56:24', 'e0eb969fb7f476ee3f6721ec5947d735', '', '', '', '', ''),
(66, 'saumya', 'saumya@gmail.com', '8077024382', '2018-01-20 06:56:26', 'e0eb969fb7f476ee3f6721ec5947d735', '', '', '', '', ''),
(67, 'saumya', 'saumya@gmail.com', '8077024382', '2018-01-20 06:57:50', 'e0eb969fb7f476ee3f6721ec5947d735', '', '', '', '', ''),
(68, 'saumya', 'saumya@gmail.com', '8077024382', '2018-01-20 06:57:51', 'e0eb969fb7f476ee3f6721ec5947d735', '', '', '', '', ''),
(71, 'saumya', 'saumya@gmail.com', '8077024382', '2018-01-20 07:01:36', 'e0eb969fb7f476ee3f6721ec5947d735', '', '', '', '', ''),
(72, 'saumya', 'saumya@gmail.com', '8077024382', '2018-01-20 07:01:38', 'e0eb969fb7f476ee3f6721ec5947d735', '', '', '', '', ''),
(73, 'saumya', 'saumya@gmail.com', '8077024382', '2018-01-20 07:03:09', 'e0eb969fb7f476ee3f6721ec5947d735', '', '', '', '', ''),
(74, 'saumya', 'saumya@gmail.com', '8077024382', '2018-01-20 07:03:10', 'e0eb969fb7f476ee3f6721ec5947d735', '', '', '', '', ''),
(75, 'saumya', 'saumya@gmail.com', '8077024382', '2018-01-20 07:03:44', 'e0eb969fb7f476ee3f6721ec5947d735', '', '', '', '', ''),
(76, 'saumya', 'saumya@gmail.com', '8077024382', '2018-01-20 07:03:45', 'e0eb969fb7f476ee3f6721ec5947d735', '', '', '', '', ''),
(78, 'saumya', 'saumyaranjan@gmail.com', '8439666778', '2018-01-20 07:43:40', 'e0eb969fb7f476ee3f6721ec5947d735', '', '', '', '', ''),
(83, 'ghffdd', 'rajkj@gy.hhg', '5998888888', '2018-01-20 10:44:11', '81dc9bdb52d04dc20036dbd8313ed055', '', '', '', '', ''),
(80, 'rakesh', 'rakesh.jha@isiwal.com', '9717844140', '2018-01-20 07:45:30', '25f9e794323b453885f5181f1b624d0b', '', '', '', '', ''),
(81, 'rakesh', 'rakesh.jha@isiwal.com', '9717844140', '2018-01-20 07:46:42', '25f9e794323b453885f5181f1b624d0b', '', '', '', '', ''),
(82, 'rakesh', 'rakesh.jha@isiwal.com', '9717844140', '2018-01-20 07:46:44', '25f9e794323b453885f5181f1b624d0b', '', '', '', '', ''),
(84, 'ghffdd', 'rajkj@gy.hhg', '5998888888', '2018-01-20 10:44:13', '81dc9bdb52d04dc20036dbd8313ed055', '', '', '', '', ''),
(87, 'rakeshjha', 'rak@gmail.com', '9205629660', '2018-01-25 15:00:30', '827ccb0eea8a706c4c34a16891f84e7b', '', '', '', '', ''),
(86, 'rakesh', 'rak@gmail.cpm', '9039765775', '2018-01-20 11:24:19', '75ac2c91d93efba8651671f18ec013d0', '', '', '', '', ''),
(88, 'rakeshjha', 'rak@gmail.com', '9205629660', '2018-01-25 15:00:32', '827ccb0eea8a706c4c34a16891f84e7b', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `transction`
--

CREATE TABLE IF NOT EXISTS `transction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) NOT NULL,
  `passenger_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `amount` varchar(50) NOT NULL,
  `transction_id` int(11) NOT NULL,
  `transction_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `transction`
--

INSERT INTO `transction` (`id`, `booking_id`, `passenger_id`, `driver_id`, `amount`, `transction_id`, `transction_date_time`) VALUES
(1, 29, 104, 77, '77.328692', 77, '2018-01-25 06:17:54'),
(2, 29, 104, 77, '77.328692', 77, '2018-01-25 06:18:33'),
(3, 29, 104, 77, '77.328692', 77, '2018-01-25 06:19:07'),
(4, 29, 104, 77, '77.328692', 77, '2018-01-25 06:20:35'),
(5, 1271, 104, 104, '77', 12, '2018-01-25 06:22:23');

-- --------------------------------------------------------

--
-- Table structure for table `trips`
--

CREATE TABLE IF NOT EXISTS `trips` (
  `booking_id` int(255) NOT NULL AUTO_INCREMENT,
  `ride_type` varchar(255) NOT NULL,
  `vehicle_type` varchar(255) NOT NULL,
  `vehicle_name` varchar(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL,
  `source` varchar(200) NOT NULL,
  `destination` varchar(200) NOT NULL,
  `driver_id` int(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `driver_phone` varchar(100) NOT NULL,
  `passenger_id` int(255) NOT NULL,
  `passenger_phone` varchar(255) NOT NULL,
  `passenger_name` varchar(255) NOT NULL,
  `number_passenger` varchar(255) NOT NULL,
  `payment_mode` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `fare` varchar(255) NOT NULL,
  `ride_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `is_trip_accept` tinyint(1) NOT NULL DEFAULT '0',
  `trip_complete_status` tinyint(1) NOT NULL DEFAULT '0',
  `isTripStart` tinyint(1) NOT NULL DEFAULT '0',
  `booking_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_trip_deleted_by_driver` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`booking_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1310 ;

--
-- Dumping data for table `trips`
--

INSERT INTO `trips` (`booking_id`, `ride_type`, `vehicle_type`, `vehicle_name`, `vehicle_number`, `source`, `destination`, `driver_id`, `driver_name`, `driver_phone`, `passenger_id`, `passenger_phone`, `passenger_name`, `number_passenger`, `payment_mode`, `time`, `fare`, `ride_date`, `status`, `is_trip_accept`, `trip_complete_status`, `isTripStart`, `booking_date_time`, `is_trip_deleted_by_driver`) VALUES
(1268, 'Mini', 'Mini', 'Suzuki', 'Mh04-Bh2020', '28.5891277,77.3337379', '28.570317000000003,77.3218196', 104, 'Abidkhan1', '9205629660', 69, '9891063279', '', '1', 'Cash', '13 mins', '0', '2018-01-24 11:59:29', 1, 1, 1, 1, '2018-01-24 11:59:29', 0),
(1269, 'Mini', 'Mini', 'Suzuki', 'Mh04-Bh2020', '28.5885113,77.3339532', '28.588517999999997,77.33366199999999', 104, 'Abid', '9205629660', 69, '', '', '1', 'Cash', '2 mins', '0', '2018-01-24 13:13:21', 1, 1, 1, 1, '2018-01-24 13:13:21', 0),
(1270, 'Mini', 'Mini', '', '', '28.5885429,77.3340737', '28.5897221,77.334137', 0, '', '', 69, '', '', '1', 'Cash', '2 mins', '7.9666', '2018-01-24 14:52:15', 0, 0, 0, 0, '2018-01-24 14:52:15', 0),
(1271, 'Mini', 'Mini', '', '', '28.5901657,77.3340577', '28.585727,77.330292', 0, '', '', 69, '', '', '1', 'Cash', '3 mins', '13.8226', '2018-01-24 15:53:02', 0, 0, 0, 0, '2018-01-24 15:53:02', 0),
(1272, 'Mini', 'Mini', '', '', '28.5901996,77.3344166', '28.589916,77.335993', 0, '', '', 69, '', '', '1', 'Cash', '2 mins', '3.8064', '2018-01-25 10:27:41', 0, 0, 0, 0, '2018-01-25 10:27:41', 0),
(1273, 'micro', '', '', '', '28.5885429,77.3340737', '28.589007,77.33424629999999', 0, '', '', 0, '9205629660', '', '3', 'Cash', '2 mins', '20.9328', '2018-01-25 13:29:25', 0, 0, 0, 0, '2018-01-25 13:29:25', 0),
(1274, 'micro', '', '', '', '28.5885429,77.3340737', '28.589007,77.33424629999999', 0, '', '', 0, '9205629660', '', '3', 'Cash', '2 mins', '20.9328', '2018-01-25 13:29:27', 0, 0, 0, 0, '2018-01-25 13:29:27', 0),
(1275, 'mini', '', '', '', '28.5885429,77.3340737', '28.589007,77.33424629999999', 104, '', '', 0, '8678678678', '', '2', 'cash', '2 mins', '20.9328', '2018-01-25 13:40:30', 0, 0, 0, 0, '2018-01-25 13:40:30', 0),
(1276, 'micro', '', '', '', '28.5905243,77.3340577', '28.589195699999998,77.33399039999999', 0, '', '', 0, '9874589658', '', '3', 'Cash', '1 min', '6.7032', '2018-01-25 13:41:48', 0, 0, 0, 0, '2018-01-25 13:41:48', 0),
(1277, 'micro', '', '', '', '28.5905243,77.3340577', '28.589195699999998,77.33399039999999', 0, '', '', 0, '9874589658', '', '3', 'Cash', '1 min', '6.7032', '2018-01-25 13:41:50', 0, 0, 0, 0, '2018-01-25 13:41:50', 0),
(1278, 'micro', '', '', '', '28.5905243,77.3340577', '28.589195699999998,77.33399039999999', 0, '', '', 0, '9874589658', '', '3', 'Cash', '1 min', '6.7032', '2018-01-25 13:42:45', 0, 0, 0, 0, '2018-01-25 13:42:45', 0),
(1279, 'micro', '', '', '', '28.5905243,77.3340577', '28.589195699999998,77.33399039999999', 0, '', '', 0, '9874589658', '', '3', 'Cash', '1 min', '6.7032', '2018-01-25 13:42:47', 0, 0, 0, 0, '2018-01-25 13:42:47', 0),
(1280, 'micro', '', '', '', '28.5885429,77.3340737', '28.587205999999995,77.333989', 0, '', '', 0, '8596589658', '', '4', 'Cash', '3 mins', '33.5552', '2018-01-25 13:55:37', 0, 0, 0, 0, '2018-01-25 13:55:37', 0),
(1281, 'micro', '', '', '', '28.5885429,77.3340737', '28.587205999999995,77.333989', 0, '', '', 0, '8596589658', '', '4', 'Cash', '3 mins', '33.5552', '2018-01-25 13:55:39', 0, 0, 0, 0, '2018-01-25 13:55:39', 0),
(1282, 'micro', '', '', '', '28.5885429,77.3340737', '28.588817700000003,77.334137', 0, '', '', 0, '8899665588', '', '1', 'Cash', '2 mins', '20.0704', '2018-01-25 14:01:22', 0, 0, 0, 0, '2018-01-25 14:01:22', 0),
(1283, 'micro', '', '', '', '28.5885429,77.3340737', '28.588817700000003,77.334137', 0, '', '', 0, '8899665588', '', '1', 'Cash', '2 mins', '20.0704', '2018-01-25 14:01:24', 0, 0, 0, 0, '2018-01-25 14:01:24', 0),
(1284, 'semi', '', '', '', '28.5885429,77.3340737', '28.589007,77.33424629999999', 104, '', '', 0, '8678678678', '', '2', 'cash', '2 mins', '12.899304', '2018-01-25 14:24:03', 0, 0, 1, 0, '2018-01-25 14:24:03', 0),
(1285, 'micro', '', '', '', '28.5885113,77.3339532', '28.5879357,77.3332946', 0, '', '', 0, '9205629660', '', '1', 'Cash', '1 min', '3.528', '2018-01-25 16:24:27', 0, 0, 0, 0, '2018-01-25 16:24:27', 0),
(1286, 'micro', '', '', '', '28.5885113,77.3339532', '28.5879357,77.3332946', 0, '', '', 0, '9205629660', '', '3', 'Cash', '1 min', '3.528', '2018-01-25 16:24:36', 0, 0, 0, 0, '2018-01-25 16:24:36', 0),
(1289, 'micro', '', '', '', '28.5893807,77.3333398', '28.587434899999998,77.3348062', 0, '', '', 0, '9205629660', '', '1', 'Cash', '7 mins', '46.5304', '2018-01-27 06:55:56', 0, 0, 0, 0, '2018-01-27 06:55:56', 0),
(1290, 'micro', '', '', '', '28.5883727,77.3340577', '28.587434899999998,77.3348062', 0, '', '', 0, '9205629660', '', '3', 'Cash', '2 mins', '8.5456', '2018-01-27 07:18:16', 0, 0, 0, 0, '2018-01-27 07:18:16', 0),
(1291, 'micro', '', '', '', '28.5894146,77.3336987', '28.589233099999998,77.333979', 0, '', '', 0, '9205629660', '', '1', 'Cash', '1 min', '0.3136', '2018-01-27 07:37:12', 0, 0, 0, 0, '2018-01-27 07:37:12', 0),
(1292, 'micro', '', '', '', '28.5883727,77.3340577', '28.587434899999998,77.3348062', 0, '', '', 0, '8569589658', '', '1', 'Cash', '2 mins', '8.5456', '2018-01-27 07:44:08', 0, 0, 0, 0, '2018-01-27 07:44:08', 0),
(1293, 'micro', '', '', '', '28.5883727,77.3340577', '28.588517999999997,77.33366199999999', 0, '', '', 0, '9650708098', '', '1', 'Cash', '5 mins', '39.5528', '2018-01-27 07:54:47', 0, 0, 0, 0, '2018-01-27 07:54:47', 0),
(1294, 'micro', '', '', '', '28.5883727,77.3340577', '28.587205999999995,77.333989', 0, '', '', 0, '9650708098', '', '1', 'Cash', '1 min', '7.448', '2018-01-27 07:59:05', 0, 0, 0, 0, '2018-01-27 07:59:05', 0),
(1295, 'micro', '', '', '', '28.5890899,77.3340577', '28.587434899999998,77.3348062', 0, '', '', 0, '9650701465', '', '1', 'Cash', '6 mins', '42.14', '2018-01-27 08:17:21', 0, 0, 0, 0, '2018-01-27 08:17:21', 0),
(1296, 'luxary', '', '', '', '28.5885429,77.3340737', '28.589007,77.33424629999999', 104, '', '', 0, '9450875645', '', '2', 'cash', '2 mins', '20.9328', '2018-01-27 08:35:11', 0, 0, 0, 0, '2018-01-27 08:35:11', 0),
(1297, 'micro', '', '', '', '28.5885429,77.3340737', '28.589007,77.33424629999999', 130, '', '', 0, '9450875645', '', '2', 'cash', '2 mins', '20.9328', '2018-01-27 09:39:20', 0, 0, 0, 0, '2018-01-27 09:39:20', 0),
(1298, 'micro', '', '', '', '28.5885429,77.3340737', '28.589007,', 130, '', '', 0, '9450875645', '', '2', 'cash', '', '0', '2018-01-27 09:40:25', 0, 0, 0, 0, '2018-01-27 09:40:25', 0),
(1299, 'micro', '', '', '', '28.5885429,77.3340737', '28.589007,77.3342462999999', 130, '', '', 0, '9450875645', '', '2', 'cash', '2 mins', '20.9328', '2018-01-27 09:43:51', 0, 0, 0, 0, '2018-01-27 09:43:51', 0),
(1300, 'micro', '', '', '', '28.5900436,77.3337319', '28.589988599999998,77.3337416', 130, '', '', 0, '9450875645', '', '2', 'cash', '1 min', '0.196', '2018-01-27 09:56:05', 0, 0, 0, 0, '2018-01-27 09:56:05', 0),
(1301, 'micro', '', '', '', '28.5900436,', '28.589988599999998,77.3337416', 130, '', '', 0, '9450875645', '', '2', 'cash', '', '0', '2018-01-27 10:09:48', 0, 0, 0, 0, '2018-01-27 10:09:48', 0),
(1302, 'micro', '', '', '', '28.5900436,77.3337319', '28.589988599999998,77.33374166', 130, '', '', 0, '9450875645', '', '2', 'cash', '1 min', '0.196', '2018-01-27 10:11:58', 0, 0, 0, 0, '2018-01-27 10:11:58', 0),
(1303, 'micro', '', 'Suzuki', 'Mh04-Bh2020', '28.5900617,77.3337397', '28.589916,77.335993', 104, 'Abidkhan1', '9205629660', 0, '8547856985', '', '3', 'Online', '2 mins', '14.89986', '2018-01-27 10:19:19', 0, 0, 1, 1, '2018-01-27 10:19:19', 0),
(1304, 'micro', '', '', '', '28.5900436,77.3337319', '28.589988599999998,77.333974166', 130, '', '', 0, '9450875645', '', '2', 'cash', '1 min', '0.784', '2018-01-27 10:24:06', 0, 0, 0, 0, '2018-01-27 10:24:06', 0),
(1305, 'micro', '', '', '', '28.5900436,77.3337319', '28.58998859999998,77.333974166', 130, '', '', 0, '9450875645', '', '2', 'cash', '1 min', '0.784', '2018-01-27 10:26:37', 0, 0, 0, 0, '2018-01-27 10:26:37', 0),
(1306, 'micro', '', '', '', '28.5885429,77.3340737', '28.589007,77.334246299', 130, '', '', 0, '9450875645', '', '2', 'cash', '2 mins', '20.9328', '2018-01-27 10:28:10', 0, 0, 0, 0, '2018-01-27 10:28:10', 0),
(1307, 'micro', '', '', '', '28.5885429,77.3340737', '28.589007,77.334246299', 130, '', '', 0, '9450875645', '', '1', 'cash', '2 mins', '20.9328', '2018-01-27 10:29:24', 0, 0, 0, 0, '2018-01-27 10:29:24', 0),
(1308, 'micro', '', '', '', '28.5885429,77.3340737', '28.589007,77.334246299', 130, '', '', 0, '9450875645', '', '3', 'cash', '2 mins', '20.9328', '2018-01-27 10:32:13', 0, 0, 0, 0, '2018-01-27 10:32:13', 0),
(1309, 'micro', '', 'Suzuki', 'Mh04-Bh2020', '28.59006,77.3337389', '28.589916,77.335993', 104, 'Abidkhan1', '9205629660', 0, '8569325154', '', '1', 'Cash', '2 mins', '5', '2018-01-27 11:34:09', 0, 0, 1, 1, '2018-01-27 11:34:09', 0);

-- --------------------------------------------------------

--
-- Table structure for table `trips_auto`
--

CREATE TABLE IF NOT EXISTS `trips_auto` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `trip_type` varchar(255) NOT NULL,
  `trip_number` varchar(255) NOT NULL,
  `trip_date` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `passenger_name` varchar(255) NOT NULL,
  `fare` varchar(255) NOT NULL,
  `taxi` varchar(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `source` varchar(200) NOT NULL,
  `destination` varchar(200) NOT NULL,
  `driver_phone` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `trips_auto`
--

INSERT INTO `trips_auto` (`id`, `trip_type`, `trip_number`, `trip_date`, `driver_name`, `passenger_name`, `fare`, `taxi`, `vehicle_number`, `time`, `source`, `destination`, `driver_phone`) VALUES
(1, 'Rental', '453', '2017-10-11', 'Rahul', 'sumit', '45', 'basic', 'UP16FT2345', '20minutes', 'Gurugram', 'delhi', '7169573332');

-- --------------------------------------------------------

--
-- Table structure for table `trips_bike`
--

CREATE TABLE IF NOT EXISTS `trips_bike` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `trip_type` varchar(255) NOT NULL,
  `trip_number` varchar(255) NOT NULL,
  `trip_date` varchar(255) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `passenger_name` varchar(255) NOT NULL,
  `fare` varchar(255) NOT NULL,
  `taxi` varchar(255) NOT NULL,
  `vehicle_number` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `source` varchar(200) NOT NULL,
  `destination` varchar(200) NOT NULL,
  `driver_phone` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `trips_bike`
--

INSERT INTO `trips_bike` (`id`, `trip_type`, `trip_number`, `trip_date`, `driver_name`, `passenger_name`, `fare`, `taxi`, `vehicle_number`, `time`, `source`, `destination`, `driver_phone`) VALUES
(1, 'booking', '453', '2017-10-27', 'Sumit', 'Ravindra', '345', 'basic', 'UP16FT2345', '56minutes', 'delhi', 'Noida', '7757715178');

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE IF NOT EXISTS `type` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `base` varchar(255) NOT NULL,
  `commission` varchar(100) NOT NULL,
  `GST` varchar(100) NOT NULL,
  `other_tax` varchar(100) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`id`, `base`, `commission`, `GST`, `other_tax`, `created_date_time`) VALUES
(1, '10', '3', '18', '2', '2018-01-04 12:33:00');

-- --------------------------------------------------------

--
-- Table structure for table `type_auto`
--

CREATE TABLE IF NOT EXISTS `type_auto` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `base` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `type_auto`
--

INSERT INTO `type_auto` (`id`, `base`) VALUES
(1, '18');

-- --------------------------------------------------------

--
-- Table structure for table `type_bike`
--

CREATE TABLE IF NOT EXISTS `type_bike` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `base` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `type_bike`
--

INSERT INTO `type_bike` (`id`, `base`) VALUES
(1, '20');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(240) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `image` varchar(40) NOT NULL,
  `role` varchar(20) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `device_token` varchar(255) NOT NULL,
  `device_type` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `phone`, `password`, `image`, `role`, `status`, `device_token`, `device_type`, `created_date_time`) VALUES
(1, 'Admin', 'hyr@gmail.com', '8456791230', '123456789', 'user.png', 'Super Admin', 0, '', '', '2018-01-04 07:53:22');

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE IF NOT EXISTS `user_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_name` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`id`, `user_type_name`, `created_date_time`) VALUES
(1, 'Driver', '2018-01-10 11:00:42'),
(2, 'Passenger', '2018-01-10 11:00:42');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_type`
--

CREATE TABLE IF NOT EXISTS `vehicle_type` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `vehicle_type` varchar(255) NOT NULL,
  `price_per_km` varchar(255) NOT NULL,
  `price_per_min` varchar(255) NOT NULL,
  `base_fare` varchar(255) NOT NULL,
  `person_capacity` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `vehicle_type`
--

INSERT INTO `vehicle_type` (`id`, `vehicle_type`, `price_per_km`, `price_per_min`, `base_fare`, `person_capacity`) VALUES
(1, 'luxary', '54', '56', '233', '4');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_type_auto`
--

CREATE TABLE IF NOT EXISTS `vehicle_type_auto` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `vehicle_type` varchar(255) NOT NULL,
  `price_per_km` varchar(255) NOT NULL,
  `price_per_min` varchar(255) NOT NULL,
  `base_fare` varchar(255) NOT NULL,
  `person_capacity` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_type_bike`
--

CREATE TABLE IF NOT EXISTS `vehicle_type_bike` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `vehicle_type` varchar(255) NOT NULL,
  `price_per_km` varchar(255) NOT NULL,
  `price_per_min` varchar(255) NOT NULL,
  `base_fare` varchar(255) NOT NULL,
  `person_capacity` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_typ_data`
--

CREATE TABLE IF NOT EXISTS `vehicle_typ_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle_type_name` varchar(50) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `vehicle_typ_data`
--

INSERT INTO `vehicle_typ_data` (`id`, `vehicle_type_name`, `created_date_time`) VALUES
(1, 'Auto', '2018-01-05 09:14:12'),
(2, 'Cab', '2018-01-05 09:14:12'),
(3, 'Bike', '2018-01-05 09:14:20');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
