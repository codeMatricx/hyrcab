﻿<!--<link rel="stylesheet" href="assest/AdminLTE.min.css">-->
<?php
$email = $_SESSION['user'];
$sql ="SELECT name, image, role from user WHERE email ='$email'";
$result = $conn->query($sql);
if($result->num_rows > 0)
{
   $user = $result->fetch_assoc();
}
if ($_SESSION['role']=='technician' || $_SESSION['role']=='Agent') {
 ?>
 <script type="text/javascript">
     $(window).bind('keydown', function(event) {
    if (event.ctrlKey || event.metaKey) {
        switch (String.fromCharCode(event.which).toLowerCase()) {
        case 's':
            event.preventDefault();
            alert('Not Allowed to use ctrl + s');
            break;
        case 'u':
            event.preventDefault();
            alert('Not Allowed to use ctrl + u');
            break;
        case 'i':
            event.preventDefault();
            alert('Not Allowed to use ctrl + i');
            break;
        case 'p':
            event.preventDefault();
            alert('Not Allowed to use ctrl + p');
            break;
        }

    }
});
 </script>
<?php 
}
 ?>
<div id="wrap">
        <!-- HEADER SECTION -->
        <div id="top">

            <nav class="navbar navbar-inverse navbar-fixed-top " style="padding: 4px 0px; background-color: #<?php echo $header['head_color']; ?>">
                <a data-original-title="Show/Hide Menu" data-placement="bottom" data-tooltip="tooltip" class="accordion-toggle btn btn-primary btn-sm visible-xs" data-toggle="collapse" href="#menu" id="menu-toggle">
                    <i class="icon-align-justify"></i>
                </a>
                <!-- LOGO SECTION -->
                <header class="navbar-header">
                    <img src="assets/img/logo/<?php echo $header['logo']; ?>" style="width:114px;height:83px;margin-left: -7px;" alt="Paycabs" class="headerimage" />


                </header>
                <!-- END LOGO SECTION -->


                <ul class="nav navbar-top-links navbar-right">

                   

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="icon-user "></i>&nbsp; <i class="icon-chevron-down "></i>
                        </a>

                        <ul class="dropdown-menu dropdown-user">
                            
                    
					 
                
                  <!-- Menu Footer-->
                  
				  </li>
					
                            <!-- <li class="divider"></li> -->
                            <!-- <li><a href="login.html"><i class="icon-signout"></i> Logout </a>
                            </li> -->
                           <li>
                                <form method="post" class="logoutform">
                                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<i class="icon-signout"></i>
                                    <input class="logout" type="submit" name="logout" Value="logout">
                                </form>
                                <?php
                                    if(isset($_POST['logout']))
                                    {
                                        unset($_SESSION['user']);
                                        session_destroy();
                                        echo '<meta http-equiv="refresh" content="0">';
                                    }?>
                            </li>
                        </ul>

                    </li>
                    <!--END ADMIN SETTINGS -->
                </ul>

            </nav>

        </div>
        <!-- END HEADER SECTION -->
        <!-- MENU SECTION -->
        <div id="left">
            <div class="media user-media well-small">
                <a class="user-link" href="#">
                    <img style="height:127px;width:164px;" class="media-object img-thumbnail user-img" alt="User Picture" src="assets/img/User/<?php echo $user['image'] ?>" />
                </a>
                <br />
                <div class="media-body">
                    <h5 class="media-heading" style="color:#fff;"><?php echo strtoupper($user['name'])." (".$user['role'].")"; ?> </h5>
                </div>
                <br />
            </div>
			
            <ul id="menu" class="collapse">
             <div id="custom-search-input">
                            <div class="input-group col-md-12">
                                <input id="searchfor" type="text" class="  search-query form-control" placeholder="Search">
                                <span class="input-group-btn">
                                    <button class="btn btn-danger" type="button">
                                        <span class=" glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
                            </div>
             </div>
                
				
                 <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="icon-user "></i>&nbspCabs <i class="icon-chevron-down "></i></a>
						
                        <ul class="dropdown-menu " style="text-align: left; background-color: #ccc;">
						  <li><a href="driver1"><i class="icon-truck "></i>&nbsp <b>Driver</b></a></li>
						  <li><a href="trips"><i class="icon-truck "></i>&nbsp <b>Trips</b></a></li>
						  <li><a href="report"><i class="icon-truck "></i>&nbsp <b>Payment Report</b></a></li>
						  <li><a href="dispatch"><i class="icon-truck "></i>&nbsp <b>Manual Dispatch</b></a></li>
						  <li><a href="later"><i class="icon-truck "></i>&nbsp <b>Ride Later Booking</b></a></li>
						  <li><a href="taxi1"><i class="icon-truck "></i>&nbsp <b>Details</b></a></li>
						  <li><a href="instant"><i class="icon-truck "></i>&nbsp <b>Instant Booking</b></a></li>
                         </ul>
						 </li>	
				
				<li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="icon-truck "></i>&nbspAuto <i class="icon-chevron-down "></i></a>
                        <ul class="dropdown-menu " style="text-align:left; background-color: #ccc;">
						  <li><a href="driverauto"><i class="icon-truck "></i>&nbsp <b>Driver</b></a></li>
						  <li><a href="tripauto"><i class="icon-truck "></i>&nbsp <b>Trips</b></a></li>
						  <li><a href="reportauto"><i class="icon-truck "></i>&nbsp <b>Payment Report</b></a></li>
						  <li><a href="dispatchauto"><i class="icon-truck "></i>&nbsp <b>Manual Dispatch</b></a></li>
						  <li><a href="laterauto"><i class="icon-truck "></i>&nbsp <b>Ride Later Booking</b></a></li>
						  <li><a href="taxiauto"><i class="icon-truck "></i>&nbsp <b>Details</b></a></li>
						  <li><a href="instantauto"><i class="icon-truck "></i>&nbsp <b>Instant Booking</b></a></li>
                         </ul>
						 </li>
				
				 
				
				<li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="icon-truck "></i>&nbspBike <i class="icon-chevron-down "></i></a>
                        <ul class="dropdown-menu " style="text-align:left; background-color: #ccc;">
						 <li><a href="driverbike"><i class="icon-truck "></i>&nbsp <b>Driver</b></a></li>
						  <li><a href="tripbike"><i class="icon-truck "></i>&nbsp <b>Trips</b></a></li>
						  <li><a href="reportbike"><i class="icon-truck "></i>&nbsp <b>Payment Report</b></a></li>
						  <li><a href="dispatchbike"><i class="icon-truck "></i>&nbsp <b>Manual Dispatch</b></a></li>
						  <li><a href="laterbike"><i class="icon-truck "></i>&nbsp <b>Ride Later Booking</b></a></li>
						  <li><a href="taxibike"><i class="icon-truck "></i>&nbsp <b>Details</b></a></li>
						  <li><a href="instantbike"><i class="icon-truck "></i>&nbsp <b>Instant Booking</b></a></li>
                         </ul>
						 </li>	
				
				
                

                	

              					 
				
               
                <?php 
                if (($_SESSION['role']== 'admin') || ($_SESSION['role']== 'super admin')) {
                 ?>
                  <li><a href="rider"><i class="icon-user"></i> Passenger </a></li>
						 
                <?php } 
                if (($_SESSION['role']== 'admin') || ($_SESSION['role']== 'super admin')) {
                 ?>
                <li><a href="users"><i class="icon-user"></i> Admin </a></li>
                <?php }
                if ($_SESSION['role']== 'super admin') {
                 ?>
                <li><a href="profile"><i class="icon-user"></i> Profile </a></li>
                <?php } ?>
                
               <!-- <li><a href="team"><i class="icon-group"></i> Our Teams </a></li>
                <li><a href="product"><i class="icon-archive"></i> Product </a></li>
                <li><a href="customer"><i class="icon-smile"></i> customer </a></li>-->
                <li><a href="">
                    <form method="post" class="logoutform">
                        <i class="icon-signout"></i>
                        <input class="logout" type="submit" name="logout" Value="Logout">
                    </form>
                    <?php
                        if(isset($_POST['logout']))
                        {
                            unset($_SESSION['user']);
                            session_destroy();
                            echo '<meta http-equiv="refresh" content="0">';
                        }?>
                    </a>    
                </li>
            </ul>
        </div>
        <!--END MENU SECTION -->
        <!--PAGE CONTENT -->
        <div id="content">
            <?php 
                           if (isset($_GET['type'])) {
                                include "pages/type.php";	
                            }
							 if (isset($_GET['typeauto'])) {
                                include "pages/typeauto.php";	
                            }
							 if (isset($_GET['typebike'])) {
                                include "pages/typebike.php";	
                            }
							
							elseif (isset($_GET['rider'])) {
                                include "pages/rider.php";	
                            }
							elseif (isset($_GET['riderauto'])) {
                                include "pages/riderauto.php";	
                            }
							elseif (isset($_GET['riderbike'])) {
                                include "pages/riderbike.php";	
                            }
							
							elseif (isset($_GET['trips'])) {
                                include "pages/trips.php";	
                            }
							elseif (isset($_GET['tripauto'])) {
                                include "pages/tripauto.php";	
                            }
							elseif (isset($_GET['tripbike'])) {
                                include "pages/tripbike.php";	
                            }
							
							elseif (isset($_GET['later'])) {
                                include "pages/later.php";	
                            }
							elseif (isset($_GET['laterauto'])) {
                                include "pages/laterauto.php";	
                            }
							elseif (isset($_GET['laterbike'])) {
                                include "pages/laterbike.php";	
                            }
							
							elseif (isset($_GET['report'])) {
                                include "pages/report.php";	
                            }
							elseif (isset($_GET['reportauto'])) {
                                include "pages/reportauto.php";	
                            }
							elseif (isset($_GET['reportbike'])) {
                                include "pages/reportbike.php";	
                            }
							
							elseif (isset($_GET['taxi1'])) {
                                include "pages/taxi1.php";	
                            }
							elseif (isset($_GET['taxiauto'])) {
                                include "pages/taxiauto.php";	
                            }
							elseif (isset($_GET['taxibike'])) {
                                include "pages/taxibike.php";	
                            }
							
							elseif (isset($_GET['dispatch'])) {
                                include "pages/dispatch.php";	
                            }
							elseif (isset($_GET['dispatchauto'])) {
                                include "pages/dispatchauto.php";	
                            }
							elseif (isset($_GET['dispatchbike'])) {
                                include "pages/dispatchbike.php";	
                            }
							
							
                            elseif (isset($_GET['instant']) && (($_SESSION['role']== 'admin') || ($_SESSION['role']== 'super admin'))) {
                                include "pages/instant.php";
                            }
							elseif (isset($_GET['instantauto']) && (($_SESSION['role']== 'admin') || ($_SESSION['role']== 'super admin'))) {
                                include "pages/instantauto.php";
                            }
							elseif (isset($_GET['instantbike']) && (($_SESSION['role']== 'admin') || ($_SESSION['role']== 'super admin'))) {
                                include "pages/instantbike.php";
                            }
							
							
                            elseif (isset($_GET['setting'])) {
                                include "pages/setting.php";
                            }
                            elseif (isset($_GET['users']) && (($_SESSION['role']== 'admin') || ($_SESSION['role']== 'super admin'))) {
                                include "pages/users.php";
                            }
                            
                            
                            elseif (isset($_GET['profile']) && $_SESSION['role'] == 'super admin') {
                                include "pages/profile.php";
                            }
							
							elseif (isset($_GET['driverauto'])) {
                                include "pages/driverauto.php";
								
                            }
							elseif (isset($_GET['driverbike'])) {
                                include "pages/driverbike.php";
								
                            }
							
                            else
							{
                            
						
                                include "pages/driver1.php";
								
                            }
							
                         ?>
         <div style="clear:both;"></div>
        </div>
    </div>
    <!--END MAIN WRAPPER -->
    <!-- FOOTER -->
    <div id="footer">
        <p>© 2012 -2017. Paycabs . All Rights Reserved.</p>
    </div>
  <span id="message" style="position: fixed; top:0px; text-align: center; height: auto; padding:10px 0px; width: 400px; left: calc(50% - 200px);z-index: 3000; border:1px solid black;"><?php if(isset($responseMessage)) echo $responseMessage; ?></span>