<style type="text/css">
    body
    {
        background-color: #<?php echo $header['head_color']; ?>;
    }
</style>
<?php
if(isset($_POST['login']))
{
    $status = 1;
    function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    if (empty($_POST['email']) and empty($_POST['email']))
    {
        $emailerr = "*Please Enter Email Id";
        $status = 0;
    }
    else
    {
        $email = test_input($_POST['email']);
        $role = test_input($_POST['role']);
    }
    if (empty($_POST['password'])) {
        $passworderr = "*Please Enter Password";
        $status = 0;
    }
    else
    {
        $password = test_input($_POST['password']);
    }
    if ($status)
    {
        $sql = "SELECT id,password,image,name,role from user WHERE email = '$email' AND role = '$role'";
        $result = $conn->query($sql);
        if ($result->num_rows>0)
        {
            $userdata = $result->fetch_assoc();
            if ($userdata['password']== $password)
            {
                $_SESSION['user'] = $email;
                $_SESSION['role'] = strtolower($userdata['role']);
                $_SESSION['name'] = $userdata['name'];
                $_SESSION['uid'] = $userdata['id'];
                echo '<meta http-equiv="refresh" content="0">';
            }
            else
            {
                $passworderr = "*Incorrect Password";
            }    
            
        }
        else
        {
            $emailerr = "**Enter Valid Email Id and Password";
        }
    }  
}
?>
<div class="container">
    <div class="text-center">
        <h4><img src="assets/img/logo/<?php echo $header['logo']; ?>" style="width:150px;height:100px; margin-right: 20px;"></h4>
    </div>
    <div class="tab-content" style="width: 437px; margin-left: 30%; background-color: white;">
        <div id="login" class="tab-pane active">
            <form class="form-signin" method="post">
                <p class="text-muted text-center btn-block btn btn-primary btn-rect" style="color:white;background-color: #333333;border-color: #333333;">
                    PLEASE LOGIN
                </p>
                <select name="role" class="form-control" style="margin-top:20px;">
                 <!--<option>Select Type</option>-->
                    <option selected>Super Admin</option>
                    
                    
                </select>
                <input type="email" name="email" placeholder="Email" class="form-control" />
                <input type="password" name="password" placeholder="Password" class="form-control" />
                <input class="btn text-muted text-center btn-primary" type="submit" name="login" value="Sign in" style="background-color: #333333; border-color: #333333;">
                <div><?php 
                            if(isset($emailerr))
                            {
                                echo $emailerr;
                                echo "<br>";
                            }
                            if(isset($passworderr))
                            {
                                echo $passworderr;
                            }
                            ?>
                </div>
            </form>
        </div>
        <div id="forgot" class="tab-pane">
            <div class="form-signin">
                <p class="text-muted text-center btn-block btn btn-primary btn-rect">Enter your valid e-mail</p>
                <input id="email" type="email"  required="required" placeholder="Your E-mail"  class="form-control" name="email" />
                <br />
                <input id="mailPassword" type="submit" name="forgetpassword" class="btn text-muted text-center btn-success" value="Recover Password">
				
            </div>
            <div id="mailresponse" style="text-align: center;display: none;"></div>
			
        </div>
        <div id="signup" class="tab-pane">
            <form action="signupaction.php" class="form-signin">
                <p class="text-muted text-center btn-block btn btn-primary btn-rect">Please Fill Details To Register</p>
                 <input type="text" placeholder="First Name" class="form-control" />
                 <input type="text" placeholder="Last Name" class="form-control" />
                <input type="text" placeholder="Username" class="form-control" />
                <input type="email" placeholder="Your E-mail" class="form-control" />
                <input type="password" placeholder="password" class="form-control" />
                <input type="password" placeholder="Re type password" class="form-control" />
                <button class="btn text-muted text-center btn-success" type="submit">Register</button>
            </form>
        </div>
		
		<div class="text-center">
        <ul class="list-inline">
            
            <li><a class="text-muted" href="#forgot" data-toggle="tab"><u>FORGOT PASSWORD</u></a></li>
           
        </ul>
    </div>
    </div>
    


</div>
