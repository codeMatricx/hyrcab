//Function To Display Popup
function div_show(id,eid) {
	if(id == 'updateProduct')
	{
		$("#updatename").val($("#"+eid+" .name").html());
		$("#updateemail").val($("#"+eid+" .email").html());
		$("#updateprice").val($("#"+eid+" .phone").html());
		$("#updatedate").val($("#"+eid+" .registrationdate").html());
		$("#updatedl").val($("#"+eid+" .dl").html());
		$("#updatepassword").val($("#"+eid+" .pwd").html());
		$("#updatecity").val($("#"+eid+" .city").html());
		$("#updateaadhar").val($("#"+eid+" .aadhar").html());
		$("#updateId").val(eid);
	}
	
	if(id == 'updatePassenger')
	{
		$("#updatename").val($("#"+eid+" .name").html());
		$("#updateemail").val($("#"+eid+" .email").html());
		$("#updatephone").val($("#"+eid+" .phone").html());
		$("#updatedate").val($("#"+eid+" .date").html());
		$("#updatepwd").val($("#"+eid+" .pwd").html());
		//$("#updatecity").val($("#"+eid+" .city").html());
		$("#updateId").val(eid);
	}
	
	if(id == 'updateTaxi')
	{
		$("#updatename").val($("#"+eid+" .name").html());
		
		$("#updatedname").val($("#"+eid+" .dname").html());
		$("#updatevnumber").val($("#"+eid+" .vnumber").html());
		
		$("#updateId").val(eid);
	}
	
	
	if(id == 'updateInstant')
	{
		$("#updatepickup").val($("#"+eid+" .pickup").html());
		$("#updatedestination").val($("#"+eid+" .destination").html());
		$("#updatetype").val($("#"+eid+" .type").html());
		$("#updatedate").val($("#"+eid+" .date").html());
		$("#updatefare").val($("#"+eid+" .fare").html());
		$("#updatetime").val($("#"+eid+" .time").html());
		$("#updatepayment").val($("#"+eid+" .payment").html());
		$("#updatepname").val($("#"+eid+" .pname").html());
		$("#updatepphone").val($("#"+eid+" .pphone").html());
		$("#updatedname").val($("#"+eid+" .dname").html());
		$("#updatedphone").val($("#"+eid+" .dphone").html());
		$("#updateId").val(eid);
	}
	
	
	if(id == 'updateBase')
	{
		$("#updatebase").val($("#base").html());
		
		
	}
	
	
	if(id == 'updateTrip')
	{
		$("#updatedphone").val($("#"+eid+" .dphone").html());
		$("#updatetype").val($("#"+eid+" .type").html());
		//$("#updatetripno").val($("#"+eid+" .tripno").html());
		$("#updatedate").val($("#"+eid+" .date").html());		
		$("#updatedname").val($("#"+eid+" .dname").html());
		$("#updatepname").val($("#"+eid+" .pname").html());
		$("#updatefare").val($("#"+eid+" .fare").html());
		$("#updatetaxi").val($("#"+eid+" .taxi").html());
		$("#updatevnumber").val($("#"+eid+" .vnumber").html());
		$("#updatetime").val($("#"+eid+" .time").html());
		$("#updatesource").val($("#"+eid+" .source").html());
		$("#updatedestination").val($("#"+eid+" .destination").html());
		$("#updatenumber_passenger").val($("#"+eid+" .npassenger").html());
		$("#updatevname").val($("#"+eid+" .vname").html());
		$("#updateId").val(eid);
	}
	
	if(id == 'updateLate')
	{
		$("#updatepname").val($("#"+eid+" .pname").html());
		$("#updatedate").val($("#"+eid+" .date").html());
		$("#updatepickup").val($("#"+eid+" .pickup").html());
		$("#updatedestination").val($("#"+eid+" .destination").html());
		$("#updatedname").val($("#"+eid+" .dname").html());
		$("#updaterdate").val($("#"+eid+" .rdate").html());
		$("#updatevnumber").val($("#"+eid+" .vnumber").html());
		$("#updateId").val(eid);
	}
	
	if(id == 'updateAdmin')
	{
		$("#updatename").val($("#"+eid+" .name").html());
		$("#updateemail").val($("#"+eid+" .email").html());
		$("#updatephone").val($("#"+eid+" .phone").html());
		$("#updatepwd").val($("#"+eid+" .pwd").html());
		$("#updateId").val(eid);
	}
	if(id=='updateProfile')
	{
		$("#updatename").val($("#name").html());
		$("#updateemail").val($("#email").html());
		$("#updatemobile").val($("#mobile").html());
		$("#updateheading").val($("#heading").html());
	}
	
	if(id =='updateReport')
	{
		$("#updaternumber").val($("#"+eid+" .rnumber").html());
		$("#updatedname").val($("#"+eid+" .dname").html());
		$("#updatepname").val($("#"+eid+" .pname").html());
		$("#updatedate").val($("#"+eid+" .date").html());
		$("#updatefare").val($("#"+eid+" .fare").html());
		$("#updatestatus").val($("#"+eid+" .status").html());
		$("#updatepayment").val($("#"+eid+" .payment").html());
		$("#updatevnumber").val($("#"+eid+" .vnumber").html());
		$("#updateId").val(eid);
	}
	
	if (id=='imageview')
	{
		$("#imageviewer").attr('src', eid);
	}
	if (id == 'updateImage')
	{
		$("#fetchimage").val(eid);
	}
	$("#"+id).css('display', 'block');
	$("#deleteId").val(eid);
}
//Function to Hide Popup
function div_hide(id){
$("#"+id).css('display', 'none');
// document.getElementById('addProduct').style.display = "none";
}
$(document).ready(function(){
		var i= 0;  
     $('#searchfor').keyup(function(){
     	var searchedText = $(this).val();
     	 $(".name").each(function(){
            // If the listed item does not contain the text phrase fade it out
            if ($(this).text().search(new RegExp(searchedText, "i")) < 0)
            {
                $(this).parent().fadeOut();
            // Show the listed item if the phrase matches and increase the count by 1
            }
            else
            {
                $(this).parent().show();    
            }
        });
     	 $(".title").each(function(){
            // If the listed item does not contain the text phrase fade it out
            if ($(this).text().search(new RegExp(searchedText, "i")) < 0)
            {
                $(this).parent().fadeOut();
            // Show the listed item if the phrase matches and increase the count by 1
            }
            else
            {
                $(this).parent().show();    
            }
        });
    });
});

$(document).ready(function(){
	if ($("#message").is(':empty'))
	{
		$("#message").hide();
	}
	else
	{
		$("#message").fadeIn();
		$("#message").fadeOut(5000);
	}
});
$( document ).ready(function(){
	$("#mailPassword").click(function(){
		email = $('#email').val();
		$.ajax({
				type: "POST",
				url: 'mail.php',
				data:{"email" : email},
				success: function(data)
				{
					$("#mailresponse").fadeIn();
					$("#mailresponse").html(data);
					$("#mailresponse").fadeOut(10000);
					$("#email").val("");
				}
		});
	});	
});


$( document ).ready(function(){
	
    $("#colorpicker").change(function(event) {
        $("#headercolor").val($(this).val());
    });
});
